package io.swagger.organization.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * Characteristics used to group Parties for the formulation and targeting of MarketingCampaigns. ProfileTypes can be based on PartyDemographics, GeographicAreas, ProductOfferings, and MarketSegments. User-defined DataPoints can also be specified.
 */
@ApiModel(description = "Characteristics used to group Parties for the formulation and targeting of MarketingCampaigns. ProfileTypes can be based on PartyDemographics, GeographicAreas, ProductOfferings, and MarketSegments. User-defined DataPoints can also be specified.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartyProfileType   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("category")
  private String category = null;

  public PartyProfileType id(String id) {
    this.id = id;
    return this;
  }

  /**
   * A unique identifier for the PartyProfileType.
   * @return id
  **/
  @ApiModelProperty(value = "A unique identifier for the PartyProfileType.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public PartyProfileType description(String description) {
    this.description = description;
    return this;
  }

  /**
   * A narrative that explains what the PartyProfileType is.
   * @return description
  **/
  @ApiModelProperty(value = "A narrative that explains what the PartyProfileType is.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public PartyProfileType name(String name) {
    this.name = name;
    return this;
  }

  /**
   * A word, term, or phrase by which the characteristic is known and distinguished from characteristics.
   * @return name
  **/
  @ApiModelProperty(value = "A word, term, or phrase by which the characteristic is known and distinguished from characteristics.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PartyProfileType category(String category) {
    this.category = category;
    return this;
  }

  /**
   * A classification that groups PartyProfileTypes together because of common characteristics.
   * @return category
  **/
  @ApiModelProperty(value = "A classification that groups PartyProfileTypes together because of common characteristics.")


  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PartyProfileType partyProfileType = (PartyProfileType) o;
    return Objects.equals(this.id, partyProfileType.id) &&
        Objects.equals(this.description, partyProfileType.description) &&
        Objects.equals(this.name, partyProfileType.name) &&
        Objects.equals(this.category, partyProfileType.category);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, description, name, category);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PartyProfileType {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

