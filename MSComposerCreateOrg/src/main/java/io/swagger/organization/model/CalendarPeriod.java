package io.swagger.organization.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * CalendarPeriod
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CalendarPeriod   {
  @JsonProperty("day")
  private String day = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("comment")
  private String comment = null;

  @JsonProperty("timeZone")
  private String timeZone = null;

  @JsonProperty("hourPeriod")
  @Valid
  private List<HourPeriod> hourPeriod = null;

  public CalendarPeriod day(String day) {
    this.day = day;
    return this;
  }

  /**
   * Day where the calendar status applies (e.g.: monday, mon-to-fri, weekdays, weekend, all week, ...)
   * @return day
  **/
  @ApiModelProperty(value = "Day where the calendar status applies (e.g.: monday, mon-to-fri, weekdays, weekend, all week, ...)")


  public String getDay() {
    return day;
  }

  public void setDay(String day) {
    this.day = day;
  }

  public CalendarPeriod status(String status) {
    this.status = status;
    return this;
  }

  /**
   * Indication of the availability of the resource (e.g.: open)
   * @return status
  **/
  @ApiModelProperty(value = "Indication of the availability of the resource (e.g.: open)")


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public CalendarPeriod comment(String comment) {
    this.comment = comment;
    return this;
  }

  /**
   * Comment text about the entry   Note: COMMENT [iCalendar]
   * @return comment
  **/
  @ApiModelProperty(value = "Comment text about the entry   Note: COMMENT [iCalendar]")


  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public CalendarPeriod timeZone(String timeZone) {
    this.timeZone = timeZone;
    return this;
  }

  /**
   * Indication of the timezone applicable to the calendar information (e.g.: Paris, GMT+1)
   * @return timeZone
  **/
  @ApiModelProperty(value = "Indication of the timezone applicable to the calendar information (e.g.: Paris, GMT+1)")


  public String getTimeZone() {
    return timeZone;
  }

  public void setTimeZone(String timeZone) {
    this.timeZone = timeZone;
  }

  public CalendarPeriod hourPeriod(List<HourPeriod> hourPeriod) {
    this.hourPeriod = hourPeriod;
    return this;
  }

  public CalendarPeriod addHourPeriodItem(HourPeriod hourPeriodItem) {
    if (this.hourPeriod == null) {
      this.hourPeriod = new ArrayList<HourPeriod>();
    }
    this.hourPeriod.add(hourPeriodItem);
    return this;
  }

  /**
   * Get hourPeriod
   * @return hourPeriod
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<HourPeriod> getHourPeriod() {
    return hourPeriod;
  }

  public void setHourPeriod(List<HourPeriod> hourPeriod) {
    this.hourPeriod = hourPeriod;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CalendarPeriod calendarPeriod = (CalendarPeriod) o;
    return Objects.equals(this.day, calendarPeriod.day) &&
        Objects.equals(this.status, calendarPeriod.status) &&
        Objects.equals(this.comment, calendarPeriod.comment) &&
        Objects.equals(this.timeZone, calendarPeriod.timeZone) &&
        Objects.equals(this.hourPeriod, calendarPeriod.hourPeriod);
  }

  @Override
  public int hashCode() {
    return Objects.hash(day, status, comment, timeZone, hourPeriod);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CalendarPeriod {\n");
    
    sb.append("    day: ").append(toIndentedString(day)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    comment: ").append(toIndentedString(comment)).append("\n");
    sb.append("    timeZone: ").append(toIndentedString(timeZone)).append("\n");
    sb.append("    hourPeriod: ").append(toIndentedString(hourPeriod)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

