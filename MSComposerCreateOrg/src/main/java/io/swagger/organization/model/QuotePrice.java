package io.swagger.organization.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Description of price and discount awarded
 */
@ApiModel(description = "Description of price and discount awarded")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuotePrice   {
  @JsonProperty("description")
  private String description = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("priceType")
  private String priceType = null;

  @JsonProperty("recurringChargePeriod")
  private String recurringChargePeriod = null;

  @JsonProperty("price")
  private Price price = null;

  public QuotePrice description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Description of the quote/quote item price
   * @return description
  **/
  @ApiModelProperty(value = "Description of the quote/quote item price")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public QuotePrice name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the quote /quote item price
   * @return name
  **/
  @ApiModelProperty(value = "Name of the quote /quote item price")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public QuotePrice priceType(String priceType) {
    this.priceType = priceType;
    return this;
  }

  /**
   * indicate if the price is for recurrent or no-recurrent charge
   * @return priceType
  **/
  @ApiModelProperty(value = "indicate if the price is for recurrent or no-recurrent charge")


  public String getPriceType() {
    return priceType;
  }

  public void setPriceType(String priceType) {
    this.priceType = priceType;
  }

  public QuotePrice recurringChargePeriod(String recurringChargePeriod) {
    this.recurringChargePeriod = recurringChargePeriod;
    return this;
  }

  /**
   * Used for recurring charge to indicate period (month, week, etc..)
   * @return recurringChargePeriod
  **/
  @ApiModelProperty(value = "Used for recurring charge to indicate period (month, week, etc..)")


  public String getRecurringChargePeriod() {
    return recurringChargePeriod;
  }

  public void setRecurringChargePeriod(String recurringChargePeriod) {
    this.recurringChargePeriod = recurringChargePeriod;
  }

  public QuotePrice price(Price price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Price getPrice() {
    return price;
  }

  public void setPrice(Price price) {
    this.price = price;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    QuotePrice quotePrice = (QuotePrice) o;
    return Objects.equals(this.description, quotePrice.description) &&
        Objects.equals(this.name, quotePrice.name) &&
        Objects.equals(this.priceType, quotePrice.priceType) &&
        Objects.equals(this.recurringChargePeriod, quotePrice.recurringChargePeriod) &&
        Objects.equals(this.price, quotePrice.price);
  }

  @Override
  public int hashCode() {
    return Objects.hash(description, name, priceType, recurringChargePeriod, price);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class QuotePrice {\n");
    
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    priceType: ").append(toIndentedString(priceType)).append("\n");
    sb.append("    recurringChargePeriod: ").append(toIndentedString(recurringChargePeriod)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

