package io.swagger.organization.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * A geographic address is a structured textual way of describing how to find a Property in an urban area (country properties are often defined differently)
 */
@ApiModel(description = "A geographic address is a structured textual way of describing how to find a Property in an urban area (country properties are often defined differently)")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeographicAddressSpec   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("@type")
  private String _type = null;

  @JsonProperty("@schemaLocation")
  private String schemaLocation = null;

  @JsonProperty("characteristic")
  @Valid
  private List<Characteristic> characteristic = null;

  public GeographicAddressSpec id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Unique identifier of the address
   * @return id
  **/
  @ApiModelProperty(value = "Unique identifier of the address")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public GeographicAddressSpec href(String href) {
    this.href = href;
    return this;
  }

  /**
   * An URI used to access to the address resource
   * @return href
  **/
  @ApiModelProperty(value = "An URI used to access to the address resource")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public GeographicAddressSpec type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Type of the GeographicAddressSpec
   * @return type
  **/
  @ApiModelProperty(value = "Type of the GeographicAddressSpec")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public GeographicAddressSpec _type(String type) {
    this._type = type;
    return this;
  }

  /**
   * Indicates the type of the resource. Here can be 'UrbanPropertyAddress', ‘FormattedAddress’, ‘JapanesePropertyAddress’ , ‘AustralianPropertyAddress’, etc…
   * @return type
  **/
  @ApiModelProperty(value = "Indicates the type of the resource. Here can be 'UrbanPropertyAddress', ‘FormattedAddress’, ‘JapanesePropertyAddress’ , ‘AustralianPropertyAddress’, etc…")


  public String get_Type() {
    return _type;
  }

  public void set_Type(String type) {
    this._type = type;
  }

  public GeographicAddressSpec schemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
    return this;
  }

  /**
   * A Link to the schema describing this REST Resource. The resource described 'UrbanPropertyAddress' but a schema could be used for other property address description.
   * @return schemaLocation
  **/
  @ApiModelProperty(value = "A Link to the schema describing this REST Resource. The resource described 'UrbanPropertyAddress' but a schema could be used for other property address description.")


  public String getSchemaLocation() {
    return schemaLocation;
  }

  public void setSchemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
  }

  public GeographicAddressSpec characteristic(List<Characteristic> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public GeographicAddressSpec addCharacteristicItem(Characteristic characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<Characteristic>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

  /**
   * Get characteristic
   * @return characteristic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Characteristic> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<Characteristic> characteristic) {
    this.characteristic = characteristic;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GeographicAddressSpec geographicAddressSpec = (GeographicAddressSpec) o;
    return Objects.equals(this.id, geographicAddressSpec.id) &&
        Objects.equals(this.href, geographicAddressSpec.href) &&
        Objects.equals(this.type, geographicAddressSpec.type) &&
        Objects.equals(this.type, geographicAddressSpec.type) &&
        Objects.equals(this.schemaLocation, geographicAddressSpec.schemaLocation) &&
        Objects.equals(this.characteristic, geographicAddressSpec.characteristic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, href, type, type, schemaLocation, characteristic);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GeographicAddressSpec {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    schemaLocation: ").append(toIndentedString(schemaLocation)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

