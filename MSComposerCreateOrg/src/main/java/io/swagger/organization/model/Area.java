package io.swagger.organization.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * Area corresponds to a geographic area as a city, a locality, a district, etc
 */
@ApiModel(description = "Area corresponds to a geographic area as a city, a locality, a district, etc")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Area   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("description")
  private String description = null;

  public Area id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Unique identifier of an Area
   * @return id
  **/
  @ApiModelProperty(value = "Unique identifier of an Area")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Area name(String name) {
    this.name = name;
    return this;
  }

  /**
   * The defined name of the municipality
   * @return name
  **/
  @ApiModelProperty(value = "The defined name of the municipality")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Area type(String type) {
    this.type = type;
    return this;
  }

  /**
   * SUBURB, LOCALITY, CITY, TOWN, BOROUGH, ....
   * @return type
  **/
  @ApiModelProperty(value = "SUBURB, LOCALITY, CITY, TOWN, BOROUGH, ....")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Area description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Description of an Area
   * @return description
  **/
  @ApiModelProperty(value = "Description of an Area")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Area area = (Area) o;
    return Objects.equals(this.id, area.id) &&
        Objects.equals(this.name, area.name) &&
        Objects.equals(this.type, area.type) &&
        Objects.equals(this.description, area.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, type, description);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Area {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

