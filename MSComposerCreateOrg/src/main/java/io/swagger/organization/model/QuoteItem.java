package io.swagger.organization.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * A quote items describe an action to be performed on a productOffering or a product in order to get pricing elements and condition.
 */
@ApiModel(description = "A quote items describe an action to be performed on a productOffering or a product in order to get pricing elements and condition.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuoteItem   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("action")
  private String action = null;

  @JsonProperty("quantity")
  private Integer quantity = null;

  @JsonProperty("state")
  private String state = null;

  @JsonProperty("productOffering")
  private ProductOfferingRef productOffering = null;

  @JsonProperty("quoteItemPrice")
  @Valid
  private List<QuotePrice> quoteItemPrice = null;

  public QuoteItem id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Identifier of the quote item (generally it is a sequence number 01, 02, 03, ...)
   * @return id
  **/
  @ApiModelProperty(value = "Identifier of the quote item (generally it is a sequence number 01, 02, 03, ...)")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public QuoteItem action(String action) {
    this.action = action;
    return this;
  }

  /**
   * Action to be performed on this quote item (add, modify, remove, etc.)
   * @return action
  **/
  @ApiModelProperty(value = "Action to be performed on this quote item (add, modify, remove, etc.)")


  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public QuoteItem quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Quantity asked for this quote item
   * @return quantity
  **/
  @ApiModelProperty(value = "Quantity asked for this quote item")


  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public QuoteItem state(String state) {
    this.state = state;
    return this;
  }

  /**
   * State of the quote item : described in the state machine diagram
   * @return state
  **/
  @ApiModelProperty(value = "State of the quote item : described in the state machine diagram")


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public QuoteItem productOffering(ProductOfferingRef productOffering) {
    this.productOffering = productOffering;
    return this;
  }

  /**
   * Get productOffering
   * @return productOffering
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ProductOfferingRef getProductOffering() {
    return productOffering;
  }

  public void setProductOffering(ProductOfferingRef productOffering) {
    this.productOffering = productOffering;
  }

  public QuoteItem quoteItemPrice(List<QuotePrice> quoteItemPrice) {
    this.quoteItemPrice = quoteItemPrice;
    return this;
  }

  public QuoteItem addQuoteItemPriceItem(QuotePrice quoteItemPriceItem) {
    if (this.quoteItemPrice == null) {
      this.quoteItemPrice = new ArrayList<QuotePrice>();
    }
    this.quoteItemPrice.add(quoteItemPriceItem);
    return this;
  }

  /**
   * Price for this quote item
   * @return quoteItemPrice
  **/
  @ApiModelProperty(value = "Price for this quote item")

  @Valid

  public List<QuotePrice> getQuoteItemPrice() {
    return quoteItemPrice;
  }

  public void setQuoteItemPrice(List<QuotePrice> quoteItemPrice) {
    this.quoteItemPrice = quoteItemPrice;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    QuoteItem quoteItem = (QuoteItem) o;
    return Objects.equals(this.id, quoteItem.id) &&
        Objects.equals(this.action, quoteItem.action) &&
        Objects.equals(this.quantity, quoteItem.quantity) &&
        Objects.equals(this.state, quoteItem.state) &&
        Objects.equals(this.productOffering, quoteItem.productOffering) &&
        Objects.equals(this.quoteItemPrice, quoteItem.quoteItemPrice);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, action, quantity, state, productOffering, quoteItemPrice);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class QuoteItem {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    action: ").append(toIndentedString(action)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    productOffering: ").append(toIndentedString(productOffering)).append("\n");
    sb.append("    quoteItemPrice: ").append(toIndentedString(quoteItemPrice)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

