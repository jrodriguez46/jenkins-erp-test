package io.swagger.organization.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * PolicyRuleRef
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PolicyRuleRef   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  public PolicyRuleRef id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Unique identifier of the PolicyRule.
   * @return id
  **/
  @ApiModelProperty(value = "Unique identifier of the PolicyRule.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public PolicyRuleRef href(String href) {
    this.href = href;
    return this;
  }

  /**
   * Reference of the related PolicyRule.
   * @return href
  **/
  @ApiModelProperty(value = "Reference of the related PolicyRule.")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public PolicyRuleRef name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the PolicyRule
   * @return name
  **/
  @ApiModelProperty(value = "Name of the PolicyRule")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PolicyRuleRef description(String description) {
    this.description = description;
    return this;
  }

  /**
   * A narrative that explains in detail what the PolicyRule is
   * @return description
  **/
  @ApiModelProperty(value = "A narrative that explains in detail what the PolicyRule is")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PolicyRuleRef policyRuleRef = (PolicyRuleRef) o;
    return Objects.equals(this.id, policyRuleRef.id) &&
        Objects.equals(this.href, policyRuleRef.href) &&
        Objects.equals(this.name, policyRuleRef.name) &&
        Objects.equals(this.description, policyRuleRef.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, href, name, description);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PolicyRuleRef {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

