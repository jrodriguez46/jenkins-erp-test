package io.swagger.organization.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Quote can be used to negotiate service and product acquisition or modification between a customer and a service provider. Quote contain list of quote items, a reference to customer (partyRole), a list of productOffering and attached prices and conditions.
 */
@ApiModel(description = "Quote can be used to negotiate service and product acquisition or modification between a customer and a service provider. Quote contain list of quote items, a reference to customer (partyRole), a list of productOffering and attached prices and conditions.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuoteRef   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("category")
  private String category = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("externalId")
  private String externalId = null;

  @JsonProperty("quoteItem")
  @Valid
  private List<QuoteItem> quoteItem = null;

  @JsonProperty("quoteTotalPrice")
  @Valid
  private List<QuotePrice> quoteTotalPrice = null;

  @JsonProperty("validFor")
  private TimePeriod validFor = null;

  public QuoteRef id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Unique identifier - attributed by quoting system
   * @return id
  **/
  @ApiModelProperty(value = "Unique identifier - attributed by quoting system")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public QuoteRef href(String href) {
    this.href = href;
    return this;
  }

  /**
   * Hyperlink to access the quote
   * @return href
  **/
  @ApiModelProperty(value = "Hyperlink to access the quote")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public QuoteRef category(String category) {
    this.category = category;
    return this;
  }

  /**
   * Used to categorize the quote from a business perspective that can be useful for the CRM system (e.g. \"enterprise\", \"residential\", ...)
   * @return category
  **/
  @ApiModelProperty(value = "Used to categorize the quote from a business perspective that can be useful for the CRM system (e.g. \"enterprise\", \"residential\", ...)")


  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public QuoteRef description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Description of the quote
   * @return description
  **/
  @ApiModelProperty(value = "Description of the quote")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public QuoteRef externalId(String externalId) {
    this.externalId = externalId;
    return this;
  }

  /**
   * ID given by the consumer and only understandable by him (to facilitate his searches afterwards)
   * @return externalId
  **/
  @ApiModelProperty(value = "ID given by the consumer and only understandable by him (to facilitate his searches afterwards)")


  public String getExternalId() {
    return externalId;
  }

  public void setExternalId(String externalId) {
    this.externalId = externalId;
  }

  public QuoteRef quoteItem(List<QuoteItem> quoteItem) {
    this.quoteItem = quoteItem;
    return this;
  }

  public QuoteRef addQuoteItemItem(QuoteItem quoteItemItem) {
    if (this.quoteItem == null) {
      this.quoteItem = new ArrayList<QuoteItem>();
    }
    this.quoteItem.add(quoteItemItem);
    return this;
  }

  /**
   * An item of the quote - it is used to descirbe an operation on a product to be quoted
   * @return quoteItem
  **/
  @ApiModelProperty(value = "An item of the quote - it is used to descirbe an operation on a product to be quoted")

  @Valid
@Size(min=1) 
  public List<QuoteItem> getQuoteItem() {
    return quoteItem;
  }

  public void setQuoteItem(List<QuoteItem> quoteItem) {
    this.quoteItem = quoteItem;
  }

  public QuoteRef quoteTotalPrice(List<QuotePrice> quoteTotalPrice) {
    this.quoteTotalPrice = quoteTotalPrice;
    return this;
  }

  public QuoteRef addQuoteTotalPriceItem(QuotePrice quoteTotalPriceItem) {
    if (this.quoteTotalPrice == null) {
      this.quoteTotalPrice = new ArrayList<QuotePrice>();
    }
    this.quoteTotalPrice.add(quoteTotalPriceItem);
    return this;
  }

  /**
   * Quote total price
   * @return quoteTotalPrice
  **/
  @ApiModelProperty(value = "Quote total price")

  @Valid

  public List<QuotePrice> getQuoteTotalPrice() {
    return quoteTotalPrice;
  }

  public void setQuoteTotalPrice(List<QuotePrice> quoteTotalPrice) {
    this.quoteTotalPrice = quoteTotalPrice;
  }

  public QuoteRef validFor(TimePeriod validFor) {
    this.validFor = validFor;
    return this;
  }

  /**
   * Get validFor
   * @return validFor
  **/
  @ApiModelProperty(value = "")

  @Valid

  public TimePeriod getValidFor() {
    return validFor;
  }

  public void setValidFor(TimePeriod validFor) {
    this.validFor = validFor;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    QuoteRef quoteRef = (QuoteRef) o;
    return Objects.equals(this.id, quoteRef.id) &&
        Objects.equals(this.href, quoteRef.href) &&
        Objects.equals(this.category, quoteRef.category) &&
        Objects.equals(this.description, quoteRef.description) &&
        Objects.equals(this.externalId, quoteRef.externalId) &&
        Objects.equals(this.quoteItem, quoteRef.quoteItem) &&
        Objects.equals(this.quoteTotalPrice, quoteRef.quoteTotalPrice) &&
        Objects.equals(this.validFor, quoteRef.validFor);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, href, category, description, externalId, quoteItem, quoteTotalPrice, validFor);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class QuoteRef {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    externalId: ").append(toIndentedString(externalId)).append("\n");
    sb.append("    quoteItem: ").append(toIndentedString(quoteItem)).append("\n");
    sb.append("    quoteTotalPrice: ").append(toIndentedString(quoteTotalPrice)).append("\n");
    sb.append("    validFor: ").append(toIndentedString(validFor)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

