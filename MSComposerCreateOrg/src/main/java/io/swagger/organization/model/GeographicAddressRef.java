package io.swagger.organization.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Reference to an address resource. This reference includes the complete address specification that was requested by the provider (Claro Chile/Ericson)
 */
@ApiModel(description = "Reference to an address resource. This reference includes the complete address specification that was requested by the provider (Claro Chile/Ericson)")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeographicAddressRef   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("externalId")
  private String externalId = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("type")
  private Area type = null;

  @JsonProperty("streetNr")
  private String streetNr = null;

  @JsonProperty("streetName")
  private String streetName = null;

  @JsonProperty("streetType")
  private Area streetType = null;

  @JsonProperty("postcode")
  private String postcode = null;

  @JsonProperty("locality")
  private Area locality = null;

  @JsonProperty("city")
  private Area city = null;

  @JsonProperty("stateOrProvince")
  private Area stateOrProvince = null;

  @JsonProperty("country")
  private Area country = null;

  @JsonProperty("@type")
  private String _type = null;

  @JsonProperty("@schemaLocation")
  private String schemaLocation = null;

  @JsonProperty("geographicLocation")
  private GeographicLocation geographicLocation = null;

  @JsonProperty("geographicSubAddress")
  @Valid
  private List<GeographicSubAddress> geographicSubAddress = null;

  @JsonProperty("geographicAddressSpec")
  @Valid
  private List<GeographicAddressSpec> geographicAddressSpec = null;

  @JsonProperty("area")
  @Valid
  private List<Area> area = null;

  @JsonProperty("characteristic")
  @Valid
  private List<LocationCharacteristic> characteristic = null;

  @JsonProperty("owner")
  private String owner = null;

  public GeographicAddressRef id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Unique identifier of the address
   * @return id
  **/
  @ApiModelProperty(value = "Unique identifier of the address")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public GeographicAddressRef externalId(String externalId) {
    this.externalId = externalId;
    return this;
  }

  /**
   * Unique identifier of the address in an external system
   * @return externalId
  **/
  @ApiModelProperty(value = "Unique identifier of the address in an external system")


  public String getExternalId() {
    return externalId;
  }

  public void setExternalId(String externalId) {
    this.externalId = externalId;
  }

  public GeographicAddressRef href(String href) {
    this.href = href;
    return this;
  }

  /**
   * An URI used to access to the address resource
   * @return href
  **/
  @ApiModelProperty(value = "An URI used to access to the address resource")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public GeographicAddressRef type(Area type) {
    this.type = type;
    return this;
  }

  /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Area getType() {
    return type;
  }

  public void setType(Area type) {
    this.type = type;
  }

  public GeographicAddressRef streetNr(String streetNr) {
    this.streetNr = streetNr;
    return this;
  }

  /**
   * Number identifying a specific property on a public street. It may be combined with streetNrLast for ranged addresses
   * @return streetNr
  **/
  @ApiModelProperty(value = "Number identifying a specific property on a public street. It may be combined with streetNrLast for ranged addresses")


  public String getStreetNr() {
    return streetNr;
  }

  public void setStreetNr(String streetNr) {
    this.streetNr = streetNr;
  }

  public GeographicAddressRef streetName(String streetName) {
    this.streetName = streetName;
    return this;
  }

  /**
   * Name of the street or other street type
   * @return streetName
  **/
  @ApiModelProperty(value = "Name of the street or other street type")


  public String getStreetName() {
    return streetName;
  }

  public void setStreetName(String streetName) {
    this.streetName = streetName;
  }

  public GeographicAddressRef streetType(Area streetType) {
    this.streetType = streetType;
    return this;
  }

  /**
   * Get streetType
   * @return streetType
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Area getStreetType() {
    return streetType;
  }

  public void setStreetType(Area streetType) {
    this.streetType = streetType;
  }

  public GeographicAddressRef postcode(String postcode) {
    this.postcode = postcode;
    return this;
  }

  /**
   * Descriptor for a postal delivery area, used to speed and simplify the delivery of mail (also known as zipcode)
   * @return postcode
  **/
  @ApiModelProperty(value = "Descriptor for a postal delivery area, used to speed and simplify the delivery of mail (also known as zipcode)")


  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public GeographicAddressRef locality(Area locality) {
    this.locality = locality;
    return this;
  }

  /**
   * Get locality
   * @return locality
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Area getLocality() {
    return locality;
  }

  public void setLocality(Area locality) {
    this.locality = locality;
  }

  public GeographicAddressRef city(Area city) {
    this.city = city;
    return this;
  }

  /**
   * Get city
   * @return city
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Area getCity() {
    return city;
  }

  public void setCity(Area city) {
    this.city = city;
  }

  public GeographicAddressRef stateOrProvince(Area stateOrProvince) {
    this.stateOrProvince = stateOrProvince;
    return this;
  }

  /**
   * Get stateOrProvince
   * @return stateOrProvince
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Area getStateOrProvince() {
    return stateOrProvince;
  }

  public void setStateOrProvince(Area stateOrProvince) {
    this.stateOrProvince = stateOrProvince;
  }

  public GeographicAddressRef country(Area country) {
    this.country = country;
    return this;
  }

  /**
   * Get country
   * @return country
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Area getCountry() {
    return country;
  }

  public void setCountry(Area country) {
    this.country = country;
  }

  public GeographicAddressRef type(String type) {
    this._type = type;
    return this;
  }

  /**
   * Indicates the type of the resource. Here can be 'UrbanPropertyAddress', ‘FormattedAddress’, ‘JapanesePropertyAddress’ , ‘AustralianPropertyAddress’, etc…
   * @return type
  **/
  @ApiModelProperty(value = "Indicates the type of the resource. Here can be 'UrbanPropertyAddress', ‘FormattedAddress’, ‘JapanesePropertyAddress’ , ‘AustralianPropertyAddress’, etc…")


  public String get_Type() {
    return _type;
  }

  public void set_Type(String type) {
    this._type = type;
  }

  public GeographicAddressRef schemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
    return this;
  }

  /**
   * A Link to the schema describing this REST Resource. The resource described 'UrbanPropertyAddress' but a schema could be used for other property address description.
   * @return schemaLocation
  **/
  @ApiModelProperty(value = "A Link to the schema describing this REST Resource. The resource described 'UrbanPropertyAddress' but a schema could be used for other property address description.")


  public String getSchemaLocation() {
    return schemaLocation;
  }

  public void setSchemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
  }

  public GeographicAddressRef geographicLocation(GeographicLocation geographicLocation) {
    this.geographicLocation = geographicLocation;
    return this;
  }

  /**
   * Get geographicLocation
   * @return geographicLocation
  **/
  @ApiModelProperty(value = "")

  @Valid

  public GeographicLocation getGeographicLocation() {
    return geographicLocation;
  }

  public void setGeographicLocation(GeographicLocation geographicLocation) {
    this.geographicLocation = geographicLocation;
  }

  public GeographicAddressRef geographicSubAddress(List<GeographicSubAddress> geographicSubAddress) {
    this.geographicSubAddress = geographicSubAddress;
    return this;
  }

  public GeographicAddressRef addGeographicSubAddressItem(GeographicSubAddress geographicSubAddressItem) {
    if (this.geographicSubAddress == null) {
      this.geographicSubAddress = new ArrayList<GeographicSubAddress>();
    }
    this.geographicSubAddress.add(geographicSubAddressItem);
    return this;
  }

  /**
   * Get geographicSubAddress
   * @return geographicSubAddress
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<GeographicSubAddress> getGeographicSubAddress() {
    return geographicSubAddress;
  }

  public void setGeographicSubAddress(List<GeographicSubAddress> geographicSubAddress) {
    this.geographicSubAddress = geographicSubAddress;
  }

  public GeographicAddressRef geographicAddressSpec(List<GeographicAddressSpec> geographicAddressSpec) {
    this.geographicAddressSpec = geographicAddressSpec;
    return this;
  }

  public GeographicAddressRef addGeographicAddressSpecItem(GeographicAddressSpec geographicAddressSpecItem) {
    if (this.geographicAddressSpec == null) {
      this.geographicAddressSpec = new ArrayList<GeographicAddressSpec>();
    }
    this.geographicAddressSpec.add(geographicAddressSpecItem);
    return this;
  }

  /**
   * Get geographicAddressSpec
   * @return geographicAddressSpec
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<GeographicAddressSpec> getGeographicAddressSpec() {
    return geographicAddressSpec;
  }

  public void setGeographicAddressSpec(List<GeographicAddressSpec> geographicAddressSpec) {
    this.geographicAddressSpec = geographicAddressSpec;
  }

  public GeographicAddressRef area(List<Area> area) {
    this.area = area;
    return this;
  }

  public GeographicAddressRef addAreaItem(Area areaItem) {
    if (this.area == null) {
      this.area = new ArrayList<Area>();
    }
    this.area.add(areaItem);
    return this;
  }

  /**
   * Get area
   * @return area
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Area> getArea() {
    return area;
  }

  public void setArea(List<Area> area) {
    this.area = area;
  }

  public GeographicAddressRef characteristic(List<LocationCharacteristic> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public GeographicAddressRef addCharacteristicItem(LocationCharacteristic characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<LocationCharacteristic>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

  /**
   * Get characteristic
   * @return characteristic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<LocationCharacteristic> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<LocationCharacteristic> characteristic) {
    this.characteristic = characteristic;
  }

  public GeographicAddressRef owner(String owner) {
    this.owner = owner;
    return this;
  }

  /**
   * Name of the geographic address owner
   * @return owner
  **/
  @ApiModelProperty(value = "Name of the geographic address owner")


  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GeographicAddressRef geographicAddressRef = (GeographicAddressRef) o;
    return Objects.equals(this.id, geographicAddressRef.id) &&
        Objects.equals(this.externalId, geographicAddressRef.externalId) &&
        Objects.equals(this.href, geographicAddressRef.href) &&
        Objects.equals(this.type, geographicAddressRef.type) &&
        Objects.equals(this.streetNr, geographicAddressRef.streetNr) &&
        Objects.equals(this.streetName, geographicAddressRef.streetName) &&
        Objects.equals(this.streetType, geographicAddressRef.streetType) &&
        Objects.equals(this.postcode, geographicAddressRef.postcode) &&
        Objects.equals(this.locality, geographicAddressRef.locality) &&
        Objects.equals(this.city, geographicAddressRef.city) &&
        Objects.equals(this.stateOrProvince, geographicAddressRef.stateOrProvince) &&
        Objects.equals(this.country, geographicAddressRef.country) &&
        Objects.equals(this.type, geographicAddressRef.type) &&
        Objects.equals(this.schemaLocation, geographicAddressRef.schemaLocation) &&
        Objects.equals(this.geographicLocation, geographicAddressRef.geographicLocation) &&
        Objects.equals(this.geographicSubAddress, geographicAddressRef.geographicSubAddress) &&
        Objects.equals(this.geographicAddressSpec, geographicAddressRef.geographicAddressSpec) &&
        Objects.equals(this.area, geographicAddressRef.area) &&
        Objects.equals(this.characteristic, geographicAddressRef.characteristic) &&
        Objects.equals(this.owner, geographicAddressRef.owner);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, externalId, href, type, streetNr, streetName, streetType, postcode, locality, city, stateOrProvince, country, type, schemaLocation, geographicLocation, geographicSubAddress, geographicAddressSpec, area, characteristic, owner);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GeographicAddressRef {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    externalId: ").append(toIndentedString(externalId)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    streetNr: ").append(toIndentedString(streetNr)).append("\n");
    sb.append("    streetName: ").append(toIndentedString(streetName)).append("\n");
    sb.append("    streetType: ").append(toIndentedString(streetType)).append("\n");
    sb.append("    postcode: ").append(toIndentedString(postcode)).append("\n");
    sb.append("    locality: ").append(toIndentedString(locality)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    stateOrProvince: ").append(toIndentedString(stateOrProvince)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    schemaLocation: ").append(toIndentedString(schemaLocation)).append("\n");
    sb.append("    geographicLocation: ").append(toIndentedString(geographicLocation)).append("\n");
    sb.append("    geographicSubAddress: ").append(toIndentedString(geographicSubAddress)).append("\n");
    sb.append("    geographicAddressSpec: ").append(toIndentedString(geographicAddressSpec)).append("\n");
    sb.append("    area: ").append(toIndentedString(area)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

