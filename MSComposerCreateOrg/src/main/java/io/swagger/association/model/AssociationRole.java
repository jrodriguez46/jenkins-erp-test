package io.swagger.association.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * This embedded object represents the role and type of each entity involved in a relationship.
 */
@ApiModel(description = "This embedded object represents the role and type of each entity involved in a relationship.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-28T18:42:22.040Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssociationRole   {
  @JsonProperty("isSource")
  private Boolean isSource = null;

  @JsonProperty("role")
  private String role = null;

  @JsonProperty("entity")
  private EntityRef entity = null;

  public AssociationRole isSource(Boolean isSource) {
    this.isSource = isSource;
    return this;
  }

  /**
   * A flag indicating if the participant involved in a relationship is the source or not. If this flag is true for both roles in an association, the association is bi-directional (both end points are navigable)
   * @return isSource
  **/
  @ApiModelProperty(value = "A flag indicating if the participant involved in a relationship is the source or not. If this flag is true for both roles in an association, the association is bi-directional (both end points are navigable)")


  public Boolean isIsSource() {
    return isSource;
  }

  public void setIsSource(Boolean isSource) {
    this.isSource = isSource;
  }

  public AssociationRole role(String role) {
    this.role = role;
    return this;
  }

  /**
   * The association role of this relationship participant as defined in the associationRoleSpecification
   * @return role
  **/
  @ApiModelProperty(value = "The association role of this relationship participant as defined in the associationRoleSpecification")


  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public AssociationRole entity(EntityRef entity) {
    this.entity = entity;
    return this;
  }

  /**
   * The entity being referred to
   * @return entity
  **/
  @ApiModelProperty(value = "The entity being referred to")

  @Valid

  public EntityRef getEntity() {
    return entity;
  }

  public void setEntity(EntityRef entity) {
    this.entity = entity;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AssociationRole associationRole = (AssociationRole) o;
    return Objects.equals(this.isSource, associationRole.isSource) &&
        Objects.equals(this.role, associationRole.role) &&
        Objects.equals(this.entity, associationRole.entity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(isSource, role, entity);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AssociationRole {\n");
    
    sb.append("    isSource: ").append(toIndentedString(isSource)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("    entity: ").append(toIndentedString(entity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

