package io.swagger.association.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ec.com.claro.mscomposercreateorg.util.CustomerDeserializer;
import ec.com.claro.mscomposercreateorg.util.CustomerSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Association is the class that describes a relationship between two or more entities or entity specifications based on a given association specification. The role of each endpoint in the relationship is given by an association role. The type of endpoints in the relationship should match the ones as defined in the corresponding association role specification. A relationship between entity specifications may be governed by conditions and rules which are addressed by constraint references in this resource. Constraints for a relationship may include new rules and conditions in addition to those defined for the corresponding association specification
 */
@ApiModel(description = "Association is the class that describes a relationship between two or more entities or entity specifications based on a given association specification. The role of each endpoint in the relationship is given by an association role. The type of endpoints in the relationship should match the ones as defined in the corresponding association role specification. A relationship between entity specifications may be governed by conditions and rules which are addressed by constraint references in this resource. Constraints for a relationship may include new rules and conditions in addition to those defined for the corresponding association specification")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-28T18:42:22.040Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Association   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("action")
  private String action = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("lastUpdate")
  @JsonSerialize(using = CustomerSerializer.class)
  @JsonDeserialize(using = CustomerDeserializer.class)
  private OffsetDateTime lastUpdate = null;

  @JsonProperty("lifecycleStatus")
  private String lifecycleStatus = null;

  @JsonProperty("validFor")
  private TimePeriod validFor = null;

  @JsonProperty("associationRole")
  @Valid
  private List<AssociationRole> associationRole = null;

  public Association id(String id) {
    this.id = id;
    return this;
  }

  /**
   * unique identifier
   * @return id
  **/
  @ApiModelProperty(value = "unique identifier")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Association href(String href) {
    this.href = href;
    return this;
  }

  /**
   * Hyperlink reference
   * @return href
  **/
  @ApiModelProperty(value = "Hyperlink reference")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public Association name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name given to this association
   * @return name
  **/
  @ApiModelProperty(value = "Name given to this association")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Association description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Description of the association
   * @return description
  **/
  @ApiModelProperty(value = "Description of the association")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Association action(String action) {
    this.action = action;
    return this;
  }

  /**
   * Action to do it: Create, List
   * @return action
  **/
  @ApiModelProperty(value = "Action to do it: Create, List")


  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public Association type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Type of association: XRef, Catalog
   * @return type
  **/
  @ApiModelProperty(value = "Type of association: XRef, Catalog")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Association lastUpdate(OffsetDateTime lastUpdate) {
    this.lastUpdate = lastUpdate;
    return this;
  }

  /**
   * The last modified date of this association object
   * @return lastUpdate
  **/
  @ApiModelProperty(value = "The last modified date of this association object")

  @Valid

  public OffsetDateTime getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(OffsetDateTime lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public Association lifecycleStatus(String lifecycleStatus) {
    this.lifecycleStatus = lifecycleStatus;
    return this;
  }

  /**
   * Indicates the current lifecycle status
   * @return lifecycleStatus
  **/
  @ApiModelProperty(value = "Indicates the current lifecycle status")


  public String getLifecycleStatus() {
    return lifecycleStatus;
  }

  public void setLifecycleStatus(String lifecycleStatus) {
    this.lifecycleStatus = lifecycleStatus;
  }

  public Association validFor(TimePeriod validFor) {
    this.validFor = validFor;
    return this;
  }

  /**
   * The period for which this association is valid
   * @return validFor
  **/
  @ApiModelProperty(value = "The period for which this association is valid")

  @Valid

  public TimePeriod getValidFor() {
    return validFor;
  }

  public void setValidFor(TimePeriod validFor) {
    this.validFor = validFor;
  }

  public Association associationRole(List<AssociationRole> associationRole) {
    this.associationRole = associationRole;
    return this;
  }

  public Association addAssociationRoleItem(AssociationRole associationRoleItem) {
    if (this.associationRole == null) {
      this.associationRole = new ArrayList<AssociationRole>();
    }
    this.associationRole.add(associationRoleItem);
    return this;
  }

  /**
   * The end point roles of this association
   * @return associationRole
  **/
  @ApiModelProperty(value = "The end point roles of this association")

  @Valid
@Size(min=2) 
  public List<AssociationRole> getAssociationRole() {
    return associationRole;
  }

  public void setAssociationRole(List<AssociationRole> associationRole) {
    this.associationRole = associationRole;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Association association = (Association) o;
    return Objects.equals(this.id, association.id) &&
        Objects.equals(this.href, association.href) &&
        Objects.equals(this.name, association.name) &&
        Objects.equals(this.description, association.description) &&
        Objects.equals(this.action, association.action) &&
        Objects.equals(this.type, association.type) &&
        Objects.equals(this.lastUpdate, association.lastUpdate) &&
        Objects.equals(this.lifecycleStatus, association.lifecycleStatus) &&
        Objects.equals(this.validFor, association.validFor) &&
        Objects.equals(this.associationRole, association.associationRole);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, href, name, description, action, type, lastUpdate, lifecycleStatus, validFor, associationRole);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Association {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    action: ").append(toIndentedString(action)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    lastUpdate: ").append(toIndentedString(lastUpdate)).append("\n");
    sb.append("    lifecycleStatus: ").append(toIndentedString(lifecycleStatus)).append("\n");
    sb.append("    validFor: ").append(toIndentedString(validFor)).append("\n");
    sb.append("    associationRole: ").append(toIndentedString(associationRole)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

