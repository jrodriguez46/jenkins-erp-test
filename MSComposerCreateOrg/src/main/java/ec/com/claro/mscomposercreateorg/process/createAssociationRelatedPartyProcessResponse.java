package ec.com.claro.mscomposercreateorg.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.swagger.association.model.Association;
import io.swagger.association.model.GenericFault;

@Component
public class createAssociationRelatedPartyProcessResponse implements Processor {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Override
	public void process(Exchange exchange) throws Exception {
		String resultRP = "";
		if(exchange.getIn().getBody(String.class).contains("errorList")) {
			resultRP = "error";
			GenericFault genericFault = exchange.getIn().getBody(GenericFault.class);
			exchange.setProperty("genericFaultRP", genericFault);
			exchange.setProperty("responseFault", "RESPONSE_FAULT");
		}else {
			resultRP = "success";
			Association association = exchange.getIn().getBody(Association.class);
			exchange.setProperty("organizationRP", association);
			
		}
		log.info("resultRP = {}", resultRP);
		exchange.setProperty("resultRP", resultRP);
	}

}
