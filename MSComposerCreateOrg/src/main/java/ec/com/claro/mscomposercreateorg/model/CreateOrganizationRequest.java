package ec.com.claro.mscomposercreateorg.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.association.model.Association;
import io.swagger.organization.model.Organization;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Organization represents a group of people identified by shared interests or purpose. Examples include business, department and enterprise. Because of the complex nature of many businesses, both organizations and organization units are represented by the same data.
 */
@ApiModel(description = "Organization represents a group of people identified by shared interests or purpose. Examples include business, department and enterprise. Because of the complex nature of many businesses, both organizations and organization units are represented by the same data.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateOrganizationRequest   {

  @Override
	public String toString() {
		return "CreateOrganizationRequest [createOrganizationRequest=" + createOrganizationRequest
				+ ", listAssociationResponse=" + listAssociationResponse + "]";
	}

@JsonProperty("createOrganizationRequest")
  private Organization createOrganizationRequest = null;

  @JsonProperty("listAssociationResponse")
  @Valid
  private List<Association> listAssociationResponse = null;

  
  /**
   * Get createOrganizationRequest
   * @return createOrganizationRequest
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Organization getCreateOrganizationRequest() {
    return createOrganizationRequest;
  }

  public void setCreateOrganizationRequest(Organization createOrganizationRequest) {
    this.createOrganizationRequest = createOrganizationRequest;
  }
  
  
  /**
   * Get listAssociationResponse
   * @return listAssociationResponse
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Association> getListAssociationResponse() {
    return listAssociationResponse;
  }

  public void setListAssociationResponse(List<Association> listAssociationResponse) {
    this.listAssociationResponse = listAssociationResponse;
  }  
  
  public CreateOrganizationRequest addListAssociationResponseItem(Association listAssociationResponse) {
	    if (this.listAssociationResponse == null) {
	      this.listAssociationResponse = new ArrayList<Association>();
	    }
	    this.listAssociationResponse.add(listAssociationResponse);
	    return this;
	  }
}

