package ec.com.claro.mscomposercreateorg.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.fasterxml.jackson.databind.ObjectMapper;

import ec.com.claro.mscomposercreateorg.util.CommonMappings;

public class CreateOrganizationSGAProcessResponseFault implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		final Throwable ex = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
		
		exchange.getOut().setBody(new ObjectMapper().writeValueAsString(exchange.getProperty("CreateOrganizationRequest")));
		exchange.getOut().setHeader("rabbitmq.ROUTING_KEY", "failed");
		exchange.getOut().setHeader("rabbitmq.EXCHANGE_NAME", "Failed");
		
		exchange.getOut().setHeader("errorMessage", ex.getMessage());
		exchange.getOut().setHeader("errorActor", "composerCreate");
		exchange.getOut().setHeader(CommonMappings.REQUEST_DATE, exchange.getProperty(CommonMappings.REQUEST_DATE).toString());
		exchange.getOut().setHeader(CommonMappings.MESSAGE_ID, exchange.getProperty(CommonMappings.MESSAGE_ID));
		exchange.getOut().setHeader(CommonMappings.CORRELATOR_ID, exchange.getProperty(CommonMappings.CORRELATOR_ID));
		exchange.getOut().setHeader("MS", "MSComposerCreateOrg");
		
	}

}
