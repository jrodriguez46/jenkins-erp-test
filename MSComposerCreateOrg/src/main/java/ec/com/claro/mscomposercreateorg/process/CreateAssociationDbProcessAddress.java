package ec.com.claro.mscomposercreateorg.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import ec.com.claro.mscomposercreateorg.model.CreateOrganizationRequest;
import ec.com.claro.mscomposercreateorg.util.CommonMappings;
import io.swagger.association.model.Association;
import io.swagger.association.model.AssociationRole;
import io.swagger.association.model.Characteristic;
import io.swagger.association.model.EntityRef;
import io.swagger.association.model.RelatedParty;
import io.swagger.organization.model.ContactMedium;
import io.swagger.organization.model.Organization;

@Component
public class CreateAssociationDbProcessAddress implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		CommonMappings commonMappings = new CommonMappings();
		
		// Se obtiene de las propiedades el body de inicio
		CreateOrganizationRequest createOrganizationRequest0 = 
			exchange.getProperty("CreateOrganizationRequest",CreateOrganizationRequest.class);
		Organization createOrganizationRequest = createOrganizationRequest0.getCreateOrganizationRequest();
		
		Organization organizationSgaResp = exchange.getProperty("organizationSGA",Organization.class);
		Organization organizationAxisResp = exchange.getProperty("organizationAXIS",Organization.class);		
		
		List<Association> listAssociation = new ArrayList<>();
		
		// Obtener Id de direccion 
		String addressId = "";
		if (createOrganizationRequest.getContactMedium() != null) {
			for (ContactMedium contactMedium : createOrganizationRequest.getContactMedium()) {
				if(!contactMedium.getMediumType().equalsIgnoreCase("emailAddress") &&
						!contactMedium.getMediumType().equalsIgnoreCase("telephoneNumber") &&
						!contactMedium.getMediumType().equalsIgnoreCase("mobileNumber") &&
						!contactMedium.getMediumType().equalsIgnoreCase("webSite") &&
						!contactMedium.getMediumType().equalsIgnoreCase("faxNumber") && 
						contactMedium.getAddress() != null) {
							addressId = contactMedium.getAddress().getId();
				}
			}
		}
		
		final String ROLE_ASSOCIATION = "AddressId";
		final String VALUE_TYPE_CHAR = "Attribute";
		Association association = new Association();
		association.setName(ROLE_ASSOCIATION + addressId + "Assn");
		association.setDescription("Asociación de dirección " + addressId);
		association.setType("XRef");		
		AssociationRole associationRole = new AssociationRole();
		
		if (addressId != null && !addressId.equals("")) {
			// MSComposerCreateOrg 
			associationRole.setRole(ROLE_ASSOCIATION + addressId + "AssnRoleSF");
			associationRole.setIsSource(true);
			// Entity
			EntityRef entity = new EntityRef();
			// Characteristic
			Characteristic characteristic = new Characteristic();
			characteristic.setName("id");
			characteristic.setValue(addressId);
			characteristic.setValueType(VALUE_TYPE_CHAR);
			entity.addCharacteristicItem(characteristic);
			entity.setDescription("Rol de Asociación de dirección SalesForce " + addressId);
			entity.setName("Address");
			// RelatedParty
			RelatedParty relatedParty = new RelatedParty();
			relatedParty.setName("SalesForce");
			entity.setRelatedParty(relatedParty);
			associationRole.setEntity(entity);			
			association.addAssociationRoleItem(associationRole);
		}
		
		// MSCreateOrganizationAxis
		// Obtener Id de direccion de Axis
		String addressExternalIdAxis = "";
		if (organizationAxisResp != null && organizationAxisResp.getContactMedium() != null) {
			for (ContactMedium contactMediumAxisRes : organizationAxisResp.getContactMedium()) {
				if (contactMediumAxisRes.getAddress() != null) {
					addressExternalIdAxis = contactMediumAxisRes.getAddress().getExternalId();
					// AssociationRole
					associationRole = new AssociationRole();
					associationRole.setRole(ROLE_ASSOCIATION + addressExternalIdAxis + "AssnRoleAxis");
					associationRole.setIsSource(false);
					// Entity
					EntityRef entityAxis = new EntityRef();
					entityAxis.setDescription("Rol de Asociación de dirección Axis " + addressExternalIdAxis);
					entityAxis.setName("UBICACIONES");
					// Characteristic
					Characteristic characteristicAxis = new Characteristic();
					characteristicAxis.setName("PV_IDADDRES");
					characteristicAxis.setValue(addressExternalIdAxis);
					characteristicAxis.setValueType(VALUE_TYPE_CHAR);
					entityAxis.addCharacteristicItem(characteristicAxis);		
					// RelatedParty
					RelatedParty relatedPartyAxis = new RelatedParty();
					relatedPartyAxis.setName("AXIS");
					entityAxis.setRelatedParty(relatedPartyAxis);		
					associationRole.setEntity(entityAxis);
					association.addAssociationRoleItem(associationRole);
				}
			}
		}
		
		
		//MSCreateOrganizationSGA
		// Obtener Id de direccion de SGA
		String addressExternalIdSga = "";
		if (organizationSgaResp != null && organizationSgaResp.getContactMedium() != null) {
			for (ContactMedium contactMediumSgaRes : organizationSgaResp.getContactMedium()) {
				if (contactMediumSgaRes.getAddress() != null) {
					addressExternalIdSga = contactMediumSgaRes.getAddress().getExternalId();
					// AssociationRole
					associationRole = new AssociationRole();
					associationRole.setRole(ROLE_ASSOCIATION + addressExternalIdSga + "AssnRoleSGA");
					associationRole.setIsSource(false);
					// Entity
					EntityRef entitySga = new EntityRef();
					entitySga.setDescription("Rol de Asociación de dirección SGA " + addressExternalIdSga);
					entitySga.setName("UBICACIONES");
					// Characteristic
					Characteristic characteristicSga = new Characteristic();
					characteristicSga.setName("id");
					characteristicSga.setValue(addressExternalIdSga);
					characteristicSga.setValueType(VALUE_TYPE_CHAR);
					entitySga.addCharacteristicItem(characteristicSga);	
					// RelatedParty
					RelatedParty relatedPartySga = new RelatedParty();
					relatedPartySga.setName("SGA");
					entitySga.setRelatedParty(relatedPartySga);
					associationRole.setEntity(entitySga);
					association.addAssociationRoleItem(associationRole);	
				}
			}
		}
		
		listAssociation.add(association);	
		commonMappings.setHeaderRequest(exchange);
		exchange.getOut().setHeader("rabbitmq.ROUTING_KEY", "CreateAssociationKey");
		exchange.getOut().setHeader("rabbitmq.EXCHANGE_NAME", "CreateAssociationExch");
		exchange.getOut().setHeader("MS", "MSComposerCreateOrg");
		exchange.getOut().setBody(listAssociation);

	}

}
