package ec.com.claro.mscomposercreateorg.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.swagger.organization.model.Organization;

@Component
public class CreateOrganizationSGAProcessResponse implements Processor {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Override
	public void process(Exchange exchange) throws Exception {
		
		log.info("1"+exchange.getIn().getBody(io.swagger.organization.model.GenericFault.class));
		String resultSGA = "";
		int responseCode = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE, Integer.class);
		if (responseCode >= 200 && responseCode < 300) {			
			resultSGA = "success";
			Organization organization = exchange.getIn().getBody(Organization.class);
			exchange.setProperty("organizationSGA", organization);
		}else {
			resultSGA = "error";
			io.swagger.organization.model.GenericFault genericFault = exchange.getIn().getBody(io.swagger.organization.model.GenericFault.class);
			exchange.setProperty("genericFaultSGA", genericFault);
			exchange.setProperty("responseFault", "RESPONSE_FAULT");
			
		}
		
		log.info("resultSGA = {}", resultSGA);
		exchange.setProperty("resultSGA", resultSGA);
	}

}
