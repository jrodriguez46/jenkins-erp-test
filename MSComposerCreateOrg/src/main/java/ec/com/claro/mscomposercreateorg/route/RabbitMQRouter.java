package ec.com.claro.mscomposercreateorg.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ec.com.claro.mscomposercreateorg.model.CreateOrganizationRequest;
import ec.com.claro.mscomposercreateorg.process.CreateAssociationDbProcessAddress;
import ec.com.claro.mscomposercreateorg.process.CreateAssociationDbProcessRelatedParty;
import ec.com.claro.mscomposercreateorg.process.CreateOrganizationAxisProcessRequest;
import ec.com.claro.mscomposercreateorg.process.CreateOrganizationAxisProcessResponse;
import ec.com.claro.mscomposercreateorg.process.createAssociationRelatedPartyProcessResponse;
import ec.com.claro.mscomposercreateorg.process.createAssociationAdressProcessResponse;
import ec.com.claro.mscomposercreateorg.process.CreateOrganizationSGAProcessRequest;
import ec.com.claro.mscomposercreateorg.process.CreateOrganizationSGAProcessResponse;
import ec.com.claro.mscomposercreateorg.process.CreateOrganizationSGAProcessResponseFault;
import ec.com.claro.mscomposercreateorg.process.SendNotifyProcessReq;
import io.swagger.association.model.Association;
import io.swagger.organization.model.GenericFault;
import io.swagger.organization.model.Organization;

@Component
public class RabbitMQRouter extends RouteBuilder {
	
	String msNotify = System.getenv().get("ENDPOINT_REST_NOTIFY") + "?throwExceptionOnFailure=false";
	//String msNotify = "http://localhost:9087/MS/PAR/Party/RSMSNotify/v1/Notify/sendNotify"+"?throwExceptionOnFailure=false";
	
	@Value("${endpoint.rabbitmq.composercreateorg}")
	private  String endpointRabbitMQ;
	
	@Autowired
	CreateOrganizationSGAProcessRequest createOrganizationSGAProcessRequest;
	
	@Autowired
	CreateOrganizationSGAProcessResponse createOrganizationSGAProcessResponse;
	
	@Autowired
	CreateOrganizationAxisProcessRequest createOrganizationAxisProcessRequest;
	@Autowired
	createAssociationRelatedPartyProcessResponse createAssociationRelatedPartyProcessResponse;
	@Autowired
	createAssociationAdressProcessResponse createAssociationAdressProcessResponse;
	@Autowired
	CreateOrganizationAxisProcessResponse createOrganizationAxisProcessResponse;
	@Autowired
	SendNotifyProcessReq Notify;
	
	@Autowired
	CreateAssociationDbProcessRelatedParty createAssociationDbProcessRelatedParty;
	
	@Autowired
	CreateAssociationDbProcessAddress createAssociationDbProcessAddress;
	
	private JacksonDataFormat jdfCOReq = new JacksonDataFormat(CreateOrganizationRequest.class);	
	private JacksonDataFormat jdfOrg = new JacksonDataFormat(Organization.class);	
	private JacksonDataFormat jdfGFault = new JacksonDataFormat(GenericFault.class);	
	private JacksonDataFormat jdfGFaultAss = new JacksonDataFormat(io.swagger.association.model.GenericFault.class);
	private JacksonDataFormat jdfAssoc = new ListJacksonDataFormat(Association.class);
	@Override
    public void configure() throws Exception {
		
		
		
		from(endpointRabbitMQ +"/MSComposerCreateOrg_Exch?autoDelete=false&queue=MSComposerCreateOrg_Queue&skipQueueDeclare=false&declare=true&routingKey=MSComposerCreateOrg_Rkey")
		.unmarshal(jdfCOReq)
		.to("direct:createOrganizationIni");
    	
		from("direct:createOrganizationIni")
			.doTry()
				.to("direct:createOrganization")
				
			.doCatch(Exception.class)
				.process(new CreateOrganizationSGAProcessResponseFault())
				.log("Finaliza Fault >>>>> MSComposerCreateOrg")
				.inOnly(endpointRabbitMQ + "/Failed?autoDelete=false&queue=failed&skipQueueDeclare=false&declare=true")
			/*
				.choice()
				
					.when(simple("${property[responseFault]} == 'RESPONSE_FAULT'"))
					.log("Inicia >>>>> MSNotifyCompCreate")
					.process(Notify)
					.log("body notify>> ${body}" )
					.to(msNotify)
					.log("END >>>>> MSNotifyCompCreate")
				
		        .end()
			*/	
				.endRest();
	
		
    	
		from("direct:createOrganization")
    		.log("START >>>>> MSComposerCreateOrg")
    		
    		// Proceso para crear la Organizacion en SGA
    		.process(createOrganizationSGAProcessRequest)    		
    		.marshal(jdfOrg)    
    		.log("createOrganizationSGAProcessRequest >>>>> ${body}")
    		.inOut(endpointRabbitMQ +"/MSCreateOrganizationSGA_Exch?autoDelete=false&queue=MSCreateOrganizationSGA_Queue&skipQueueDeclare=false&declare=true&requestTimeout=120000")
    		
    		.log("createOrganizationSGAProcessResponse >>>>> ${body}")
    		.choice()
    			.when().simple("${header.CamelHttpResponseCode} == '201'")					
					.unmarshal(jdfOrg)	
				.otherwise()
						.unmarshal(jdfGFault)	
			.end()
			
    		.process(createOrganizationSGAProcessResponse)
    		
    		// Proceso para crear la Organizacion en Axis
    		.process(createOrganizationAxisProcessRequest)
    		.marshal(jdfOrg)
    		.log("createOrganizationAxisProcessRequest >>>>> ${body}")
    		.inOut(endpointRabbitMQ +"/createOrgAxisExch?autoDelete=false&queue=createOrgAxisReq&declare=true&skipQueueDeclare=false&requestTimeout=190000")
    		.log("createOrganizationAxisProcessResponse >>>>> ${body}")
    		.choice()
    			.when(bodyAs(String.class).contains("errorList"))
					.log("Generic Fault Axis")
					.unmarshal(jdfGFault)						
				.otherwise()
					.log("Organization Axis")
					.unmarshal(jdfOrg)	
			.end()
    		.process(createOrganizationAxisProcessResponse)
    		
    		
    		//Proceso para Crear Association DB RelatedParty
    		.log("Lanzar >>>>>  createAssociationDbProcessRelatedParty")
			.process(createAssociationDbProcessRelatedParty)
			.marshal(jdfAssoc)
			.log("createAssociationDbProcessRelatedParty >>>>> ${body}")
			.inOut(endpointRabbitMQ +"/CreateAssociationExch?autoDelete=false&queue=CreateAssociation&skipQueueDeclare=false&declare=true&skipQueueDeclare=false&requestTimeout=190000")
			.log("createAsociation 1 >>>>> ${body}")
			.choice()
			.when(bodyAs(String.class).contains("errorList"))
				.log("Generic Fault RP")
				.unmarshal(jdfGFaultAss)						
			.otherwise()
				.log("Organization RP")
				.unmarshal(jdfAssoc)	
		    .end()
		   
		    .process(createAssociationRelatedPartyProcessResponse)
			//Proceso para Crear Association DB Address
		    
			.process(createAssociationDbProcessAddress)
			.marshal(jdfAssoc)
			.log("createAssociationDbProcessAddress >>>>> ${body}")
			.inOut(endpointRabbitMQ +"/CreateAssociationExch?autoDelete=false&queue=CreateAssociation&skipQueueDeclare=false&declare=true&skipQueueDeclare=false&requestTimeout=190000")
			.log("createAssociation 2 res>>>>> ${body}")
			/*.choice()
			.when(bodyAs(String.class).contains("errorList"))
				.log("Generic Fault Adress")
				.unmarshal(jdfGFaultAss)						
			.otherwise()
				.log("Organization Adress")
				.unmarshal(jdfAssoc)	
		    .end()
		    */
		    .process(createAssociationAdressProcessResponse)
			.log("END >>>>> MSComposerCreateOrg")
			
			
			.choice()
				
				.when(simple("${property[responseFault]} == 'RESPONSE_FAULT'"))
				.log("Inicia >>>>> MSNotifyCompCreate")
				.process(Notify)
				.log("body notify>> ${body}" )
				.to(msNotify)
				.log("END >>>>> MSNotifyCompCreate")
				
		     .end()
			
			
			
			.end();
		
		
    }
}

