package ec.com.claro.mscomposercreateorg.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import ec.com.claro.mscomposercreateorg.model.CreateOrganizationRequest;
import ec.com.claro.mscomposercreateorg.util.CommonMappings;
import io.swagger.association.model.Association;
import io.swagger.association.model.AssociationRole;
import io.swagger.association.model.Characteristic;
import io.swagger.association.model.EntityRef;
import io.swagger.association.model.RelatedParty;
import io.swagger.organization.model.Organization;
import io.swagger.organization.model.RelatedPartyRef;

@Component
public class CreateAssociationDbProcessRelatedParty implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {			
		// Se obtiene de las propiedades el body de inicio
		CreateOrganizationRequest createOrganizationRequest0 = 
						exchange.getProperty("CreateOrganizationRequest",CreateOrganizationRequest.class);
		Organization createOrganizationRequest = createOrganizationRequest0.getCreateOrganizationRequest();
		
		Organization organizationSgaResp = exchange.getProperty("organizationSGA",Organization.class);
		Organization organizationAxisResp = exchange.getProperty("organizationAXIS",Organization.class);
		
		List<Association> listAssociation = new ArrayList<>();
		final String ROLE_ASSOCIATION = "RelatedPartyCustomer";
		final String VALUE_TYPE_CHAR = "Attribute";
		final String ROLE_ACCOUNT = "account";
		
		// Obtener Id del Role
		String roleId = "";
		if (createOrganizationRequest.getRelatedParty() != null) {
			for (RelatedPartyRef relatedParty : createOrganizationRequest.getRelatedParty()) {
				if (!relatedParty.getRole().equalsIgnoreCase(ROLE_ACCOUNT)) {
					roleId = relatedParty.getId();
				}
			}
		}				
		
		Association association = new Association();
		association.setName(ROLE_ASSOCIATION + roleId + "Assn");
		association.setDescription("Asociación del cliente " + roleId);
		association.setType("XRef");
		AssociationRole associationRole = new AssociationRole();
		
		if (roleId != null && !roleId.equals("")) {
			// MSComposerCreateOrg 				
			associationRole.setRole(ROLE_ASSOCIATION + roleId + "AssnRoleSF");
			associationRole.setIsSource(true);
			// Entity
			EntityRef entity = new EntityRef();
			// Characteristic
			Characteristic characteristic = new Characteristic();
			characteristic.setName("id");
			characteristic.setValue(roleId);
			characteristic.setValueType(VALUE_TYPE_CHAR);
			entity.addCharacteristicItem(characteristic);
			entity.setDescription("Rol de Asociación de cliente SalesForce" + roleId);
			entity.setName("RelatedParty");
			// RelatedParty
			RelatedParty relatedParty = new RelatedParty();
			relatedParty.setName("SalesForce");
			entity.setRelatedParty(relatedParty);
			associationRole.setEntity(entity);			
			association.addAssociationRoleItem(associationRole);
		}
		
		// MSCreateOrganizationAxis
		// Obtener Id del Role de Axis
		String roleIdAxis = "";
		if (organizationAxisResp != null && organizationAxisResp.getRelatedParty() != null) {
			for (RelatedPartyRef relatedPartyAxisRes : organizationAxisResp.getRelatedParty()) {
				if (!relatedPartyAxisRes.getRole().equalsIgnoreCase(ROLE_ACCOUNT)) {
					roleIdAxis = relatedPartyAxisRes.getId();
					// AssociationRole
					associationRole = new AssociationRole();
					associationRole.setRole(ROLE_ASSOCIATION + roleIdAxis + "AssnRoleAxis");
					associationRole.setIsSource(false);
					// Entity
					EntityRef entityAxis = new EntityRef();
					entityAxis.setDescription("Rol de Asociación de cliente Axis" + roleIdAxis);
					entityAxis.setName("CLIENTE");
					// Characteristic
					Characteristic characteristicAxis = new Characteristic();
					characteristicAxis.setName("PV_IDCLIENTE");
					characteristicAxis.setValue(roleIdAxis);
					characteristicAxis.setValueType(VALUE_TYPE_CHAR);
					entityAxis.addCharacteristicItem(characteristicAxis);
						
					// RelatedParty
					RelatedParty relatedPartyAxis = new RelatedParty();
					relatedPartyAxis.setName("AXIS");
					entityAxis.setRelatedParty(relatedPartyAxis);
						
					associationRole.setEntity(entityAxis);
					association.addAssociationRoleItem(associationRole);
				}
			}
		}
		
		
		//MSCreateOrganizationSGA
		// Obtener Id del Role de SGA
		String roleIdSga = "";
		if (organizationSgaResp != null && organizationSgaResp.getRelatedParty() != null) {
			for (RelatedPartyRef relatedPartySgaRes : organizationSgaResp.getRelatedParty()) {
				if (!relatedPartySgaRes.getRole().equalsIgnoreCase(ROLE_ACCOUNT)) {
					roleIdSga = relatedPartySgaRes.getId();
					// AssociationRole
					associationRole = new AssociationRole();
					associationRole.setRole(ROLE_ASSOCIATION + roleIdSga + "AssnRoleSGA");
					associationRole.setIsSource(false);
					// Entity
					EntityRef entitySga = new EntityRef();
					entitySga.setDescription("Rol de Asociación de cliente SGA" + roleIdSga);
					entitySga.setName("cliente");
					// Characteristic
					Characteristic characteristicSga = new Characteristic();
					characteristicSga.setName("identificador");
					characteristicSga.setValue(roleIdSga);
					characteristicSga.setValueType(VALUE_TYPE_CHAR);
					entitySga.addCharacteristicItem(characteristicSga);	
					RelatedParty relatedPartySga = new RelatedParty();
					relatedPartySga.setName("SGA");
					entitySga.setRelatedParty(relatedPartySga);
					associationRole.setEntity(entitySga);
					association.addAssociationRoleItem(associationRole);
				}
			}
		}
		
		
		listAssociation.add(association);				

		//Set Headers Request
		CommonMappings commonMappings = new CommonMappings();
		commonMappings.setHeaderRequest(exchange);
		exchange.getOut().setHeader("rabbitmq.ROUTING_KEY", "CreateAssociationKey");
		exchange.getOut().setHeader("rabbitmq.EXCHANGE_NAME", "CreateAssociationExch");
		exchange.getOut().setHeader("MS", "MSComposerCreateOrg");
		exchange.getOut().setBody(listAssociation);
	}

}
