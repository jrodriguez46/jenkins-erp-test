package ec.com.claro.mscomposercreateorg.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import ec.com.claro.mscomposercreateorg.model.CreateOrganizationRequest;
import ec.com.claro.mscomposercreateorg.model.Notify;
import ec.com.claro.mscomposercreateorg.util.CommonMappings;
import io.swagger.association.model.GenericFault;
import io.swagger.organization.model.Organization;

@Component
public class SendNotifyProcessReq implements Processor {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Override
	public void process(Exchange exchange) throws Exception {
		
		CommonMappings commonMappings = new CommonMappings();
		
		// Se obtiene de las propiedades el body de inicio
		CreateOrganizationRequest createOrganizationRequest0 = 
			exchange.getProperty("CreateOrganizationRequest",CreateOrganizationRequest.class);
	Organization requestOrg = createOrganizationRequest0.getCreateOrganizationRequest();
	if(requestOrg!=null) {	
	log.info("request>>>"+ requestOrg.toString());
	}
		//log.info(".tostring>>"+exchange.getProperty("CreateOrganizationRequest").toString());
		
		io.swagger.organization.model.GenericFault organizationSgaResp = exchange.getProperty("genericFaultSGA",io.swagger.organization.model.GenericFault.class);
		if(organizationSgaResp!=null) {
		log.info("responseSGA>>>"+ organizationSgaResp.toString());
		}
		io.swagger.organization.model.GenericFault organizationAxisResp = exchange.getProperty("genericFaultAXIS",io.swagger.organization.model.GenericFault.class);		
		if(organizationAxisResp!=null) {
		log.info("responseAXIS>>>"+ organizationAxisResp.toString());
		}
		GenericFault organizationRP = exchange.getProperty("genericFaultRP",GenericFault.class);		
		if(organizationRP!=null) {
		log.info("responseRP>>>"+ organizationRP.toString());
		}
		
		GenericFault organizationAdress = exchange.getProperty("genericFaultAdress",GenericFault.class);		
		if(organizationAdress!=null) {
		log.info("responseADRESS>>>"+ organizationAdress.toString());
		}
		Notify mensaje = new Notify();
		
		mensaje.setRequest(requestOrg);
		List<io.swagger.organization.model.GenericFault> respuestaOrg =new ArrayList<io.swagger.organization.model.GenericFault>();
		respuestaOrg.add(organizationAxisResp);
		respuestaOrg.add(organizationSgaResp);
		mensaje.setResponseOrg(respuestaOrg);
		
		
		List<io.swagger.association.model.GenericFault> respuestaList =new ArrayList<io.swagger.association.model.GenericFault>();
		respuestaList.add(organizationRP);
		respuestaList.add(organizationAdress);
		mensaje.setResponseList(respuestaList);
		
		String jsonMensaje = new ObjectMapper().writeValueAsString(mensaje);
		
		
		commonMappings.setHeaderRequest(exchange);
		exchange.getOut().setHeader("Subject", "Fault-Notificacion MAIL-PartyManaement-Metodo POST");
		exchange.getOut().setBody(jsonMensaje);
	}

}
