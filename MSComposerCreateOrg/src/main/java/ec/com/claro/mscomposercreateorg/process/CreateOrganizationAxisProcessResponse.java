package ec.com.claro.mscomposercreateorg.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import io.swagger.organization.model.Organization;

@Component
public class CreateOrganizationAxisProcessResponse implements Processor {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Override
	public void process(Exchange exchange) throws Exception {
		String resultAXIS = "";
		if(exchange.getIn().getBody(String.class).contains("errorList")) {
			resultAXIS = "error";
			io.swagger.organization.model.GenericFault genericFault = exchange.getIn().getBody(io.swagger.organization.model.GenericFault.class);
			exchange.setProperty("genericFaultAXIS", genericFault);
			exchange.setProperty("responseFault", "RESPONSE_FAULT");
		}else {
			resultAXIS = "success";
			Organization organization = exchange.getIn().getBody(Organization.class);
			exchange.setProperty("organizationAXIS", organization);
			
		}
		log.info("resultAXIS = {}", resultAXIS);
		exchange.setProperty("resultAXIS", resultAXIS);
	}

}
