package ec.com.claro.mscomposercreateorg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = "ec.com.claro.mscomposercreateorg")
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
    }
}