package ec.com.claro.mscomposercreateorg.process;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import ec.com.claro.mscomposercreateorg.model.CreateOrganizationRequest;
import ec.com.claro.mscomposercreateorg.util.CommonMappings;
import io.swagger.association.model.Association;
import io.swagger.organization.model.Area;
import io.swagger.organization.model.CalendarPeriod;
import io.swagger.organization.model.Characteristic;
import io.swagger.organization.model.ContactMedium;
import io.swagger.organization.model.ExternalReference;
import io.swagger.organization.model.GeographicAddressRef;
import io.swagger.organization.model.GeographicAddressSpec;
import io.swagger.organization.model.GeographicLocation;
import io.swagger.organization.model.GeographicSubAddress;
import io.swagger.organization.model.LocationCharacteristic;
import io.swagger.organization.model.MediumCharacteristic;
import io.swagger.organization.model.Note;
import io.swagger.organization.model.Organization;
import io.swagger.organization.model.OrganizationIdentification;
import io.swagger.organization.model.PartyProfileType;
import io.swagger.organization.model.RelatedPartyRef;
import io.swagger.organization.model.TimePeriod;

@Component
public class CreateOrganizationSGAProcessRequest implements Processor {
	@Override
	public void process(Exchange exchange) throws Exception {
		
		//Save Headers Request
		CommonMappings commonMappings = new CommonMappings();
		commonMappings.saveHeaders(exchange);
		
		// Se convierte el Mensaje Body al objeto CreateOrganizationRequest
		CreateOrganizationRequest createOrganizationRequest = exchange.getIn().getBody(CreateOrganizationRequest.class);
		// Se guarda el mensaje de entrada
		exchange.setProperty("CreateOrganizationRequest", createOrganizationRequest);
		
		// Se crea el objeto Organization para enviar como request
		Organization organization = this.mappingRequestSGA(createOrganizationRequest.getCreateOrganizationRequest(),
				createOrganizationRequest.getListAssociationResponse());
		
		//Set Headers Request
		commonMappings.setHeaderRequest(exchange);
		exchange.getOut().setHeader("rabbitmq.CORRELATIONID", exchange.getProperty("rabbitMQCorrelatorId"));
		exchange.getOut().setHeader("rabbitmq.EXCHANGE_NAME", "MSCreateOrganizationSGA_Exch");
		exchange.getOut().setHeader("rabbitmq.ROUTING_KEY", "MSCreateOrganizationSGA_Rkey");
		
		exchange.getOut().setBody(organization);
	}
	
	public Organization mappingRequestSGA(Organization createOrganizationRequest, List<Association> listAssociationResponse) {
		//// Obtener valores del catalogo de asociacion 
		StringBuilder associationLocalityId = new StringBuilder();
		StringBuilder associationStateOrProvinceId = new StringBuilder();
		StringBuilder associationCountryId = new StringBuilder();
		StringBuilder associationIdentificationType = new StringBuilder();
		StringBuilder checkDigit = new StringBuilder();
		StringBuilder associationValueTypeTaxpayer = new StringBuilder();
		StringBuilder roleRelatedParty = new StringBuilder();
		StringBuilder partyRankRelatedParty = new StringBuilder();
		StringBuilder categorySectorPartyProfileType = new StringBuilder();
		StringBuilder associationStatus = new StringBuilder();
		StringBuilder associationCityId = new StringBuilder();
		final String NAME_RELATED_PARTY = "SalesForce";
		
		if (listAssociationResponse != null) {
			for (Association association : listAssociationResponse) {
				// busqueda en el catalogo para Identificador de la localidad
				StringBuilder nameAssociationLocalityId = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("Locality")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("id"))
						.forEach(p -> nameAssociationLocalityId.append(association.getName()));
				if (!nameAssociationLocalityId.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> associationLocalityId.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				// busqueda en el catalogo para Identificador de la provincia o estado
				StringBuilder nameAssociationStateOrProvinceId = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("StateOrProvince")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("id"))
						.forEach(p -> nameAssociationStateOrProvinceId.append(association.getName()));
				if (!nameAssociationStateOrProvinceId.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> associationStateOrProvinceId.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				// nameCountry
				StringBuilder nameAssociationCountry = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("Country")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("name"))
						.forEach(p -> nameAssociationCountry.append(association.getName()));
				if (!nameAssociationCountry.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> associationCountryId.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				// busqueda en el catalogo para Tipo de documento de identificación
				StringBuilder nameAssociationIdentificationType = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("OrganizationIdentification")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("identificationType"))
						.forEach(p -> nameAssociationIdentificationType.append(association.getName()));
				if (!nameAssociationIdentificationType.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> associationIdentificationType.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				// checkDigit
				StringBuilder nameAssociationCheckDigit = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("OrganizationIdentification")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("checkDigit"))
						.forEach(p -> nameAssociationCheckDigit.append(association.getName()));	
				if (!nameAssociationCheckDigit.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> checkDigit.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				// busqueda en el catalogo para Valor de la característica de la organización
				StringBuilder nameAssociationValueTypeTaxpayer = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("PartyCharacteristic")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("valueTypeTaxpayer"))
						.forEach(p -> nameAssociationValueTypeTaxpayer.append(association.getName()));
				if (!nameAssociationValueTypeTaxpayer.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> associationValueTypeTaxpayer.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				// roleRelatedParty				
				StringBuilder nameAssociationRole = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("RelatedParty")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("role"))
						.forEach(p -> nameAssociationRole.append(association.getName()));	
				if (!nameAssociationRole.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> roleRelatedParty.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				//partyRankRelatedParty	
				StringBuilder nameAssociationPartyRank = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("RelatedParty")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("partyRank"))
						.forEach(p -> nameAssociationPartyRank.append(association.getName()));	
				if (!nameAssociationPartyRank.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA")).forEach(
									p -> partyRankRelatedParty.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				// categorySectorPartyProfileType	
				StringBuilder nameAssociationCategorySector = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("PartyProfileType")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("categorySector"))
						.forEach(p -> nameAssociationCategorySector.append(association.getName()));	
				if (!nameAssociationCategorySector.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> categorySectorPartyProfileType
									.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				// busqueda en el catalogo para Estado de la organización
				StringBuilder nameAssociationStatus = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("Organization")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("status"))
						.forEach(p -> nameAssociationStatus.append(association.getName()));
				if (!nameAssociationStatus.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> associationStatus.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
				
				// busqueda en el catalogo para Identificador de la City
				StringBuilder nameAssociationCityId = new StringBuilder();
				association.getAssociationRole().stream()
						.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase(NAME_RELATED_PARTY)
								&& p.getEntity().getName().equalsIgnoreCase("City")
								&& p.getEntity().getCharacteristic().get(0).getName().equalsIgnoreCase("id"))
						.forEach(p -> nameAssociationCityId.append(association.getName()));
				if (!nameAssociationCityId.toString().equalsIgnoreCase("")) {	
					association.getAssociationRole().stream()
							.filter(p -> p.getEntity().getRelatedParty().getName().equalsIgnoreCase("SGA"))
							.forEach(p -> associationCityId.append(p.getEntity().getCharacteristic().get(0).getValue()));	
				}
			}
		}
		
		// Mapping Organization
		Organization organization = new Organization();		
		organization.setId(createOrganizationRequest.getId());
		organization.setExternalId(createOrganizationRequest.getExternalId());
		organization.setHref(createOrganizationRequest.getHref());
		organization.setIsHeadOffice(createOrganizationRequest.isIsHeadOffice());
		organization.setIsLegalEntity(createOrganizationRequest.isIsLegalEntity());
		organization.setName(createOrganizationRequest.getName());
		organization.setNameType(createOrganizationRequest.getNameType());
		organization.setOrganizationType(createOrganizationRequest.getOrganizationType());
		organization.setTradingName(createOrganizationRequest.getTradingName());
		// El Valor de status se consulta de listAssociationResponse
		if(!associationStatus.toString().equalsIgnoreCase(""))
			organization.status(associationStatus.toString());
		else
			organization.status(createOrganizationRequest.getStatus());
		organization.setOwner(createOrganizationRequest.getOwner());
		organization.setCreateDateTime(createOrganizationRequest.getCreateDateTime());
		organization.setLastUpdateDateTime(createOrganizationRequest.getLastUpdateDateTime());
		organization.setCreatedBy(createOrganizationRequest.getCreatedBy());
		organization.setLastUpdateBy(createOrganizationRequest.getLastUpdateBy());
		organization.setState(createOrganizationRequest.getState());
		organization.setBaseType(createOrganizationRequest.getBaseType());
		organization.setSchemaLocation(createOrganizationRequest.getSchemaLocation());
		organization.setType(createOrganizationRequest.getType());
		
		// ExistsDuring
		if (createOrganizationRequest.getExistsDuring() != null) {
			TimePeriod existsDuring = new TimePeriod();
			existsDuring.setEndDateTime(createOrganizationRequest.getExistsDuring().getEndDateTime());
			existsDuring.setStartDateTime(createOrganizationRequest.getExistsDuring().getStartDateTime());
			organization.setExistsDuring(existsDuring);
		}
		
		// ContactMedium
		if (createOrganizationRequest.getContactMedium() != null) {
			for (ContactMedium contactMediumCOR:createOrganizationRequest.getContactMedium()) {
				// ContactMedium
				ContactMedium contactMedium = new ContactMedium();				
				contactMedium.setMediumType(contactMediumCOR.getMediumType());
				contactMedium.setPreferred(contactMediumCOR.isPreferred());
				
				//Characteristic
				if (contactMediumCOR.getCharacteristic() != null) {
					MediumCharacteristic characteristic = new MediumCharacteristic();
					characteristic.setCity(contactMediumCOR.getCharacteristic().getCity());
					characteristic.setContactType(contactMediumCOR.getCharacteristic().getContactType());
					characteristic.setCountry(contactMediumCOR.getCharacteristic().getCountry());
					characteristic.setEmailAddress(contactMediumCOR.getCharacteristic().getEmailAddress());
					characteristic.setFaxNumber(contactMediumCOR.getCharacteristic().getFaxNumber());
					characteristic.setPhoneNumber(contactMediumCOR.getCharacteristic().getPhoneNumber());
					characteristic.setMobileNumber(contactMediumCOR.getCharacteristic().getMobileNumber());
					characteristic.setPostCode(contactMediumCOR.getCharacteristic().getPostCode());
					characteristic.setWebSite(contactMediumCOR.getCharacteristic().getWebSite());
					characteristic.setStateOrProvince(contactMediumCOR.getCharacteristic().getStateOrProvince());
					characteristic.setStreet1(contactMediumCOR.getCharacteristic().getStreet1());
					characteristic.setStreet2(contactMediumCOR.getCharacteristic().getStreet2());
					
					// Calendar
					if (contactMediumCOR.getCharacteristic().getCalendar() != null) {
						for (CalendarPeriod calendarCOR:contactMediumCOR.getCharacteristic().getCalendar()) {
							CalendarPeriod calendar = new CalendarPeriod();
							calendar.setDay(calendarCOR.getDay());
							calendar.setStatus(calendarCOR.getStatus());
							calendar.setComment(calendarCOR.getComment());
							calendar.setTimeZone(calendarCOR.getTimeZone());
							
							// HourPeriod
							calendar.setHourPeriod(calendarCOR.getHourPeriod());
							
							characteristic.addCalendarItem(calendar);
						}
					}
					contactMedium.setCharacteristic(characteristic);
				}
				
				// Address
				GeographicAddressRef address = new GeographicAddressRef();
				if (contactMediumCOR.getAddress() != null) {
					address.setId(contactMediumCOR.getAddress().getId());
					address.setExternalId(contactMediumCOR.getAddress().getExternalId());
					address.setHref(contactMediumCOR.getAddress().getHref());
					address.setStreetNr(contactMediumCOR.getAddress().getStreetNr());
					address.setStreetName(contactMediumCOR.getAddress().getStreetName());
					address.setPostcode(contactMediumCOR.getAddress().getPostcode());
					address.set_Type(contactMediumCOR.getAddress().get_Type());
					address.setSchemaLocation(contactMediumCOR.getAddress().getSchemaLocation());
					address.setOwner(contactMediumCOR.getAddress().getOwner());
					
					// Type
					if (contactMediumCOR.getAddress().getType() != null) {
						Area type = new Area();
						type.setId(contactMediumCOR.getAddress().getType().getId());
						type.setName(contactMediumCOR.getAddress().getType().getName());
						type.setType(contactMediumCOR.getAddress().getType().getType());
						type.setDescription(contactMediumCOR.getAddress().getType().getDescription());
						address.setType(type);
					}
					
					// StreetType
					if (contactMediumCOR.getAddress().getStreetType() != null) {
						Area streetType = new Area();
						streetType.setId(contactMediumCOR.getAddress().getStreetType().getId());
						streetType.setName(contactMediumCOR.getAddress().getStreetType().getName());
						streetType.setType(contactMediumCOR.getAddress().getStreetType().getType());
						streetType.setDescription(contactMediumCOR.getAddress().getStreetType().getDescription());
						address.setStreetType(streetType);
					}
					
					// Locality
					if (contactMediumCOR.getAddress().getLocality() != null) {
						Area locality = new Area();
						// El Valor de id se consulta de listAssociationResponse
						if(!associationLocalityId.toString().equalsIgnoreCase(""))
							locality.setId(associationLocalityId.toString());
						else
							locality.setId(contactMediumCOR.getAddress().getLocality().getId());
						locality.setName(contactMediumCOR.getAddress().getLocality().getName());
						locality.setType(contactMediumCOR.getAddress().getLocality().getType());
						locality.setDescription(contactMediumCOR.getAddress().getLocality().getDescription());
						address.setLocality(locality);
					}
					
					// City
					if (contactMediumCOR.getAddress().getCity() != null) {
						Area city = new Area();
						// El Valor de id se consulta de listAssociationResponse
						if(!associationCityId.toString().equalsIgnoreCase("")
								&& !contactMediumCOR.getMediumType().equalsIgnoreCase("emailAddress")
								&& !contactMediumCOR.getMediumType().equalsIgnoreCase("telephoneNumber")
								&& !contactMediumCOR.getMediumType().equalsIgnoreCase("mobileNumber")
								&& !contactMediumCOR.getMediumType().equalsIgnoreCase("webSite")
								&& !contactMediumCOR.getMediumType().equalsIgnoreCase("faxNumber"))
							city.setId(associationCityId.toString());
						else
							city.setId(contactMediumCOR.getAddress().getCity().getId());
						city.setName(contactMediumCOR.getAddress().getCity().getName());
						city.setType(contactMediumCOR.getAddress().getCity().getType());
						city.setDescription(contactMediumCOR.getAddress().getCity().getDescription());
						address.setCity(city);
					}
					
					// StateOrProvince
					if (contactMediumCOR.getAddress().getStateOrProvince() != null) {
						Area stateOrProvince = new Area();
						// El Valor de id se consulta de listAssociationResponse
						if(!associationStateOrProvinceId.toString().equalsIgnoreCase(""))
							stateOrProvince.setId(associationStateOrProvinceId.toString());
						else
							stateOrProvince.setId(contactMediumCOR.getAddress().getStateOrProvince().getId());
						stateOrProvince.setName(contactMediumCOR.getAddress().getStateOrProvince().getName());
						stateOrProvince.setType(contactMediumCOR.getAddress().getStateOrProvince().getType());
						stateOrProvince.setDescription(contactMediumCOR.getAddress().getStateOrProvince().getDescription());
						address.setStateOrProvince(stateOrProvince);
					}
					
					// Country
					if (contactMediumCOR.getAddress().getCountry() != null) {
						Area country = new Area();
						// El Valor de name se consulta de listAssociationResponse
						if(!associationCountryId.toString().equalsIgnoreCase(""))
							country.setId(associationCountryId.toString());
						else
							country.setId(contactMediumCOR.getAddress().getCountry().getId());

						country.setName(contactMediumCOR.getAddress().getCountry().getName());
						country.setType(contactMediumCOR.getAddress().getCountry().getType());
						country.setDescription(contactMediumCOR.getAddress().getCountry().getDescription());
						address.setCountry(country);
					}
					
					// GeographicLocation
					if (contactMediumCOR.getAddress().getGeographicLocation() != null) {
						GeographicLocation geographicLocation = new GeographicLocation();
						geographicLocation.setId(contactMediumCOR.getAddress().getGeographicLocation().getId());
						geographicLocation.setHref(contactMediumCOR.getAddress().getGeographicLocation().getHref());
						geographicLocation.setName(contactMediumCOR.getAddress().getGeographicLocation().getName());
						geographicLocation.setGeometryType(contactMediumCOR.getAddress().getGeographicLocation().getGeometryType());
						geographicLocation.setAccuracy(contactMediumCOR.getAddress().getGeographicLocation().getAccuracy());
						geographicLocation.setSpatialRef(contactMediumCOR.getAddress().getGeographicLocation().getSpatialRef());
						geographicLocation.setType(contactMediumCOR.getAddress().getGeographicLocation().getType());
						geographicLocation.setSchemaLocation(contactMediumCOR.getAddress().getGeographicLocation().getSchemaLocation());
						geographicLocation.setGeometry(contactMediumCOR.getAddress().getGeographicLocation().getGeometry());
						address.setGeographicLocation(geographicLocation);
					}
					
					// GeographicSubAddress
					if (contactMediumCOR.getAddress().getGeographicSubAddress() != null) {
						for (GeographicSubAddress geographicSubAddressCOR: contactMediumCOR.getAddress().getGeographicSubAddress()) {
							GeographicSubAddress geographicSubAddress = new GeographicSubAddress();
							geographicSubAddress.setId(geographicSubAddressCOR.getId());
							geographicSubAddress.setType(geographicSubAddressCOR.getType());
							geographicSubAddress.setName(geographicSubAddressCOR.getName());
							geographicSubAddress.setDescription(geographicSubAddressCOR.getDescription());
							geographicSubAddress.setSubUnitType(geographicSubAddressCOR.getSubUnitType());
							geographicSubAddress.setSubUnitNumber(geographicSubAddressCOR.getSubUnitNumber());
							geographicSubAddress.setLevelType(geographicSubAddressCOR.getLevelType());
							geographicSubAddress.setLevelNumber(geographicSubAddressCOR.getLevelNumber());
							geographicSubAddress.setBuildingName(geographicSubAddressCOR.getBuildingName());
							geographicSubAddress.setPrivateStreetNumber(geographicSubAddressCOR.getPrivateStreetNumber());
							geographicSubAddress.setPrivateStreetName(geographicSubAddressCOR.getPrivateStreetName());
							geographicSubAddress.set_Type(geographicSubAddressCOR.get_Type());
							geographicSubAddress.setSchemaLocation(geographicSubAddressCOR.getSchemaLocation());
							
							address.addGeographicSubAddressItem(geographicSubAddress);
						}
					}
					
					// GeographicAddressSpec
					if (contactMediumCOR.getAddress().getGeographicAddressSpec() != null) {
						for (GeographicAddressSpec geographicAddressSpecCOR:contactMediumCOR.getAddress().getGeographicAddressSpec()) {
							GeographicAddressSpec geographicAddressSpec = new GeographicAddressSpec();
							geographicAddressSpec.setId(geographicAddressSpecCOR.getId());
							geographicAddressSpec.setHref(geographicAddressSpecCOR.getHref());
							geographicAddressSpec.setType(geographicAddressSpecCOR.getType());
							geographicAddressSpec.set_Type(geographicAddressSpecCOR.get_Type());
							geographicAddressSpec.setSchemaLocation(geographicAddressSpecCOR.getSchemaLocation());
							
							//  Characteristic
							if (geographicAddressSpecCOR.getCharacteristic() != null) {
								for (Characteristic characteristicGAS_COR:geographicAddressSpecCOR.getCharacteristic()) {
									Characteristic characteristicGAS = new Characteristic();
									characteristicGAS.setId(characteristicGAS_COR.getId());
									characteristicGAS.setName(characteristicGAS_COR.getName());
									characteristicGAS.setValueType(characteristicGAS_COR.getValueType());
									characteristicGAS.setValue(characteristicGAS_COR.getValue());
									
									geographicAddressSpec.addCharacteristicItem(characteristicGAS);
								}
							}
							
							address.addGeographicAddressSpecItem(geographicAddressSpec);
						}
					}
					 
					// Area
					if (contactMediumCOR.getAddress().getArea() != null) {
						for (Area areaCOR:contactMediumCOR.getAddress().getArea()) {
							Area area = new Area();
							area.setId(areaCOR.getId());
							area.setName(areaCOR.getName());
							area.setType(areaCOR.getType());
							area.setDescription(areaCOR.getDescription());
							address.addAreaItem(area);
						}
					}
					
					//Characteristic
					if (contactMediumCOR.getAddress().getCharacteristic() != null) {
						for (LocationCharacteristic characteristicA_COR:contactMediumCOR.getAddress().getCharacteristic()) {
							LocationCharacteristic characteristicA = new LocationCharacteristic();
							characteristicA.setId(characteristicA_COR.getId());
							characteristicA.setName(characteristicA_COR.getName());
							characteristicA.setValue(characteristicA_COR.getValue());
							
							address.addCharacteristicItem(characteristicA);
						}
					}
					
					contactMedium.setAddress(address);
				}				
				organization.addContactMediumItem(contactMedium);
			}
		}	
		
		// ExternalReference
		organization.setExternalReference(createOrganizationRequest.getExternalReference());
		
		// OrganizationChildRelationship
		organization.setOrganizationChildRelationship(createOrganizationRequest.getOrganizationChildRelationship());
		
		// OrganizationIdentification
		if(createOrganizationRequest.getOrganizationIdentification() != null) {
			for(OrganizationIdentification organizationIdentificationCOR:createOrganizationRequest.getOrganizationIdentification()) {
				OrganizationIdentification organizationIdentification = new OrganizationIdentification();
				organizationIdentification.setIdentificationId(organizationIdentificationCOR.getIdentificationId());
				// El Valor de IdentificationType se consulta de listAssociationResponse
				if(!associationIdentificationType.toString().equalsIgnoreCase(""))
					organizationIdentification.setIdentificationType(associationIdentificationType.toString());
				else
					organizationIdentification.setIdentificationType(organizationIdentificationCOR.getIdentificationType());
				organizationIdentification.setIssuingAuthority(organizationIdentificationCOR.getIssuingAuthority());
				organizationIdentification.setIssuingDate(organizationIdentificationCOR.getIssuingDate());
				// El Valor de CheckDigit se consulta de listAssociationResponse
				if(!checkDigit.toString().equalsIgnoreCase(""))
					organizationIdentification.setCheckDigit(checkDigit.toString());
				else
					organizationIdentification.setCheckDigit(organizationIdentificationCOR.getCheckDigit());
				organizationIdentification.setBaseType(organizationIdentificationCOR.getBaseType());
				organizationIdentification.setSchemaLocation(organizationIdentificationCOR.getSchemaLocation());
				organizationIdentification.setType(organizationIdentificationCOR.getType());
				organizationIdentification.setValidFor(organizationIdentificationCOR.getValidFor());
				organization.addOrganizationIdentificationItem(organizationIdentification);
			}
		}		
		
		// OrganizationParentRelationship
		organization.setOrganizationParentRelationship(createOrganizationRequest.getOrganizationParentRelationship());
		
		// OtherName
		organization.setOtherName(createOrganizationRequest.getOtherName());
		
		// PartyCharacteristic
		if (createOrganizationRequest.getPartyCharacteristic() != null) {
			for(Characteristic partyCharacteristicCOR:createOrganizationRequest.getPartyCharacteristic()) {
				Characteristic partyCharacteristic = new Characteristic();
				if (partyCharacteristicCOR.getName().equalsIgnoreCase("TipoContribuyente")) {
					partyCharacteristic.setId(partyCharacteristicCOR.getId());		
					partyCharacteristic.setName(partyCharacteristicCOR.getName());	
					partyCharacteristic.setValueType(partyCharacteristicCOR.getValueType());
					// El Valor de Value se consulta de listAssociationResponse
					if(!associationValueTypeTaxpayer.toString().equalsIgnoreCase(""))
						partyCharacteristic.setValue(associationValueTypeTaxpayer.toString());
					else
						partyCharacteristic.setValue(partyCharacteristicCOR.getValue());	
				}else {
					partyCharacteristic.setId(partyCharacteristicCOR.getId());		
					partyCharacteristic.setName(partyCharacteristicCOR.getName());	
					partyCharacteristic.setValueType(partyCharacteristicCOR.getValueType());	
					partyCharacteristic.setValue(partyCharacteristicCOR.getValue());	
				}
				organization.addPartyCharacteristicItem(partyCharacteristic);
			}
		}
		
		// RelatedParty
		if (createOrganizationRequest.getRelatedParty() != null) {
			for(RelatedPartyRef relatedPartyCOR:createOrganizationRequest.getRelatedParty()) {
				RelatedPartyRef relatedParty = new RelatedPartyRef();
				relatedParty.setId(relatedPartyCOR.getId());
				relatedParty.setIdentifierType(relatedPartyCOR.getIdentifierType());
				relatedParty.setHref(relatedPartyCOR.getHref());
				relatedParty.setName(relatedPartyCOR.getName());				
				if (!relatedPartyCOR.getRole().equalsIgnoreCase("account")) {
					// El Valor de Role se obtiene de listAssociationResponse
					if(!roleRelatedParty.toString().equalsIgnoreCase(""))
						relatedParty.setRole(roleRelatedParty.toString());
					else
						relatedParty.setRole(relatedPartyCOR.getRole());
					
					// El Valor de PartyRank se obtiene de listAssociationResponse 
					if(!partyRankRelatedParty.toString().equalsIgnoreCase(""))
						relatedParty.setPartyRank(partyRankRelatedParty.toString());
					else
						relatedParty.setPartyRank(relatedPartyCOR.getPartyRank());
				}else {
					relatedParty.setRole(relatedPartyCOR.getRole());
					relatedParty.setPartyRank(relatedPartyCOR.getPartyRank());
				}					
				relatedParty.setIsLegalEntity(relatedPartyCOR.isIsLegalEntity());
				relatedParty.setOrganization(relatedPartyCOR.getOrganization());
				relatedParty.setBaseType(relatedPartyCOR.getBaseType());
				relatedParty.setSchemaLocation(relatedPartyCOR.getSchemaLocation());
				relatedParty.setType(relatedPartyCOR.getType());
				relatedParty.setReferredType(relatedPartyCOR.getReferredType());
				
				// PartyProfileType
				if (relatedPartyCOR.getPartyProfileType() != null) {
					for(PartyProfileType partyProfileTypeCOR:relatedPartyCOR.getPartyProfileType()) {
						PartyProfileType partyProfileType = new PartyProfileType();
						partyProfileType.setId(partyProfileTypeCOR.getId());
						partyProfileType.setDescription(partyProfileTypeCOR.getDescription());
						partyProfileType.setName(partyProfileTypeCOR.getName());
						partyProfileType.setCategory(partyProfileTypeCOR.getCategory());
						relatedParty.addPartyProfileTypeItem(partyProfileType);
					}
				}
				
				// ExternalReference
				if (relatedPartyCOR.getExternalReference() != null) {
					for(ExternalReference externalReferenceCOR:relatedPartyCOR.getExternalReference()) {
						ExternalReference externalReference = new ExternalReference();
						externalReference.setExternalReferenceType(externalReferenceCOR.getExternalReferenceType());
						externalReference.setName(externalReferenceCOR.getName());
						relatedParty.addExternalReferenceItem(externalReference);
					}
				}
				
				// PartyIdentification
				relatedParty.setPartyIdentification(relatedPartyCOR.getPartyIdentification());
				
				// Individual
				relatedParty.setIndividual(relatedPartyCOR.getIndividual());
				
				// PartyCharacteristic
				relatedParty.setPartyCharacteristic(relatedPartyCOR.getPartyCharacteristic());				
				
				organization.addRelatedPartyItem(relatedParty);
			}
		}
		
		// PartyProfileType
		if (createOrganizationRequest.getPartyProfileType() != null) {
			for(PartyProfileType partyProfileTypeCOR:createOrganizationRequest.getPartyProfileType()) {
				PartyProfileType partyProfileType = new PartyProfileType();
				partyProfileType.setId(partyProfileTypeCOR.getId());
				partyProfileType.setDescription(partyProfileTypeCOR.getDescription());
				if (partyProfileTypeCOR.getCategory().equalsIgnoreCase("sector")) {
					// El Valor de Name se obtiene de listAssociationResponse 
					if(!categorySectorPartyProfileType.toString().equalsIgnoreCase(""))
						partyProfileType.setName(categorySectorPartyProfileType.toString());
					else
						partyProfileType.setName(partyProfileTypeCOR.getName());
				}else
					partyProfileType.setName(partyProfileTypeCOR.getName());
				partyProfileType.setCategory(partyProfileTypeCOR.getCategory());
				organization.addPartyProfileTypeItem(partyProfileType);
			}
		}
		
		// Note
		if (createOrganizationRequest.getNote() != null) {
			for(Note noteCOR:createOrganizationRequest.getNote()) {
				Note note = new Note();
				note.setId(noteCOR.getId());
				note.setAuthor(noteCOR.getAuthor());
				note.setText(noteCOR.getText());
				note.setDate(noteCOR.getDate());
				organization.addNoteItem(note);
			}
		}
		
		// Retun Organization Mapping
		return organization;
	}

}
