package ec.com.claro.mscomposercreateorg.model;

import java.util.ArrayList;
import java.util.List;

import io.swagger.organization.model.GenericFault;
import io.swagger.organization.model.Organization;

public class Notify {

	private Organization Request;
	private List<GenericFault> ResponseOrg =new ArrayList<GenericFault>();
	private List<io.swagger.association.model.GenericFault> ResponseList =new ArrayList<io.swagger.association.model.GenericFault>();
	public Organization getRequest() {
		return Request;
	}
	public void setRequest(Organization request) {
		Request = request;
	}
	public List<GenericFault> getResponseOrg() {
		return ResponseOrg;
	}
	public void setResponseOrg(List<io.swagger.organization.model.GenericFault> respuestaOrg) {
		ResponseOrg = respuestaOrg;
	}
	public List<io.swagger.association.model.GenericFault> getResponseList() {
		return ResponseList;
	}
	public void setResponseList(List<io.swagger.association.model.GenericFault> responseList) {
		ResponseList = responseList;
	}
	
	
	
}
