package ec.com.claro.mscreateorganizationaxis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "ec.com.claro.mscreateorganizationaxis")
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
    }
}