
package ec.com.claro.mscreateorganizationaxis.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "TRK_C_PV_RESPONSE",
    "TRK_C_PV_ERROR",
    "sqlcode",
    "sqlerrm"
})
@Generated("jsonschema2pojo")
public class Result {

    @JsonProperty("TRK_C_PV_RESPONSE")
    private String trkCPvResponse;
    @JsonProperty("TRK_C_PV_ERROR")
    private String trkCPvError;
    @JsonProperty("sqlcode")
    private String sqlcode;
    @JsonProperty("sqlerrm")
    private String sqlerrm;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("TRK_C_PV_RESPONSE")
    public String getTrkCPvResponse() {
        return trkCPvResponse;
    }

    @JsonProperty("TRK_C_PV_RESPONSE")
    public void setTrkCPvResponse(String trkCPvResponse) {
        this.trkCPvResponse = trkCPvResponse;
    }

    @JsonProperty("TRK_C_PV_ERROR")
    public String getTrkCPvError() {
        return trkCPvError;
    }

    @JsonProperty("TRK_C_PV_ERROR")
    public void setTrkCPvError(String trkCPvError) {
        this.trkCPvError = trkCPvError;
    }

    @JsonProperty("sqlcode")
    public String getSqlcode() {
        return sqlcode;
    }

    @JsonProperty("sqlcode")
    public void setSqlcode(String sqlcode) {
        this.sqlcode = sqlcode;
    }

    @JsonProperty("sqlerrm")
    public String getSqlerrm() {
        return sqlerrm;
    }

    @JsonProperty("sqlerrm")
    public void setSqlerrm(String sqlerrm) {
        this.sqlerrm = sqlerrm;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Result.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("trkCPvResponse");
        sb.append('=');
        sb.append(((this.trkCPvResponse == null)?"<null>":this.trkCPvResponse));
        sb.append(',');
        sb.append("trkCPvError");
        sb.append('=');
        sb.append(((this.trkCPvError == null)?"<null>":this.trkCPvError));
        sb.append(',');
        sb.append("sqlcode");
        sb.append('=');
        sb.append(((this.sqlcode == null)?"<null>":this.sqlcode));
        sb.append(',');
        sb.append("sqlerrm");
        sb.append('=');
        sb.append(((this.sqlerrm == null)?"<null>":this.sqlerrm));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
