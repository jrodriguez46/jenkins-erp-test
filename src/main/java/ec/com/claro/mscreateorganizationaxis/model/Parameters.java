
package ec.com.claro.mscreateorganizationaxis.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "TRK_CLIENTE_API.TRP_CREA_CLIENTE"
})
@Generated("jsonschema2pojo")
public class Parameters {

    @JsonProperty("TRK_CLIENTE_API.TRP_CREA_CLIENTE")
    private TrkClienteApiTrpCreaCliente trkClienteApiTrpCreaCliente;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
 Thread t= new Thread();
    @JsonProperty("TRK_CLIENTE_API.TRP_CREA_CLIENTE")
    public TrkClienteApiTrpCreaCliente getTrkClienteApiTrpCreaCliente() {
        return trkClienteApiTrpCreaCliente;
    }

    @JsonProperty("TRK_CLIENTE_API.TRP_CREA_CLIENTE")
    public void setTrkClienteApiTrpCreaCliente(TrkClienteApiTrpCreaCliente trkClienteApiTrpCreaCliente) {
        this.trkClienteApiTrpCreaCliente = trkClienteApiTrpCreaCliente;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Parameters.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("trkClienteApiTrpCreaCliente");
        sb.append('=');
        sb.append(((this.trkClienteApiTrpCreaCliente == null)?"<null>":this.trkClienteApiTrpCreaCliente));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
