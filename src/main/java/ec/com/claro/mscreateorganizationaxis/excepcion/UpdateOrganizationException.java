package ec.com.claro.mscreateorganizationaxis.excepcion;

import java.io.IOException;

@SuppressWarnings("serial")
public class UpdateOrganizationException extends RuntimeException{
	public UpdateOrganizationException(String mensaje) throws IOException{
        super(mensaje);
    }

}

