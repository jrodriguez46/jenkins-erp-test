package ec.com.claro.mscreateorganizationaxis.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import ec.com.claro.mscreateorganizationaxis.model.CreateOrganizationReq;
import ec.com.claro.mscreateorganizationaxis.util.CommonMappings;

public class OrganizationProcessorReq implements Processor {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public void process(Exchange exchange) throws Exception {
		//Save Headers CommonMappings
		CommonMappings commonMappings = new CommonMappings();
		commonMappings.saveHeaders(exchange);
		commonMappings.saveProperty(exchange);
		
		//maping
		OrganizationService organizationService = new OrganizationService();
		CreateOrganizationReq createOrganizationReq = organizationService.mappingRequest(exchange);
		String jsonCreateOrganizationReq = new ObjectMapper().writeValueAsString(createOrganizationReq);
		
		String jsonCreateOrganizationReqLog = "Body request legacy >>>>> " + jsonCreateOrganizationReq;
		log.info(jsonCreateOrganizationReqLog);
		
		exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json;charset=UTF-8");
		exchange.getOut().setHeader(Exchange.HTTP_METHOD, "POST");
		exchange.getOut().setHeader(Exchange.CONTENT_ENCODING, "UTF-8");
		exchange.getOut().setBody(jsonCreateOrganizationReq);

	}

}
