package ec.com.claro.mscreateorganizationaxis.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.threeten.bp.OffsetDateTime;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.model.DetailResType;
import io.swagger.model.ErrorListType;
import io.swagger.model.GenericFault;

public class CreateOrganizationFaildResProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		GenericFault gf = new GenericFault();
		ErrorListType errorList = new ErrorListType();
		List<DetailResType> listDetail =new ArrayList<>();
		DetailResType detail = new DetailResType();
		
		gf.setMessageUUID(exchange.getProperty("messageId").toString());
		gf.setResponseDate(OffsetDateTime.now());
		gf.setLatency(1);
		
		detail.setCode(exchange.getProperty("codeFault").toString());
		detail.setSeverityLevel("1");
		detail.setDescription(exchange.getProperty("messageFault").toString());
		detail.setActor(exchange.getProperty("actor").toString());
		detail.setBusinessMeaning(exchange.getProperty("messageFault").toString());
		listDetail.add(detail);
		errorList.setError(listDetail);
		gf.setErrorList(errorList);
		
		String json = new ObjectMapper().writeValueAsString(gf);
		exchange.getOut().setBody(json);
		exchange.getOut().setHeader("rabbitmq.CORRELATIONID", exchange.getProperty("rabbitMQCorrelatorId").toString());
		exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, exchange.getProperty("codeHttp").toString());
		exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");

	}

}
