package ec.com.claro.mscreateorganizationaxis.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import ec.com.claro.mscreateorganizationaxis.util.CommonMappings;
import io.swagger.model.Organization;

public class CreateOrganizationResProcessor implements Processor {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public void process(Exchange exchange) throws Exception {
			
		OrganizationService organizationService = new OrganizationService();
		CommonMappings commonMappings = new CommonMappings();
		
		//Mapping Legacy --> OpenAPI Response
		Organization organization = organizationService.mappingRes(exchange);
		
		String organizationJson = new ObjectMapper().writeValueAsString(organization);
		if(exchange.getProperty("isMQ").toString().equalsIgnoreCase("false")) {
			exchange.getOut().setBody(organization);
		}else {
			
			exchange.getOut().setBody(organizationJson);
		}
		
		
		
		exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, "200");
		exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
		
		
		//Header response
		commonMappings.headerResponse(exchange);
		
		String organizationLog = "Body Response MS >>>>> " + organizationJson;
		log.info(organizationLog);
	}

}
