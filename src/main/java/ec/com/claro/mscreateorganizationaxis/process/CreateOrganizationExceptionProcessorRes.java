package ec.com.claro.mscreateorganizationaxis.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class CreateOrganizationExceptionProcessorRes implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		exchange.getOut().setHeaders(exchange.getIn().getHeaders());
		exchange.getOut().setBody(exchange.getIn().getBody(String.class));

	}

}
