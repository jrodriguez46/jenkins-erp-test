package ec.com.claro.mscreateorganizationaxis.process;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.model.Area;
import io.swagger.model.Characteristic;
import io.swagger.model.ContactMedium;
import io.swagger.model.ExternalReference;
import io.swagger.model.GeographicAddressRef;
import io.swagger.model.GeographicAddressSpec;
import io.swagger.model.GeographicSubAddress;
import io.swagger.model.LocationCharacteristic;
import io.swagger.model.Organization;
import io.swagger.model.PartyProfileType;
import io.swagger.model.RelatedPartyRef;
import ec.com.claro.mscreateorganizationaxis.excepcion.UpdateOrganizationException;
import ec.com.claro.mscreateorganizationaxis.model.Channel;
import ec.com.claro.mscreateorganizationaxis.model.Consumer;
import ec.com.claro.mscreateorganizationaxis.model.CreateOrganizationReq;
import ec.com.claro.mscreateorganizationaxis.model.CreateOrganizationRes;
import ec.com.claro.mscreateorganizationaxis.model.Parameters;
import ec.com.claro.mscreateorganizationaxis.model.Transaction;
import ec.com.claro.mscreateorganizationaxis.model.TrkClienteApiTrpCreaCliente;
import ec.com.claro.mscreateorganizationaxis.util.CommonMappings;

public class OrganizationService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@SuppressWarnings("unchecked")
	public CreateOrganizationReq mappingRequest(Exchange exchange)
			throws IOException, ParserConfigurationException, TransformerException {
		CommonMappings commonMappings = new CommonMappings();
		Organization organization;

		ArrayList<HashMap<String, Object>> reqOrg = exchange.getIn().getBody(ArrayList.class);

		if (reqOrg.size() == 1) {

			organization = commonMappings.parseRequestSuccess(reqOrg);

		} else {

			String reqOrgStr = exchange.getIn().getBody(String.class);
			organization = commonMappings.parseRequestSuccess(reqOrgStr);
		}

		String jsonCreateOrganizationReq = new ObjectMapper().writeValueAsString(organization);
		String bodyResDatLog = "body Request DAT >>>>> " + jsonCreateOrganizationReq;
		log.info(bodyResDatLog);

		String pvRequest = generateXMLRequest(organization);

		// Objetos Legado
		CreateOrganizationReq retrieveOrganizationReq = new CreateOrganizationReq();
		Channel channel = new Channel();
		Consumer consumer = new Consumer();
		Transaction transaction = new Transaction();
		Parameters parameters = new Parameters();
		TrkClienteApiTrpCreaCliente trkClienteApiTrpCreaCliente = new TrkClienteApiTrpCreaCliente();

		// channel
		//MediaId
		if(!exchange.getProperty(CommonMappings.CHANNEL).toString().equalsIgnoreCase(""))
			channel.setMediaId(exchange.getProperty(CommonMappings.CHANNEL).toString());
		else
			channel.setMediaId(CommonMappings.SALESFORCE);
		
		//MediaDetailId
		if(!exchange.getProperty(CommonMappings.DEVICE).toString().equalsIgnoreCase(""))
			channel.setMediaDetailId(exchange.getProperty(CommonMappings.DEVICE).toString());
		else
			channel.setMediaDetailId(CommonMappings.SALESFORCE);
		retrieveOrganizationReq.setChannel(channel);

		// consumer
		//CompanyId
		if(!exchange.getProperty(CommonMappings.ORIGIN_SYSTEM).toString().equalsIgnoreCase(""))
			consumer.setCompanyId(exchange.getProperty(CommonMappings.ORIGIN_SYSTEM).toString());
		else
			consumer.setCompanyId(CommonMappings.SALESFORCE);
		//ConsumerId
		if(!exchange.getProperty(CommonMappings.APPLICATION_ID).toString().equalsIgnoreCase(""))
			consumer.setConsumerId(exchange.getProperty(CommonMappings.APPLICATION_ID).toString());
		else
			consumer.setConsumerId(CommonMappings.SALESFORCE);
		//Terminal
		if(!exchange.getProperty(CommonMappings.IP_CLIENT).toString().equalsIgnoreCase(""))
			consumer.setTerminal(exchange.getProperty(CommonMappings.IP_CLIENT).toString());
		else
			consumer.setTerminal(CommonMappings.IP);
		retrieveOrganizationReq.setConsumer(consumer);

		// transaction
		transaction.setAlias(CommonMappings.TRANSACTION_ALIAS);
		// parameters
		// TRK_CLIENTE_API.TRP_CREA_CLIENTE
		trkClienteApiTrpCreaCliente.setPvRequest(pvRequest);
		parameters.setTrkClienteApiTrpCreaCliente(trkClienteApiTrpCreaCliente);
		transaction.setParameters(parameters);
		retrieveOrganizationReq.setTransaction(transaction);

		return retrieveOrganizationReq;
	}

	public Organization mappingRes(Exchange exchange) throws IOException, ParserConfigurationException, SAXException {

		// Objetos OpenAPI
		Organization organization = new Organization();
		ContactMedium contactMedium;
		List<ContactMedium> listContactMedium = new ArrayList<>();
		GeographicAddressRef area;
		RelatedPartyRef customer = new RelatedPartyRef();
		List<RelatedPartyRef> listRelatedPartyRef = new ArrayList<>();

		// Parse body Legacy
		CommonMappings commonMappings = new CommonMappings();
		String bodyResLegacy = exchange.getIn().getBody(String.class);

		String bodyResLegacyLog = "bodyResLegacy >>>>> " + bodyResLegacy;
		log.info(bodyResLegacyLog);

		CreateOrganizationRes createOrganizationRes = commonMappings.parseResLegacy(bodyResLegacy);

		if (exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE).toString().equalsIgnoreCase("200")) {
			// Mapping
			String pvResponse = createOrganizationRes.getData().getResult().getTrkCPvResponse();
			DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); // Compliant
			df.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ""); // compliant
			DocumentBuilder builder = df.newDocumentBuilder();
			Document xmlDocumentEncriptar = builder.parse(new InputSource(new StringReader(pvResponse)));

			// Objetos xml --> PV_CODERROR
			String pvCodeError = xmlDocumentEncriptar.getDocumentElement().getElementsByTagName("PV_CODERROR").item(0)
					.getTextContent();
			if (pvCodeError.equalsIgnoreCase("0") || pvCodeError.equalsIgnoreCase("")) {

				// objetos xml --> PV_IDSECUBICACION
				NodeList nlIdUbicacion = xmlDocumentEncriptar.getDocumentElement()
						.getElementsByTagName("PV_IDSECUBICACION");
				for (int i = 0; i < nlIdUbicacion.getLength(); i++) {
					// objetos --> PV_IDSECUBICACION
					Node nIdUbicacion = nlIdUbicacion.item(i);
					Element eIdUbicacion = (Element) nIdUbicacion;

					// Objeto OpenAPI ContactMedium
					contactMedium = new ContactMedium();
					area = new GeographicAddressRef();

					area.setExternalId(eIdUbicacion.getTextContent());
					contactMedium.address(area);
					listContactMedium.add(contactMedium);

				}

				// Objetos xml --> PN_IDPERSONA
				customer.setId(xmlDocumentEncriptar.getDocumentElement().getElementsByTagName("PN_IDPERSONA").item(0)
						.getTextContent());
				customer.setIdentifierType("customerId");
				customer.setRole("customer");
				listRelatedPartyRef.add(customer);

				// Organization
				organization.setContactMedium(listContactMedium);
				organization.setRelatedParty(listRelatedPartyRef);

			} else {
				commonMappings.saveProperty(exchange, xmlDocumentEncriptar.getDocumentElement()
						.getElementsByTagName("PV_MSJERROR").item(0).getTextContent(), "MSA-002", "500", "Micro SRE",
						pvCodeError);
				throw new UpdateOrganizationException(exchange.getProperty("messageFault").toString());
			}
		} else {
			commonMappings.saveProperty(exchange, createOrganizationRes.getMessage(), "MSA-003",
					exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE).toString(), "Micro SRE",
					createOrganizationRes.getMessage());
			throw new UpdateOrganizationException(exchange.getProperty("messageFault").toString());
		}
		return organization;
	}

	public String generateXMLRequest(Organization organization)
			throws ParserConfigurationException, TransformerException {

		int contTelephoneNumber = 0;
		
		// mapping
		// Creo una instancia de DocumentBuilderFactory
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		// Creo un documentBuilder
		DocumentBuilder builder = factory.newDocumentBuilder();
		// Creo un DOMImplementation
		DOMImplementation implementation = builder.getDOMImplementation();

		// Creo un documento con un elemento raiz
		Document documentoXML = implementation.createDocument(null, "REQUEST", null);
		documentoXML.setXmlVersion("1.0");
		log.info("229");
		// Creo el elemento --> cliente
		Element cliente = documentoXML.createElement("CLIENTE");
		// Creo el elemento --> ubicacion

		// Creo el elemento --> MEDIOS_CONTACTO
		Element medioContacto = documentoXML.createElement("MEDIOS_CONTACTO");
		// Creo el elemento --> OBSERVACION
		Element observacion = documentoXML.createElement("OBSERVACION");

		// Añado al root el elemento cliente
		documentoXML.getDocumentElement().appendChild(cliente);
		log.info("241");
		// Orden-------------------------------------------------------------------------------------------
		Element idCliente = documentoXML.createElement("PV_IDCLIENTE");
		Text valueIdCliente = documentoXML.createTextNode("");
		cliente.appendChild(idCliente);

		Element idTypeCliente = documentoXML.createElement("PV_IDENTIFIERTYPE_CLIENTE");
		Text valueIdTypeCliente = documentoXML.createTextNode("");
		cliente.appendChild(idTypeCliente);
		log.info("250");
		Element roleCliente = documentoXML.createElement("PV_ROL_CLIENTE");
		Text valueRoleCliente = documentoXML.createTextNode("");
		cliente.appendChild(roleCliente);

		Element referenciaExterna = documentoXML.createElement("REFERENCIA_EXTERNA");
		// CLIENTE --> REFERENCIA_EXTERNA --> NAME
		Element reName = documentoXML.createElement("PV_NAME_REFERENCE");
		Text valueReName = documentoXML.createTextNode("");
		referenciaExterna.appendChild(reName);
		// CLIENTE --> REFERENCIA_EXTERNA --> PV_REFERENCETYPE
		Element reReferenceType = documentoXML.createElement("PV_REFERENCETYPE");
		Text valueReReferenceType = documentoXML.createTextNode("");
		referenciaExterna.appendChild(reReferenceType);
		// CLIENTE --> REFERENCIA_EXTERNA --> PV_REG_CLIENTE
		Element reRegCliente = documentoXML.createElement("PV_REG_CLIENTE");
		Text valueReRegCliente = documentoXML.createTextNode("");
		reRegCliente.appendChild(valueReRegCliente);
		referenciaExterna.appendChild(reRegCliente);
		cliente.appendChild(referenciaExterna);
		log.info("270");
		// CLIENTE --> PV_NOMBRECOMPLETO
		Element nombreCompleto = documentoXML.createElement("PV_NOMBRECOMPLETO");
		Text valueNombreCompleto = documentoXML.createTextNode("");
		if (organization.getTradingName() != null)
			valueNombreCompleto = documentoXML.createTextNode(organization.getTradingName());
		nombreCompleto.appendChild(valueNombreCompleto);
		cliente.appendChild(nombreCompleto);
		log.info("278");
		// CLIENTE --> PV_BUSINESSNAME
		Element businessName = documentoXML.createElement("PV_BUSINESSNAME");
		Text valueBusinessName = documentoXML.createTextNode("");
		if (organization.getName() != null)
			valueBusinessName = documentoXML.createTextNode(organization.getName());
		businessName.appendChild(valueBusinessName);
		cliente.appendChild(businessName);

		// CLIENTE --> PV_ACTIVO
		Element activo = documentoXML.createElement("PV_ACTIVO");
		Text valueActivo = documentoXML.createTextNode("");
		if (organization.getStatus() != null)
			valueActivo = documentoXML.createTextNode(organization.getStatus());
		activo.appendChild(valueActivo);
		cliente.appendChild(activo);

		// CLIENTE --> TIPOS_PERFILES
		Element tipoPerfiles = documentoXML.createElement("TIPOS_PERFILES");
		log.info("297");
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
		Element tipoPerfileSegmento = documentoXML.createElement(CommonMappings.TIPO_PERFIL);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SEGMENTO
		Element pvSegmanto = documentoXML.createElement("PV_SEGMENTO");
		Text valuepvSegmanto = documentoXML.createTextNode("");
		pvSegmanto.appendChild(valuepvSegmanto);
		tipoPerfileSegmento.appendChild(pvSegmanto);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SUBCATEGORIA
		Element nombreSegmanto = documentoXML.createElement("PV_NOMBRE_SEGMENTO");
		Text valueNombreSegmanto = documentoXML.createTextNode("");
		nombreSegmanto.appendChild(valueNombreSegmanto);
		tipoPerfileSegmento.appendChild(nombreSegmanto);
		tipoPerfiles.appendChild(tipoPerfileSegmento);
		log.info("311");
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
		Element tipoPerfileSC = documentoXML.createElement(CommonMappings.TIPO_PERFIL);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SUBCATEGORIA
		Element subcategoria = documentoXML.createElement("PV_SUBCATEGORIA");
		Text valueSubcategoria = documentoXML.createTextNode("");
		subcategoria.appendChild(valueSubcategoria);
		tipoPerfileSC.appendChild(subcategoria);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SUBCATEGORIA
		Element nombreSubcategoria = documentoXML.createElement("PV_NOMBRE_SUBCATEGORIA");
		Text valueNombreSubcategoria = documentoXML.createTextNode("");
		nombreSubcategoria.appendChild(valueNombreSubcategoria);
		tipoPerfileSC.appendChild(nombreSubcategoria);
		tipoPerfiles.appendChild(tipoPerfileSC);
		log.info("325");
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
		Element tipoPerfileSegmentoVenta = documentoXML.createElement(CommonMappings.TIPO_PERFIL);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SEGMENTOVENTA
		Element pvSegmantoVenta = documentoXML.createElement("PV_SEGMENTOVENTA");
		Text valuepvSegmantoVenta = documentoXML.createTextNode("");
		pvSegmantoVenta.appendChild(valuepvSegmantoVenta);
		tipoPerfileSegmentoVenta.appendChild(pvSegmantoVenta);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SEGMENTO_VENTA
		Element nombreSegmantoVenta = documentoXML.createElement("PV_NOMBRE_SEGMENTO_VENTA");
		Text valueNombreSegmantoVenta = documentoXML.createTextNode("");
		nombreSegmantoVenta.appendChild(valueNombreSegmantoVenta);
		tipoPerfileSegmentoVenta.appendChild(nombreSegmantoVenta);
		tipoPerfiles.appendChild(tipoPerfileSegmentoVenta);
		log.info("339");
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
		Element tipoPerfileCanalVenta = documentoXML.createElement(CommonMappings.TIPO_PERFIL);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_CANALVENTA
		Element pvCanalVenta = documentoXML.createElement("PV_CANALVENTA");
		Text valuepvCanalVenta = documentoXML.createTextNode("");
		pvCanalVenta.appendChild(valuepvCanalVenta);
		tipoPerfileCanalVenta.appendChild(pvCanalVenta);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_CANAL
		Element nombreCanalVenta = documentoXML.createElement("PV_NOMBRE_CANAL");
		Text valueNombreCanalVenta = documentoXML.createTextNode("");
		nombreCanalVenta.appendChild(valueNombreCanalVenta);
		tipoPerfileCanalVenta.appendChild(nombreCanalVenta);
		tipoPerfiles.appendChild(tipoPerfileCanalVenta);
		log.info("353");
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
		Element tipoPerfileSector = documentoXML.createElement(CommonMappings.TIPO_PERFIL);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SECTOR
		Element pvSector = documentoXML.createElement("PV_SECTOR");
		Text valuepvSector = documentoXML.createTextNode("");
		pvSector.appendChild(valuepvSector);
		tipoPerfileSector.appendChild(pvSector);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SECTOR
		Element nombreSector = documentoXML.createElement("PV_NOMBRE_SECTOR");
		Text valueNombreSector = documentoXML.createTextNode("");
		nombreSector.appendChild(valueNombreSector);
		tipoPerfileSector.appendChild(nombreSector);
		tipoPerfiles.appendChild(tipoPerfileSector);
		log.info("367");
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
		Element tipoPerfileSubSector = documentoXML.createElement(CommonMappings.TIPO_PERFIL);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SEGMENTOVENTA
		Element pvSubSector = documentoXML.createElement("PV_SUBSECTOR");
		Text valuepvSubSector = documentoXML.createTextNode("");
		pvSubSector.appendChild(valuepvSubSector);
		tipoPerfileSubSector.appendChild(pvSubSector);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SUBCATEGORIA
		Element nombreSubSector = documentoXML.createElement("PV_NOMBRE_SUBSECTOR");
		Text valueNombreSubSector = documentoXML.createTextNode("");
		nombreSubSector.appendChild(valueNombreSubSector);
		tipoPerfileSubSector.appendChild(nombreSubSector);
		tipoPerfiles.appendChild(tipoPerfileSubSector);
		log.info("381");
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
		Element tipoPerfileActividadEconomica = documentoXML.createElement(CommonMappings.TIPO_PERFIL);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_ACTIVIDADECONOMICA
		Element pvActividadEconomica = documentoXML.createElement("PV_ACTIVIDADECONOMICA");
		Text valuepvActividadEconomica = documentoXML.createTextNode("");
		pvActividadEconomica.appendChild(valuepvActividadEconomica);
		tipoPerfileActividadEconomica.appendChild(pvActividadEconomica);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_ACTIVIDADECONOMICA
		Element nombreActividadEconomica = documentoXML.createElement("PV_NOMBRE_ACTIVIDADECONOMICA");
		Text valueNombreActividadEconomica = documentoXML.createTextNode("");
		nombreActividadEconomica.appendChild(valueNombreActividadEconomica);
		tipoPerfileActividadEconomica.appendChild(nombreActividadEconomica);
		tipoPerfiles.appendChild(tipoPerfileActividadEconomica);
		log.info("395");
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
		Element tipoPerfileGiroComercial = documentoXML.createElement(CommonMappings.TIPO_PERFIL);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_GIROCOMERCIAL
		Element pvGiroComercial = documentoXML.createElement("PV_GIROCOMERCIAL");
		Text valueGiroComercial = documentoXML.createTextNode("");
		pvGiroComercial.appendChild(valueGiroComercial);
		tipoPerfileGiroComercial.appendChild(pvGiroComercial);
		// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_GIROCOMERCIAL
		Element nombreGiroComercial = documentoXML.createElement("PV_NOMBRE_GIROCOMERCIAL");
		Text valueNombreGiroComercial = documentoXML.createTextNode("");
		nombreGiroComercial.appendChild(valueNombreGiroComercial);
		tipoPerfileGiroComercial.appendChild(nombreGiroComercial);
		tipoPerfiles.appendChild(tipoPerfileGiroComercial);
		log.info("409");
		cliente.appendChild(tipoPerfiles);

		// CLIENTE --> PV_CATEGORIA_CLIENTE
		Element categoriaCliente = documentoXML.createElement("PV_CATEGORIA_CLIENTE");
		Text valueCategoriaCliente = documentoXML.createTextNode("");
		categoriaCliente.appendChild(valueCategoriaCliente);
		cliente.appendChild(categoriaCliente);

		// CLIENTE --> PV_TIPOCLIENTE
		Element tipoCliente = documentoXML.createElement("PV_TIPOCLIENTE");
		Text valueTipoCliente = documentoXML.createTextNode("");
		if (organization.getOrganizationType() != null)
			valueTipoCliente = documentoXML.createTextNode(organization.getOrganizationType());
		tipoCliente.appendChild(valueTipoCliente);
		cliente.appendChild(tipoCliente);
		log.info("425");
		// CLIENTE --> PV_TIPO_IDENTIFICACION
		Element tipoIdentificacion = documentoXML.createElement("PV_TIPO_IDENTIFICACION");
		Text valueTipoIdentificacion = documentoXML.createTextNode("");
		tipoIdentificacion.appendChild(valueTipoIdentificacion);
		cliente.appendChild(tipoIdentificacion);

		// CLIENTE --> PV_NUMERO_IDENTIFICACION
		Element numeroIdentificacion = documentoXML.createElement("PV_NUMERO_IDENTIFICACION");
		Text valueNumeroIdentificacion = documentoXML.createTextNode("");
		numeroIdentificacion.appendChild(valueNumeroIdentificacion);
		cliente.appendChild(numeroIdentificacion);
		log.info("437");
		// CLIENTE --> CUENTA_PADRE
		Element cuentaPadre = documentoXML.createElement("CUENTA_PADRE");
		// CLIENTE --> CUENTA_PADRE --> PV_ID_ACCOUNT
		Element idCuenta = documentoXML.createElement("PV_ID_ACCOUNT");
		Text valueIdCuenta = documentoXML.createTextNode("");
		idCuenta.appendChild(valueIdCuenta);
		cuentaPadre.appendChild(idCuenta);
		// CLIENTE --> CUENTA_PADRE --> PV_ROL_CUENTA
		Element rolCuenta = documentoXML.createElement("PV_ROL_CUENTA");
		Text valueRolCuenta = documentoXML.createTextNode("");
		rolCuenta.appendChild(valueRolCuenta);
		cuentaPadre.appendChild(rolCuenta);
		// CLIENTE --> CUENTA_PADRE --> PV_IDENTIFIERTYPE_CUENTA
		Element idTypeCuenta = documentoXML.createElement("PV_IDENTIFIERTYPE_CUENTA");
		Text valueIdTypeCuenta = documentoXML.createTextNode("");
		idTypeCuenta.appendChild(valueIdTypeCuenta);
		cuentaPadre.appendChild(idTypeCuenta);
		cliente.appendChild(cuentaPadre);

		// CLIENTE --> CARACTERISTICA
		Element caracteristica = documentoXML.createElement("CARACTERISTICA");
		// CLIENTE --> CARACTERISTICA --> PV_ETIQUETA_EMPLEADOS
		Element etiquetaEmpleados = documentoXML.createElement("PV_ETIQUETA_EMPLEADOS");
		Text valueEtiquetaEmpleados = documentoXML.createTextNode("");
		etiquetaEmpleados.appendChild(valueEtiquetaEmpleados);
		caracteristica.appendChild(etiquetaEmpleados);
		// CLIENTE --> CARACTERISTICA --> PV_VALOR_EMPLEADOS
		Element valorEmpleados = documentoXML.createElement("PV_VALOR_EMPLEADOS");
		Text valueValorEmpleados = documentoXML.createTextNode("");
		valorEmpleados.appendChild(valueValorEmpleados);
		caracteristica.appendChild(valorEmpleados);
		cliente.appendChild(caracteristica);
		log.info("470");
		// CLIENTE --> PV_SEXO
		Element sexo = documentoXML.createElement("PV_SEXO");
		Text valueSexo = documentoXML.createTextNode(CommonMappings.SEXO);
		sexo.appendChild(valueSexo);
		cliente.appendChild(sexo);

		// CLIENTE --> PV_CLASE_PERSONA
		Element clasePersona = documentoXML.createElement("PV_CLASE_PERSONA");
		Text valueClasePersona = documentoXML.createTextNode(CommonMappings.CLASE_PERSONA);
		clasePersona.appendChild(valueClasePersona);
		cliente.appendChild(clasePersona);
		log.info("482");
		// CLIENTE --> PV_TIPO_PERSONA
		Element tipoPersona = documentoXML.createElement("PV_TIPO_PERSONA");
		Text valueTipoPersona = documentoXML.createTextNode("");
		tipoPersona.appendChild(valueTipoPersona);
		cliente.appendChild(tipoPersona);// CLIENTE --> REPRESENTANTE_LEGAL
		Element representanteLegal = documentoXML.createElement("REPRESENTANTE_LEGAL");
		// CLIENTE --> REPRESENTANTE_LEGAL --> PV_TIPO_IDENTIFICACION_REP_LEGAL
		Element tipoIdRepLegal = documentoXML.createElement("PV_TIPO_IDENTIFICACION_REP_LEGAL");
		Text valueTipoIdRepLegal = documentoXML.createTextNode("");
		tipoIdRepLegal.appendChild(valueTipoIdRepLegal);
		representanteLegal.appendChild(tipoIdRepLegal);
		// CLIENTE --> REPRESENTANTE_LEGAL --> PV_IDENTIFICACION_REP_LEGAL
		Element idRepLegal = documentoXML.createElement("PV_IDENTIFICACION_REP_LEGAL");
		Text valueIdRepLegal = documentoXML.createTextNode("");
		idRepLegal.appendChild(valueIdRepLegal);
		representanteLegal.appendChild(idRepLegal);
		// CLIENTE --> REPRESENTANTE_LEGAL --> PV_NOMBRE_REP_LEGAL
		Element nombreRepLegal = documentoXML.createElement("PV_NOMBRE_REP_LEGAL");
		Text valueNombreRepLegal = documentoXML.createTextNode("");
		nombreRepLegal.appendChild(valueNombreRepLegal);
		representanteLegal.appendChild(nombreRepLegal);
		// CLIENTE --> REPRESENTANTE_LEGAL --> PV_FECHA_CONSTITUCION
		Element fechaConst = documentoXML.createElement("PV_FECHA_CONSTITUCION");
		Text valueFechaConst = documentoXML.createTextNode("");
		fechaConst.appendChild(valueFechaConst);
		representanteLegal.appendChild(fechaConst);
		cliente.appendChild(representanteLegal);
		log.info("510");
		/////////////////////////// 7Arreglo de Ubicaciones

		// MEDIOS_CONTACTO --> PV_EMAIL
		Element mail = documentoXML.createElement("PV_EMAIL");
		Text valueMail = documentoXML.createTextNode("");
		mail.appendChild(valueMail);
		medioContacto.appendChild(mail);

		// MEDIOS_CONTACTO --> PV_TELEFONO1
		Element telefono1 = documentoXML.createElement("PV_TELEFONO1");
		Text valueTelefono1 = documentoXML.createTextNode("");
		telefono1.appendChild(valueTelefono1);
		medioContacto.appendChild(telefono1);

		// MEDIOS_CONTACTO --> PV_TELEFONO2
		Element telefono2 = documentoXML.createElement("PV_TELEFONO2");
		Text valueTelefono2 = documentoXML.createTextNode("");
		telefono2.appendChild(valueTelefono2);
		medioContacto.appendChild(telefono2);
		log.info("530");
		// MEDIOS_CONTACTO --> PV_TELEX
		Element telex = documentoXML.createElement("PV_TELEX");
		Text valueTelex = documentoXML.createTextNode("");
		telex.appendChild(valueTelex);
		medioContacto.appendChild(telex);

		// MEDIOS_CONTACTO --> PV_FAX
		Element fax = documentoXML.createElement("PV_FAX");
		Text valueFax = documentoXML.createTextNode("");
		fax.appendChild(valueFax);
		medioContacto.appendChild(fax);

		// MEDIOS_CONTACTO --> PV_NUME
		Element nume = documentoXML.createElement("PV_NUME");
		Text valueNume = documentoXML.createTextNode("");
		nume.appendChild(valueNume);
		medioContacto.appendChild(nume);

		// MEDIOS_CONTACTO --> PV_WEBSITE
		Element webSite = documentoXML.createElement("PV_WEBSITE");
		Text valueWebSite = documentoXML.createTextNode("");
		webSite.appendChild(valueWebSite);
		medioContacto.appendChild(webSite);
		log.info("554");
		// OBSERVACION --> PV_PROPIETARIO
		Element propietario = documentoXML.createElement("PV_PROPIETARIO");
		Text valuePropietario = documentoXML.createTextNode("");
		propietario.appendChild(valuePropietario);
		observacion.appendChild(propietario);

		// OBSERVACION --> PV_CREADORPOR
		Element creadoPor = documentoXML.createElement("PV_CREADOPOR");
		Text valueCreadoPor = documentoXML.createTextNode("");
		creadoPor.appendChild(valueCreadoPor);
		observacion.appendChild(creadoPor);
		log.info("566");
		// OBSERVACION --> PV_SERVICIOSCONTRATADOS
		Element serviciosContratados = documentoXML.createElement("PV_SERVICIOSCONTRATADOS");
		Text valueServiciosContratados = documentoXML.createTextNode("");
		serviciosContratados.appendChild(valueServiciosContratados);
		observacion.appendChild(serviciosContratados);

		// ---------------------------------------------------------------------------------------------------------------
		log.info("574");
		// Añado al root el elemento cliente
		documentoXML.getDocumentElement().appendChild(cliente);

		for (ContactMedium contactMedium : organization.getContactMedium()) {

			if (!contactMedium.getMediumType().equalsIgnoreCase("emailAddress")
					&& !contactMedium.getMediumType().equalsIgnoreCase("telephoneNumber")
					&& !contactMedium.getMediumType().equalsIgnoreCase("mobileNumber")
					&& !contactMedium.getMediumType().equalsIgnoreCase("webSite")
					&& !contactMedium.getMediumType().equalsIgnoreCase("faxNumber")) {
				Element ubicacion = documentoXML.createElement("UBICACIONES");
				StringBuilder pvdireccion = new StringBuilder();
				// Orden
				// Ubicacion-----------------------------------------------------------------------------------------------------
				log.info("589");
				// UBICACIONES --> PV_SECUBICACION
				Element secUbicacion = documentoXML.createElement("PV_SECUBICACION");
				Text valueSecUbicacion = documentoXML.createTextNode("");
				secUbicacion.appendChild(valueSecUbicacion);
				ubicacion.appendChild(secUbicacion);

				// UBICACIONES --> PV_IDTIPOUBICACION
				Element idTipoUbicacion = documentoXML.createElement("PV_IDTIPOUBICACION");
				Text valueIdTipoUbicacion = documentoXML.createTextNode("");
				if (contactMedium.getMediumType() != null)
					valueIdTipoUbicacion = documentoXML.createTextNode(contactMedium.getMediumType());
				idTipoUbicacion.appendChild(valueIdTipoUbicacion);
				ubicacion.appendChild(idTipoUbicacion);
				log.info("603");
				// UBICACIONES --> PV_DIRECCION
				Element direccion = documentoXML.createElement("PV_DIRECCION");
				Text valueDireccion = documentoXML.createTextNode("");
				direccion.appendChild(valueDireccion);
				ubicacion.appendChild(direccion);

				// UBICACIONES --> PV_IDADDRES
				Element idAddress = documentoXML.createElement("PV_IDADDRES");
				Text valueIdAddress = documentoXML.createTextNode("");
				if (contactMedium.getAddress().getId() != null)
					valueIdAddress = documentoXML.createTextNode(contactMedium.getAddress().getId());
				idAddress.appendChild(valueIdAddress);
				ubicacion.appendChild(idAddress);

				// UBICACIONES --> PV_EXTERNALID
				Element externalId = documentoXML.createElement("PV_EXTERNALID");
				Text valueExternalId = documentoXML.createTextNode("");
				if (contactMedium.getAddress().getExternalId() != null)
					valueExternalId = documentoXML.createTextNode(contactMedium.getAddress().getExternalId());
				externalId.appendChild(valueExternalId);
				ubicacion.appendChild(externalId);
				log.info("625");
				// UBICACIONES --> LOCALIDAD
				Element localidad = documentoXML.createElement("LOCALIDAD");
				// UBICACIONES --> LOCALIDAD --> PV_LOCALITYNAME
				Element localityName = documentoXML.createElement("PV_LOCALITYNAME");
				Text valueLocalityName = documentoXML.createTextNode("");
				localityName.appendChild(valueLocalityName);
				localidad.appendChild(localityName);
				// UBICACIONES --> LOCALIDAD --> PV_LOCALITYTYPE
				Element localityType = documentoXML.createElement("PV_LOCALITYTYPE");
				Text valueLocalityType = documentoXML.createTextNode("");
				localityType.appendChild(valueLocalityType);
				localidad.appendChild(localityType);
				// UBICACIONES --> LOCALIDAD --> PV_IDLOCALIDAD
				Element idLocalidad = documentoXML.createElement("PV_IDLOCALIDAD");
				Text valueIdLocalidad = documentoXML.createTextNode("");
				idLocalidad.appendChild(valueIdLocalidad);
				localidad.appendChild(idLocalidad);
				ubicacion.appendChild(localidad);
				log.info("644");
				// UBICACIONES --> PROVINCIA
				Element provincia = documentoXML.createElement("PROVINCIA");
				// UBICACIONES --> PROVINCIA --> PV_PROVINCETYPE
				Element provinceType = documentoXML.createElement("PV_PROVINCETYPE");
				Text valueProvinceType = documentoXML.createTextNode("");
				provinceType.appendChild(valueProvinceType);
				provincia.appendChild(provinceType);
				// UBICACIONES --> PROVINCIA --> PV_IDPROVINCIA
				Element idProvince = documentoXML.createElement("PV_IDPROVINCIA");
				Text valueIdProvince = documentoXML.createTextNode("");
				idProvince.appendChild(valueIdProvince);
				provincia.appendChild(idProvince);
				// UBICACIONES --> PROVINCIA --> PV_PROVINCENAME
				Element provinceName = documentoXML.createElement("PV_PROVINCENAME");
				Text valueProvinceName = documentoXML.createTextNode("");
				provinceName.appendChild(valueProvinceName);
				provincia.appendChild(provinceName);
				// UBICACIONES --> PROVINCIA --> PV_PROVINCEDES
				Element provinceDes = documentoXML.createElement("PV_PROVINCEDES");
				Text valueProvinceDes = documentoXML.createTextNode("");
				provinceDes.appendChild(valueProvinceDes);
				provincia.appendChild(provinceDes);
				ubicacion.appendChild(provincia);
				log.info("668");
				// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA
				Element subDireccionGeografica = documentoXML.createElement("SUB_DIRECCION_GEOGRAFICA");
				// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_BLOQUE
				Element bloque = documentoXML.createElement("PV_BLOQUE");
				Text valueBloque = documentoXML.createTextNode("");
				bloque.appendChild(valueBloque);
				subDireccionGeografica.appendChild(bloque);
				// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_IDPARROQUIA
				Element idParroquia = documentoXML.createElement("PV_IDPARROQUIA");
				Text valueIdParroquia = documentoXML.createTextNode("");
				idParroquia.appendChild(valueIdParroquia);
				subDireccionGeografica.appendChild(idParroquia);
				// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_PARROQUIA
				Element parroquia = documentoXML.createElement("PV_PARROQUIA");
				Text valueParroquia = documentoXML.createTextNode("");
				parroquia.appendChild(valueParroquia);
				subDireccionGeografica.appendChild(parroquia);
				// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_PARROQUIA
				Element descripcionParroquia = documentoXML.createElement("PV_DESCRIPCION_PARROQUIA");
				Text valueDescripcionParroquia = documentoXML.createTextNode("");
				descripcionParroquia.appendChild(valueDescripcionParroquia);
				subDireccionGeografica.appendChild(descripcionParroquia);
				// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_PISO
				Element piso = documentoXML.createElement("PV_PISO");
				Text valuePiso = documentoXML.createTextNode("");
				piso.appendChild(valuePiso);
				subDireccionGeografica.appendChild(piso);
				// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_DEPARTAMENTO
				Element departamento = documentoXML.createElement("PV_DEPARTAMENTO");
				Text valueDepartamento = documentoXML.createTextNode("");
				departamento.appendChild(valueDepartamento);
				subDireccionGeografica.appendChild(departamento);
				ubicacion.appendChild(subDireccionGeografica);
				log.info("702");
				// UBICACIONES --> UBICACION_GEOGRAFICA
				Element ubicacionGeografica = documentoXML.createElement("UBICACION_GEOGRAFICA");
				// UBICACIONES --> UBICACION_GEOGRAFICA --> PV_LATITUD
				Element latitud = documentoXML.createElement("PV_LATITUD");
				Text valueLatitud = documentoXML.createTextNode("");
				latitud.appendChild(valueLatitud);
				ubicacionGeografica.appendChild(latitud);
				// UBICACIONES --> UBICACION_GEOGRAFICA --> PV_LONGITUD
				Element longitud = documentoXML.createElement("PV_LONGITUD");
				Text valueLongitud = documentoXML.createTextNode("");
				longitud.appendChild(valueLongitud);
				ubicacionGeografica.appendChild(longitud);
				ubicacion.appendChild(ubicacionGeografica);

				// UBICACIONES --> PV_CASILLAPOSTAL
				Element casillaPostal = documentoXML.createElement("PV_CASILLAPOSTAL");
				Text valueCasillaPostal = documentoXML.createTextNode("");
				if (contactMedium.getAddress().getPostcode() != null)
					valueCasillaPostal = documentoXML.createTextNode(contactMedium.getAddress().getPostcode());
				casillaPostal.appendChild(valueCasillaPostal);
				ubicacion.appendChild(casillaPostal);
				log.info("724");
				// UBICACIONES --> AREA
				Element area = documentoXML.createElement("AREA");
				// UBICACIONES --> AREA --> PV_TYPE_AREA
				Element typeArea = documentoXML.createElement("PV_TYPE_AREA");
				Text valueTypeArea = documentoXML.createTextNode("");
				typeArea.appendChild(valueTypeArea);
				area.appendChild(typeArea);
				// UBICACIONES --> AREA --> PV_AREA
				Element pvArea = documentoXML.createElement("PV_AREA");
				Text valuePvArea = documentoXML.createTextNode("");
				pvArea.appendChild(valuePvArea);
				area.appendChild(pvArea);
				// UBICACIONES --> AREA --> PV_CALLE1
				Element calle1 = documentoXML.createElement("PV_CALLE1");
				Text valueCalle1 = documentoXML.createTextNode("");
				calle1.appendChild(valueCalle1);
				area.appendChild(calle1);
				// UBICACIONES --> AREA --> PV_CALLE2
				Element calle2 = documentoXML.createElement("PV_CALLE2");
				Text valueCalle2 = documentoXML.createTextNode("");
				calle2.appendChild(valueCalle2);
				area.appendChild(calle2);
				// UBICACIONES --> AREA --> PV_DESCRIPCION_AREA
				Element descArea = documentoXML.createElement("PV_DESCRIPCION_AREA");
				Text valueDescArea = documentoXML.createTextNode("");
				descArea.appendChild(valueDescArea);
				area.appendChild(descArea);
				ubicacion.appendChild(area);

				// UBICACIONES --> PV_REFERENCIANAME
				Element referenciaName = documentoXML.createElement("PV_REFERENCIANAME");
				Text valueReferenciaName = documentoXML.createTextNode("");
				referenciaName.appendChild(valueReferenciaName);
				ubicacion.appendChild(referenciaName);
				log.info("759");
				// UBICACIONES --> CIUDAD
				Element ciudad = documentoXML.createElement("CIUDAD");
				// UBICACIONES --> CIUDAD --> PV_CITYTYPE
				Element ciudadType = documentoXML.createElement("PV_CITYTYPE");
				Text valueCiudadType = documentoXML.createTextNode("");
				ciudadType.appendChild(valueCiudadType);
				ciudad.appendChild(ciudadType);
				// UBICACIONES --> CIUDAD --> PV_CITYNAME
				Element cityName = documentoXML.createElement("PV_CITYNAME");
				Text valueCityName = documentoXML.createTextNode("");
				cityName.appendChild(valueCityName);
				ciudad.appendChild(cityName);
				// UBICACIONES --> CIUDAD --> PV_IDCIUDAD
				Element idCiudad = documentoXML.createElement("PV_IDCIUDAD");
				Text valueIdCiudad = documentoXML.createTextNode("");
				idCiudad.appendChild(valueIdCiudad);
				ciudad.appendChild(idCiudad);
				// UBICACIONES --> CIUDAD --> PV_CITYDESC
				Element cityDesc = documentoXML.createElement("PV_CITYDESC");
				Text valueCityDesc = documentoXML.createTextNode("");
				cityDesc.appendChild(valueCityDesc);
				ciudad.appendChild(cityDesc);
				ubicacion.appendChild(ciudad);
				log.info("783");
				// UBICACIONES --> PAIS
				Element pais = documentoXML.createElement("PAIS");
				// UBICACIONES --> PAIS --> PV_COUNTRYTYPE
				Element countrytype = documentoXML.createElement("PV_COUNTRYTYPE");
				Text valueCountrytype = documentoXML.createTextNode("");
				countrytype.appendChild(valueCountrytype);
				pais.appendChild(countrytype);
				// UBICACIONES --> PAIS --> PV_ID_PAIS
				Element idPais = documentoXML.createElement("PV_IDPAIS");
				Text valueIdPais = documentoXML.createTextNode("");
				idPais.appendChild(valueIdPais);
				pais.appendChild(idPais);
				// UBICACIONES --> PAIS --> PV_PAISNAME
				Element paisName = documentoXML.createElement("PV_PAISNAME");
				Text valuePaisName = documentoXML.createTextNode("");
				paisName.appendChild(valuePaisName);
				pais.appendChild(paisName);
				ubicacion.appendChild(pais);

				// UBICACIONES --> PV_REGION
				Element region = documentoXML.createElement("PV_REGION");
				Text valueRegion = documentoXML.createTextNode("");
				region.appendChild(valueRegion);
				ubicacion.appendChild(region);
				log.info("808");
				// UBICACIONES --> CARACTERISTICA_DIRE
				Element caracteristicaDire = documentoXML.createElement("CARACTERISTICA_DIRE");
				// UBICACIONES --> CARACTERISTICA_DIRE --> PV_IDCARACTERISTICA
				Element idCaracteristica = documentoXML.createElement("PV_IDCARACTERISTICA");
				Text valueIdCaracteristica = documentoXML.createTextNode("");
				idCaracteristica.appendChild(valueIdCaracteristica);
				caracteristicaDire.appendChild(idCaracteristica);
				// UBICACIONES --> CARACTERISTICA_DIRE --> PV_VALORCARACTERISTICA
				Element valorCaracteristica = documentoXML.createElement("PV_VALORCARACTERISTICA");
				Text valueValorCaracteristica = documentoXML.createTextNode("");
				valorCaracteristica.appendChild(valueValorCaracteristica);
				caracteristicaDire.appendChild(valorCaracteristica);
				// UBICACIONES --> CARACTERISTICA_DIRE --> PV_CARACTERISTICANAME
				Element caracteristicName = documentoXML.createElement("PV_CARACTERISTICANAME");
				Text valueCaracteristicName = documentoXML.createTextNode("");
				caracteristicName.appendChild(valueCaracteristicName);
				caracteristicaDire.appendChild(caracteristicName);

				// UBICACIONES --> CARACTERISTICA_DIRE --> PV_INFORMACIONVALOR
				Element informacionValor = documentoXML.createElement("PV_INFORMACIONVALOR");
				Text valueInformacionValor = documentoXML.createTextNode("");
				informacionValor.appendChild(valueInformacionValor);
				caracteristicaDire.appendChild(informacionValor);
				log.info("832");
				// UBICACIONES --> CARACTERISTICA_DIRE --> PV_INFORMACIONNAME
				Element informacionName = documentoXML.createElement("PV_INFORMACIONNAME");
				Text valueInformacionName = documentoXML.createTextNode("");
				informacionName.appendChild(valueInformacionName);
				caracteristicaDire.appendChild(informacionName);

				// UBICACIONES --> CARACTERISTICA_DIRE --> PV_INDICADORVALOR
				Element indicadorValor = documentoXML.createElement("PV_INDICADORVALOR");
				Text valueIndicadorValor = documentoXML.createTextNode("");
				indicadorValor.appendChild(valueIndicadorValor);
				caracteristicaDire.appendChild(indicadorValor);
				// UBICACIONES --> CARACTERISTICA_DIRE --> PV_INDICADORNAME
				Element indicadorName = documentoXML.createElement("PV_INDICADORNAME");
				Text valueIndicadorName = documentoXML.createTextNode("");
				indicadorName.appendChild(valueIndicadorName);
				caracteristicaDire.appendChild(indicadorName);
				ubicacion.appendChild(caracteristicaDire);

				// UBICACIONES --> ESPECIFICACION_GEOGRAFICA
				Element espGeografica = documentoXML.createElement("ESPECIFICACION_GEOGRAFICA");
				// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_FACTIBILIDADPRODUCTO
				Element factibilidadProducto = documentoXML.createElement("PV_FACTIBILIDADPRODUCTO");
				Text valueFactibilidadProducto = documentoXML.createTextNode("");
				factibilidadProducto.appendChild(valueFactibilidadProducto);
				espGeografica.appendChild(factibilidadProducto);
				log.info("858");
				// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_FACTIBILIDADECONOMICA
				Element factibilidadEconomica = documentoXML.createElement("PV_FACTIBILIDADECONOMICA");
				Text valueFactibilidadEconomica = documentoXML.createTextNode("");
				factibilidadEconomica.appendChild(valueFactibilidadEconomica);
				espGeografica.appendChild(factibilidadEconomica);
				// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_FACTIBILIDADTECNOLOGICA
				Element factibilidadTegnologica = documentoXML.createElement("PV_FACTIBILIDADTECNOLOGICA");
				Text valueFactibilidadTegnologica = documentoXML.createTextNode("");
				factibilidadTegnologica.appendChild(valueFactibilidadTegnologica);
				espGeografica.appendChild(factibilidadTegnologica);
				
				// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_FACTIBILIDADPRIORITY
				Element factibilidadPriority = documentoXML.createElement("PV_FACTIBILIDADPRIORITY");
				Text valueFactibilidadPriority = documentoXML.createTextNode("");
				factibilidadPriority.appendChild(valueFactibilidadPriority);
				espGeografica.appendChild(factibilidadPriority);
				// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_FACTADICIONAL
				Element factibilidadAdicional = documentoXML.createElement("PV_FACTADICIONAL");
				Text valueFactibilidadAdicional = documentoXML.createTextNode("");
				factibilidadAdicional.appendChild(valueFactibilidadAdicional);
				espGeografica.appendChild(factibilidadAdicional);
				ubicacion.appendChild(espGeografica);
				// ---------------------------------------------------------------------------------------------------------------------
				log.info("882");
				// UBICACIONES --> LOCALIDAD
				// UBICACIONES --> LOCALIDAD --> PV_LOCALITYTYPE
				if (contactMedium.getAddress().getLocality().getType() != null)
					valueLocalityType = documentoXML.createTextNode(contactMedium.getAddress().getLocality().getType());
				localityType.appendChild(valueLocalityType);
				// UBICACIONES --> LOCALIDAD --> PV_IDLOCALIDAD
				if (contactMedium.getAddress().getLocality().getId() != null)
					valueIdLocalidad = documentoXML.createTextNode(contactMedium.getAddress().getLocality().getId());
				idLocalidad.appendChild(valueIdLocalidad);
				// UBICACIONES --> LOCALIDAD --> PV_LOCALITYNAME
				if (contactMedium.getAddress().getLocality().getName() != null)
					valueLocalityName = documentoXML.createTextNode(contactMedium.getAddress().getLocality().getName());
				localityName.appendChild(valueLocalityName);

				// UBICACIONES --> PROVINCIA
				// UBICACIONES --> PROVINCIA --> PV_PROVINCETYPE
				if (contactMedium.getAddress().getStateOrProvince().getType() != null)
					valueProvinceType = documentoXML
							.createTextNode(contactMedium.getAddress().getStateOrProvince().getType());
				provinceType.appendChild(valueProvinceType);
				// UBICACIONES --> PROVINCIA --> PV_IDPROVINCIA
				if (contactMedium.getAddress().getStateOrProvince().getId() != null)
					valueIdProvince = documentoXML
							.createTextNode(contactMedium.getAddress().getStateOrProvince().getId());
				idProvince.appendChild(valueIdProvince);
				// UBICACIONES --> PROVINCIA --> PV_PROVINCENAME
				if (contactMedium.getAddress().getStateOrProvince().getName() != null)
					valueProvinceName = documentoXML
							.createTextNode(contactMedium.getAddress().getStateOrProvince().getName());
				provinceName.appendChild(valueProvinceName);
				// UBICACIONES --> PROVINCIA --> PV_PROVINCEDES
				if (contactMedium.getAddress().getStateOrProvince().getDescription() != null)
					valueProvinceDes = documentoXML
							.createTextNode(contactMedium.getAddress().getStateOrProvince().getDescription());
				provinceDes.appendChild(valueProvinceDes);

				// UBICACIONES --> AREA --> PV_CALLE1
				if (contactMedium.getAddress().getStreetName() != null) {
					valueCalle1 = documentoXML.createTextNode(contactMedium.getAddress().getStreetName());
					pvdireccion.append(contactMedium.getAddress().getStreetName() + " ");
				}
				calle1.appendChild(valueCalle1);
				log.info("925");
				// UBICACIONES --> AREA --> PV_CALLE2
				if (contactMedium.getAddress().getStreetName() != null)
					valueCalle2 = documentoXML.createTextNode(contactMedium.getAddress().getStreetName());
				calle2.appendChild(valueCalle2);

				// block
				List<GeographicSubAddress> block = new ArrayList<>();
				contactMedium.getAddress().getGeographicSubAddress().stream()
						.filter(p -> p.getType().equalsIgnoreCase("block")).forEach(p -> block.add(p));
				log.info("935");
				if (block != null && !block.isEmpty()) {
					// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_BLOQUE
					if (block.get(0).getType() != null)
						valueBloque = documentoXML.createTextNode(block.get(0).getType());
					bloque.appendChild(valueBloque);

					// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_IDPARROQUIA
					if (block.get(0).getId() != null)
						valueIdParroquia = documentoXML.createTextNode(block.get(0).getId());
					idParroquia.appendChild(valueIdParroquia);

					// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_PARROQUIA
					if (block.get(0).getName() != null)
						valueParroquia = documentoXML.createTextNode(block.get(0).getName());
					parroquia.appendChild(valueParroquia);

					// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_PARROQUIA
					if (block.get(0).getDescription() != null)
						valueDescripcionParroquia = documentoXML.createTextNode(block.get(0).getDescription());
					descripcionParroquia.appendChild(valueDescripcionParroquia);
				}
				log.info("957");
				// building
				List<GeographicSubAddress> building = new ArrayList<>();
				contactMedium.getAddress().getGeographicSubAddress().stream()
						.filter(p -> p.getType().equalsIgnoreCase("building")).forEach(p -> building.add(p));
			
				if (building != null && !building.isEmpty()) {
					
					// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_PISO
					if (building.get(0).getLevelNumber() != null) {
						valuePiso = documentoXML.createTextNode(building.get(0).getLevelNumber());
						pvdireccion.append(building.get(0).getLevelNumber() + " ");
					}
					piso.appendChild(valuePiso);
					
					// UBICACIONES --> SUB_DIRECCION_GEOGRAFICA --> PV_DEPARTAMENTO
					if (building.get(0).getLevelNumber() != null) {
						valueDepartamento = documentoXML.createTextNode(building.get(0).getSubUnitNumber());
						pvdireccion.append(building.get(0).getSubUnitNumber());
					}
					departamento.appendChild(valueDepartamento);
				}
				log.info("979");
				if (contactMedium.getAddress().getGeographicLocation() != null) {
					if (contactMedium.getAddress().getGeographicLocation().getGeometry() != null) {
						// UBICACIONES --> UBICACION_GEOGRAFICA --> PV_LATITUD
						if (contactMedium.getAddress().getGeographicLocation().getGeometry().get(0).getY() != null)
							valueLatitud = documentoXML.createTextNode(
									contactMedium.getAddress().getGeographicLocation().getGeometry().get(0).getY());
						latitud.appendChild(valueLatitud);

						// UBICACIONES --> UBICACION_GEOGRAFICA --> PV_LONGITUD
						if (contactMedium.getAddress().getGeographicLocation().getGeometry().get(0).getX() != null)
							valueLongitud = documentoXML.createTextNode(
									contactMedium.getAddress().getGeographicLocation().getGeometry().get(0).getX());
						longitud.appendChild(valueLongitud);
					}
				}
				log.info("995");
				// UBICACIONES --> PV_CASILLAPOSTAL
				if (contactMedium.getAddress().getPostcode() != null)
					valueCasillaPostal = documentoXML.createTextNode(contactMedium.getAddress().getPostcode());
				casillaPostal.appendChild(valueCasillaPostal);
				
				if (contactMedium.getAddress().getCharacteristic() != null) {
					for (LocationCharacteristic LocationCharacteristic : contactMedium.getAddress()
							.getCharacteristic()) {
						
						if (LocationCharacteristic.getName().equalsIgnoreCase("economicSocialGroup")) {

							// UBICACIONES --> CARACTERISTICA_DIRE --> PV_CARACTERISTICANAME
							if (LocationCharacteristic.getName() != null)
								valueCaracteristicName = documentoXML.createTextNode(LocationCharacteristic.getName());
							caracteristicName.appendChild(valueCaracteristicName);

							if (LocationCharacteristic.getId() != null) {
								// UBICACIONES --> CARACTERISTICA_DIRE --> PV_IDCARACTERISTICA
								if (LocationCharacteristic.getId() != null)
									valueIdCaracteristica = documentoXML.createTextNode(LocationCharacteristic.getId());
								idCaracteristica.appendChild(valueIdCaracteristica);
							}

							// UBICACIONES --> CARACTERISTICA_DIRE --> PV_VALORCARACTERISTICA
							if (LocationCharacteristic.getValue() != null)
								valueValorCaracteristica = documentoXML
										.createTextNode(LocationCharacteristic.getValue());
							valorCaracteristica.appendChild(valueValorCaracteristica);
						} else if (LocationCharacteristic.getName().equalsIgnoreCase("realEstateProject")) {

							// UBICACIONES --> CARACTERISTICA_DIRE --> PV_INFORMACIONNAME
							if (LocationCharacteristic.getName() != null)
								valueInformacionName = documentoXML.createTextNode(LocationCharacteristic.getName());
							informacionName.appendChild(valueInformacionName);

							// UBICACIONES --> CARACTERISTICA_DIRE --> PV_INFORMACIONVALOR
							if (LocationCharacteristic.getValue() != null)
								valueInformacionValor = documentoXML.createTextNode(LocationCharacteristic.getValue());
							informacionValor.appendChild(valueInformacionValor);

						} else if (LocationCharacteristic.getName().equalsIgnoreCase("autogeneration")) {

							// UBICACIONES --> CARACTERISTICA_DIRE --> PV_INDICADORNAME
							if (LocationCharacteristic.getName() != null)
								valueIndicadorName = documentoXML.createTextNode(LocationCharacteristic.getName());
							indicadorName.appendChild(valueIndicadorName);

							// UBICACIONES --> CARACTERISTICA_DIRE --> PV_INDICADORVALOR
							if (LocationCharacteristic.getValue() != null)
								valueIndicadorValor = documentoXML.createTextNode(LocationCharacteristic.getValue());
							indicadorValor.appendChild(valueIndicadorValor);

						} else {

							// UBICACIONES --> PV_REFERENCIANAME
							if (LocationCharacteristic.getValue() != null)
								valueReferenciaName = documentoXML.createTextNode(LocationCharacteristic.getValue());
							referenciaName.appendChild(valueReferenciaName);

						}

					}
				}
				log.info("1059");
				// UBICACIONES --> CIUDAD
				// UBICACIONES --> CIUDAD --> PV_CITYTYPE
				if (contactMedium.getAddress().getCity() != null) {
					if (contactMedium.getAddress().getCity().getType() != null)
						valueCiudadType = documentoXML.createTextNode(contactMedium.getAddress().getCity().getType());
					ciudadType.appendChild(valueCiudadType);
					
					// UBICACIONES --> CIUDAD --> PV_IDCIUDAD
					if (contactMedium.getAddress().getCity().getId() != null)
						valueIdCiudad = documentoXML.createTextNode(contactMedium.getAddress().getCity().getId());
					idCiudad.appendChild(valueIdCiudad);
					
					// UBICACIONES --> CIUDAD --> PV_CITYNAME
					if (contactMedium.getAddress().getCity().getName() != null)
						valueCityName = documentoXML.createTextNode(contactMedium.getAddress().getCity().getName());
					cityName.appendChild(valueCityName);
					
					// UBICACIONES --> CIUDAD --> PV_CITYDESC
					if (contactMedium.getAddress().getCity().getDescription() != null)
						valueCityDesc = documentoXML
								.createTextNode(contactMedium.getAddress().getCity().getDescription());
					cityDesc.appendChild(valueCityDesc);
				}
				log.info("1083");
				if (contactMedium.getAddress().getCountry() != null) {
					// UBICACIONES --> PAIS
					// UBICACIONES --> PAIS --> PV_COUNTRYTYPE
					if (contactMedium.getAddress().getCountry().getType() != null)
						valueCountrytype = documentoXML
								.createTextNode(contactMedium.getAddress().getCountry().getType());
					countrytype.appendChild(valueCountrytype);
					// UBICACIONES --> PAIS --> PV_ID_PAIS
					if (contactMedium.getAddress().getCountry().getId() != null)
						valueIdPais = documentoXML.createTextNode(contactMedium.getAddress().getCountry().getId());
					idPais.appendChild(valueIdPais);
					// UBICACIONES --> PAIS --> PV_PAISNAME
					if (contactMedium.getAddress().getCountry().getName() != null)
						valuePaisName = documentoXML.createTextNode(contactMedium.getAddress().getCountry().getName());
					paisName.appendChild(valuePaisName);
				}
				// UBICACIONES --> AREA

				log.info("1102");
				
				if (contactMedium.getAddress().getArea() != null) {
					for (Area street : contactMedium.getAddress().getArea()) {
						if (street.getType().equalsIgnoreCase("street")) {
							// UBICACIONES --> AREA --> PV_AREA
							if (street.getId() != null)
								valuePvArea = documentoXML.createTextNode(street.getId());
							pvArea.appendChild(valuePvArea);
							// UBICACIONES --> AREA --> PV_DESCRIPCION_AREA
							if (street.getName() != null)
								valueDescArea = documentoXML.createTextNode(street.getName());
							descArea.appendChild(valueDescArea);
							// UBICACIONES --> AREA --> PV_TYPE_AREA
							if (street.getType() != null)
								valueTypeArea = documentoXML.createTextNode(street.getType());
							typeArea.appendChild(valueTypeArea);

						}
					}
				}
				
				log.info("1124");
				// if (factibilidad != null && !factibilidad.isEmpty()) {
				if (contactMedium.getAddress().getGeographicAddressSpec() != null) {
					for (GeographicAddressSpec factibilidad : contactMedium.getAddress().getGeographicAddressSpec()) {
						if (factibilidad.getCharacteristic() != null) {
							for (Characteristic characteristicFac : factibilidad.getCharacteristic()) {
								if (characteristicFac.getName().equalsIgnoreCase("product")) {
									// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_FACTIBILIDADPRODUCTO
									if (characteristicFac.getValue() != null)
										valueFactibilidadProducto = documentoXML
												.createTextNode(characteristicFac.getValue());
									factibilidadProducto.appendChild(valueFactibilidadProducto);
								} else if (characteristicFac.getName().equalsIgnoreCase("EconomicActivity")) {
									// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_FACTIBILIDADECONOMICA
									if (characteristicFac.getValue() != null)
										valueFactibilidadEconomica = documentoXML
												.createTextNode(characteristicFac.getValue());
									factibilidadEconomica.appendChild(valueFactibilidadEconomica);

								} else if (characteristicFac.getName().equalsIgnoreCase("technology")) {
									// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_FACTIBILIDADTECNOLOGICA
									if (characteristicFac.getValue() != null)
										valueFactibilidadTegnologica = documentoXML
												.createTextNode(characteristicFac.getValue());
									factibilidadTegnologica.appendChild(valueFactibilidadTegnologica);

								} else if (characteristicFac.getName().equalsIgnoreCase("priority")) {
									// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_PV_FACTIBILIDADPRIORITY
									if (characteristicFac.getValue() != null)
										valueFactibilidadPriority = documentoXML
												.createTextNode(characteristicFac.getValue());
									factibilidadPriority.appendChild(valueFactibilidadPriority);

								} else {
									// UBICACIONES --> ESPECIFICACION_GEOGRAFICA --> PV_FACTADICIONAL
									if (characteristicFac.getValue() != null)
										valueFactibilidadAdicional = documentoXML
												.createTextNode(characteristicFac.getValue());
									factibilidadAdicional.appendChild(valueFactibilidadAdicional);

								}

							}
						}
					}

				}
				log.info("1171");
				valueDireccion = documentoXML.createTextNode(pvdireccion.toString());
				direccion.appendChild(valueDireccion);

				// Añado al root el elemento ubicacion
				documentoXML.getDocumentElement().appendChild(ubicacion);
				log.info("1177");
			} else if (contactMedium.getMediumType().equalsIgnoreCase("emailAddress")) {
			
				// MEDIOS_CONTACTO --> PV_EMAIL
				if (contactMedium.getCharacteristic() != null)
					if (contactMedium.getCharacteristic().getEmailAddress() != null)
						valueMail = documentoXML.createTextNode(contactMedium.getCharacteristic().getEmailAddress());
				mail.appendChild(valueMail);

			} else if (contactMedium.getMediumType().equalsIgnoreCase("telephoneNumber")) {
				
				contTelephoneNumber++;
				if (contTelephoneNumber == 1) {
					// MEDIOS_CONTACTO --> PV_TELEFONO1
					if (contactMedium.getCharacteristic() != null)
						if (contactMedium.getCharacteristic().getPhoneNumber() != null)
							valueTelefono1 = documentoXML
									.createTextNode(contactMedium.getCharacteristic().getPhoneNumber());
					telefono1.appendChild(valueTelefono1);

					// MEDIOS_CONTACTO --> PV_TELEX
					if (contactMedium.getAddress() != null)
						if (contactMedium.getAddress().getCity() != null)
							if (contactMedium.getAddress().getCity().getId() != null)
								valueTelex = documentoXML.createTextNode(contactMedium.getAddress().getCity().getId());
					telex.appendChild(valueTelex);
				} else {
					
					// MEDIOS_CONTACTO --> PV_TELEFONO2
					if (contactMedium.getCharacteristic() != null)
						if (contactMedium.getCharacteristic().getPhoneNumber() != null)
							valueTelefono2 = documentoXML
									.createTextNode(contactMedium.getCharacteristic().getPhoneNumber());
					telefono2.appendChild(valueTelefono2);

				}

			} else if (contactMedium.getMediumType().equalsIgnoreCase("mobileNumber")) {
				
				// MEDIOS_CONTACTO --> PV_NUME
				if (contactMedium.getCharacteristic() != null)
					if (contactMedium.getCharacteristic().getMobileNumber() != null)
						valueNume = documentoXML.createTextNode(contactMedium.getCharacteristic().getMobileNumber());
				nume.appendChild(valueNume);

			} else if (contactMedium.getMediumType().equalsIgnoreCase("webSite")) {
				
				// MEDIOS_CONTACTO --> PV_WEBSITE
				if (contactMedium.getCharacteristic() != null)
					if (contactMedium.getCharacteristic().getWebSite() != null)
						valueWebSite = documentoXML.createTextNode(contactMedium.getCharacteristic().getWebSite());
				webSite.appendChild(valueWebSite);

			} else if (contactMedium.getMediumType().equalsIgnoreCase("faxNumber")) {
				
				// MEDIOS_CONTACTO --> PV_FAX
				if (contactMedium.getCharacteristic() != null)
					if (contactMedium.getCharacteristic().getFaxNumber() != null)
						valueFax = documentoXML.createTextNode(contactMedium.getCharacteristic().getFaxNumber());
				fax.appendChild(valueFax);

			}

		}
		
		// Añado al root el elemento medioContacto
		documentoXML.getDocumentElement().appendChild(medioContacto);
		log.info("1244");
		if (organization.getOrganizationIdentification() != null) {
			// CLIENTE --> PV_TIPO_IDENTIFICACION
			if (organization.getOrganizationIdentification().get(0).getIdentificationType() != null)
				valueTipoIdentificacion = documentoXML
						.createTextNode(organization.getOrganizationIdentification().get(0).getIdentificationType());
			tipoIdentificacion.appendChild(valueTipoIdentificacion);

			// CLIENTE --> PV_NUMERO_IDENTIFICACION
			if (organization.getOrganizationIdentification().get(0).getIdentificationId() != null)
				valueNumeroIdentificacion = documentoXML
						.createTextNode(organization.getOrganizationIdentification().get(0).getIdentificationId());
			numeroIdentificacion.appendChild(valueNumeroIdentificacion);

		}
		log.info("1259");
		for (Characteristic characteristic : organization.getPartyCharacteristic()) {

			if (characteristic.getName().equalsIgnoreCase("TipoContribuyente")) {
				// CLIENTE --> PV_TIPO_PERSONA
				if (characteristic.getValue() != null)
					valueTipoPersona = documentoXML.createTextNode(characteristic.getValue());
				tipoPersona.appendChild(valueTipoPersona);
			} else if (characteristic.getName().equalsIgnoreCase("Numero de Empleados")) {
				// CLIENTE --> CARACTERISTICA
				// CLIENTE --> CARACTERISTICA --> PV_ETIQUETA_EMPLEADOS
				if (characteristic.getName() != null)
					valueEtiquetaEmpleados = documentoXML.createTextNode(characteristic.getName());
				etiquetaEmpleados.appendChild(valueEtiquetaEmpleados);

				// CLIENTE --> CARACTERISTICA --> PV_VALOR_EMPLEADOS
				if (characteristic.getValue() != null)
					valueValorEmpleados = documentoXML.createTextNode(characteristic.getValue());
				valorEmpleados.appendChild(valueValorEmpleados);
			}
		}
		log.info("1280");
		List<RelatedPartyRef> listCustomer = new ArrayList<>();
		organization.getRelatedParty().stream().filter(p -> !p.getRole().equalsIgnoreCase("account"))
				.forEach(p -> listCustomer.add(p));

		if (listCustomer != null && !listCustomer.isEmpty()) {

			RelatedPartyRef customer = listCustomer.get(0);
			
			// CLIENTE --> PV_IDCLIENTE
			// if (customer.getId() != null)
			valueIdCliente = documentoXML.createTextNode(customer.getId());
			idCliente.appendChild(valueIdCliente);

			// CLIENTE --> PV_IDENTIFIERTYPE_CLIENTE
			if (customer.getIdentifierType() != null)
				valueIdTypeCliente = documentoXML.createTextNode(customer.getIdentifierType());
			idTypeCliente.appendChild(valueIdTypeCliente);

			// CLIENTE --> PV_ROL_CLIENTE
			if (customer.getRole() != null)
				valueRoleCliente = documentoXML.createTextNode(customer.getRole());
			roleCliente.appendChild(valueRoleCliente);

			// CLIENTE --> PV_CATEGORIA_CLIENTE
			if (customer.getPartyRank() != null)
				valueCategoriaCliente = documentoXML.createTextNode(customer.getPartyRank());
			categoriaCliente.appendChild(valueCategoriaCliente);

			// List<PartyProfileType> listPartyProfileTypeTC = new ArrayList<>();
			// customer.getPartyProfileType().stream().filter(p ->
			// p.getCategory().equalsIgnoreCase("tipoCliente"))
			// .forEach(p -> listPartyProfileTypeTC.add(p));
			log.info("1313");
			if (customer.getPartyProfileType() != null) {
				for (PartyProfileType partyProfileType : customer.getPartyProfileType()) {
					if (partyProfileType.getCategory() != null) {
						if (partyProfileType.getCategory().equalsIgnoreCase("tipoCliente")) {
							if (partyProfileType.getName() != null)
								valueServiciosContratados = documentoXML.createTextNode(partyProfileType.getName());
							serviciosContratados.appendChild(valueServiciosContratados);
						} else if (partyProfileType.getCategory().equalsIgnoreCase("subcategoria")) {

							// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
							// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SUBCATEGORIA
							if (partyProfileType.getCategory() != null)
								valueSubcategoria = documentoXML.createTextNode(partyProfileType.getCategory());
							subcategoria.appendChild(valueSubcategoria);

							// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SUBCATEGORIA
							if (partyProfileType.getName() != null)
								valueNombreSubcategoria = documentoXML.createTextNode(partyProfileType.getName());
							nombreSubcategoria.appendChild(valueNombreSubcategoria);

						}
					}
				}
			}
			log.info("1338");
			if (customer.getExternalReference() != null) {
				for (ExternalReference externalReference : customer.getExternalReference()) {
					if (externalReference.getExternalReferenceType() != null
							&& externalReference.getExternalReferenceType().equalsIgnoreCase("SalesForceCustomerId")) {

						// CLIENTE --> REFERENCIA_EXTERNA

						// CLIENTE --> REFERENCIA_EXTERNA --> NAME
						if (externalReference.getName() != null)
							valueReName = documentoXML.createTextNode(externalReference.getName());
						reName.appendChild(valueReName);
						// CLIENTE --> REFERENCIA_EXTERNA --> PV_REFERENCETYPE
						if (externalReference.getExternalReferenceType() != null)
							valueReReferenceType = documentoXML
									.createTextNode(externalReference.getExternalReferenceType());
						reReferenceType.appendChild(valueReReferenceType);
					}
				}
			}

		}
		log.info("1360");
		List<RelatedPartyRef> listAccount = new ArrayList<>();
		organization.getRelatedParty().stream().filter(p -> p.getRole().equalsIgnoreCase("account"))
				.forEach(p -> listAccount.add(p));

		if (listAccount != null && !listAccount.isEmpty()) {

			RelatedPartyRef account = listAccount.get(0);

			// CLIENTE --> CUENTA_PADRE
			// CLIENTE --> CUENTA_PADRE --> PV_ID_ACCOUNT
			if (account.getId() != null)
				valueIdCuenta = documentoXML.createTextNode(account.getId());
			idCuenta.appendChild(valueIdCuenta);
			// CLIENTE --> CUENTA_PADRE --> PV_IDENTIFIERTYPE_CUENTA
			if (account.getIdentifierType() != null)
				valueIdTypeCuenta = documentoXML.createTextNode(account.getIdentifierType());
			idTypeCuenta.appendChild(valueIdTypeCuenta);
			// CLIENTE --> CUENTA_PADRE --> PV_ROL_CUENTA
			if (account.getRole() != null)
				valueRolCuenta = documentoXML.createTextNode(account.getRole());
			rolCuenta.appendChild(valueRolCuenta);

		}
		log.info("1384");
		if(organization.getPartyProfileType() != null) {
		List<PartyProfileType> segmento = new ArrayList<>();
	
		organization.getPartyProfileType().stream().filter(p -> p.getCategory().equalsIgnoreCase("segmento"))
				.forEach(p -> segmento.add(p));
		log.info("1388");
		if (segmento != null && !segmento.isEmpty()) {
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SEGMENTO
			if (segmento.get(0).getCategory() != null)
				valuepvSegmanto = documentoXML.createTextNode(segmento.get(0).getCategory());
			pvSegmanto.appendChild(valuepvSegmanto);
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SUBCATEGORIA
			if (segmento.get(0).getName() != null)
				valueNombreSegmanto = documentoXML.createTextNode(segmento.get(0).getName());
			nombreSegmanto.appendChild(valueNombreSegmanto);
		}
		
		log.info("1402");
		List<PartyProfileType> segmentoVenta = new ArrayList<>();
		organization.getPartyProfileType().stream().filter(p -> p.getCategory().equalsIgnoreCase("segmentoVenta"))
				.forEach(p -> segmentoVenta.add(p));
		log.info("1404");
		if (segmentoVenta != null && !segmentoVenta.isEmpty()) {
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SEGMENTOVENTA
			if (segmentoVenta.get(0).getCategory() != null)
				valuepvSegmantoVenta = documentoXML.createTextNode(segmentoVenta.get(0).getCategory());
			pvSegmantoVenta.appendChild(valuepvSegmantoVenta);
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SEGMENTO_VENTA
			if (segmentoVenta.get(0).getName() != null)
				valueNombreSegmantoVenta = documentoXML.createTextNode(segmentoVenta.get(0).getName());
			nombreSegmantoVenta.appendChild(valueNombreSegmantoVenta);
		}

		List<PartyProfileType> canalVenta = new ArrayList<>();
		organization.getPartyProfileType().stream().filter(p -> p.getCategory().equalsIgnoreCase("canalVenta"))
				.forEach(p -> canalVenta.add(p));

		if (canalVenta != null && !canalVenta.isEmpty()) {
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SEGMENTOVENTA
			if (canalVenta.get(0).getCategory() != null)
				valuepvCanalVenta = documentoXML.createTextNode(canalVenta.get(0).getCategory());
			pvCanalVenta.appendChild(valuepvCanalVenta);
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SUBCATEGORIA
			if (canalVenta.get(0).getName() != null)
				valueNombreCanalVenta = documentoXML.createTextNode(canalVenta.get(0).getName());
			nombreCanalVenta.appendChild(valueNombreCanalVenta);
		}

		List<PartyProfileType> sector = new ArrayList<>();
		organization.getPartyProfileType().stream().filter(p -> p.getCategory().equalsIgnoreCase("sector"))
				.forEach(p -> sector.add(p));

		if (sector != null && !sector.isEmpty()) {
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SEGMENTOVENTA
			if (sector.get(0).getCategory() != null)
				valuepvSector = documentoXML.createTextNode(sector.get(0).getCategory());
			pvSector.appendChild(valuepvSector);
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SUBCATEGORIA
			if (sector.get(0).getName() != null)
				valueNombreSector = documentoXML.createTextNode(sector.get(0).getName());
			nombreSector.appendChild(valueNombreSector);
		}

		List<PartyProfileType> subSector = new ArrayList<>();
		organization.getPartyProfileType().stream().filter(p -> p.getCategory().equalsIgnoreCase("subSector"))
				.forEach(p -> subSector.add(p));
		log.info("1452");
		if (subSector != null && !subSector.isEmpty()) {
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_SEGMENTOVENTA
			if (subSector.get(0).getCategory() != null)
				valuepvSubSector = documentoXML.createTextNode(subSector.get(0).getCategory());
			pvSubSector.appendChild(valuepvSubSector);

			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_SUBCATEGORIA
			if (subSector.get(0).getName() != null)
				valueNombreSubSector = documentoXML.createTextNode(subSector.get(0).getName());
			nombreSubSector.appendChild(valueNombreSubSector);
		}

		// partyProfileType[].category = 'subSector'
		List<PartyProfileType> actividadEconomica = new ArrayList<>();
		organization.getPartyProfileType().stream().filter(p -> p.getCategory().equalsIgnoreCase("actividadEconomica"))
				.forEach(p -> actividadEconomica.add(p));

		if (actividadEconomica != null && !actividadEconomica.isEmpty()) {
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_ACTIVIDADECONOMICA
			if (actividadEconomica.get(0).getCategory() != null)
				valuepvActividadEconomica = documentoXML.createTextNode(actividadEconomica.get(0).getCategory());
			pvActividadEconomica.appendChild(valuepvActividadEconomica);

			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_ACTIVIDADECONOMICA
			if (actividadEconomica.get(0).getName() != null)
				valueNombreActividadEconomica = documentoXML.createTextNode(actividadEconomica.get(0).getName());
			nombreActividadEconomica.appendChild(valueNombreActividadEconomica);
		}
		log.info("1483");
		// partyProfileType[].category = 'GiroComercial'
		List<PartyProfileType> giroComercial = new ArrayList<>();
		organization.getPartyProfileType().stream().filter(p -> p.getCategory().equalsIgnoreCase("giroComercial"))
				.forEach(p -> giroComercial.add(p));

		if (giroComercial != null && !giroComercial.isEmpty()) {
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL
			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_GIROCOMERCIAL
			if (giroComercial.get(0).getCategory() != null)
				valueGiroComercial = documentoXML.createTextNode(giroComercial.get(0).getCategory());
			pvGiroComercial.appendChild(valueGiroComercial);

			// CLIENTE --> TIPOS_PERFILES -- TIPO_PERFIL-- PV_NOMBRE_GIROCOMERCIAL
			if (giroComercial.get(0).getName() != null)
				valueNombreGiroComercial = documentoXML.createTextNode(giroComercial.get(0).getName());
			nombreGiroComercial.appendChild(valueNombreGiroComercial);
		}
		}
		log.info("1501");
		// OBSERVACION --> PV_PROPIETARIO
		if (organization.getOwner() != null)
			valuePropietario = documentoXML.createTextNode(organization.getOwner());
		propietario.appendChild(valuePropietario);

		// OBSERVACION --> PV_CREADORPOR
		if (organization.getCreatedBy() != null)
			valueCreadoPor = documentoXML.createTextNode(organization.getCreatedBy());
		creadoPor.appendChild(valueCreadoPor);

		// Añado al root el elemento OBSERVACION
		documentoXML.getDocumentElement().appendChild(observacion);
		log.info("1514");
		// Asocio el source con el Document
		DOMSource source = new DOMSource(documentoXML);
		TransformerFactory tf = TransformerFactory.newInstance();
		tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); // Compliant
		tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, ""); // Compliant
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		
		StringWriter writer = new  StringWriter();

		transformer.transform(source, new StreamResult(writer));
		log.info("1528");
		return writer.getBuffer().toString().replaceAll("[\n\r]", "");

	}

}
