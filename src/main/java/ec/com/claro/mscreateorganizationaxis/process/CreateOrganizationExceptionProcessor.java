package ec.com.claro.mscreateorganizationaxis.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import ec.com.claro.mscreateorganizationaxis.excepcion.UpdateOrganizationException;
import ec.com.claro.mscreateorganizationaxis.util.CommonMappings;

public class CreateOrganizationExceptionProcessor implements Processor {
	
		
	@Override
	public void process(Exchange exchange) throws Exception {
		
		CommonMappings commonMappings = new CommonMappings();
		
		if(exchange.getProperty(CommonMappings.RETRIES) == null || Integer.parseInt(exchange.getProperty(CommonMappings.RETRIES).toString()) <= 3) {
			exchange.getOut().setBody(exchange.getProperty("body"));
			commonMappings.setHeaderRequest(exchange);
			exchange.getOut().setHeader("rabbitmq.CORRELATIONID", exchange.getProperty("rabbitMQCorrelatorId"));
			exchange.getOut().setHeader("rabbitmq.EXCHANGE_NAME", "Error");
			if(exchange.getProperty(CommonMappings.RETRIES) == null) {
				exchange.getOut().setHeader(CommonMappings.RETRIES, 2);
				exchange.getOut().setHeader("rabbitmq.ROUTING_KEY", "errorAxis2");
			}else {
				exchange.getOut().setHeader(CommonMappings.RETRIES, (Integer.parseInt(exchange.getProperty(CommonMappings.RETRIES).toString())+1));
				exchange.getOut().setHeader("rabbitmq.ROUTING_KEY", ("errorAxis" + exchange.getOut().getHeader(CommonMappings.RETRIES)));
			}
		} else {
			throw new UpdateOrganizationException("mayor a 3");
		}
		
	}

}
