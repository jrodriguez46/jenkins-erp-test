package ec.com.claro.mscreateorganizationaxis.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import ec.com.claro.mscreateorganizationaxis.util.CommonMappings;

public class CreateOrganizationFaildProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		exchange.getOut().setBody(exchange.getProperty("body"));
		exchange.getOut().setHeader("rabbitmq.ROUTING_KEY", "failed");
		exchange.getOut().setHeader("rabbitmq.EXCHANGE_NAME", "Failed");

		// Headers RabbitMQ -- legacy
		exchange.getOut().setHeader("legacyErrorCode", exchange.getProperty("codeFaultLegacy").toString());
		exchange.getOut().setHeader("legacyErrorMessage", exchange.getProperty("messageFault").toString());
		exchange.getOut().setHeader("legacyName", "Axis");
		exchange.getOut().setHeader(CommonMappings.REQUEST_DATE, exchange.getProperty(CommonMappings.REQUEST_DATE).toString());
		exchange.getOut().setHeader(CommonMappings.MESSAGE_ID, exchange.getProperty(CommonMappings.MESSAGE_ID));
		exchange.getOut().setHeader(CommonMappings.CORRELATOR_ID, exchange.getProperty(CommonMappings.CORRELATOR_ID));
		exchange.getOut().setHeader("MS", "MSCreateOrganizationAxis");
	}

}
