package ec.com.claro.mscreateorganizationaxis.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ec.com.claro.mscreateorganizationaxis.model.CreateOrganizationRes;
import io.swagger.model.Organization;

@Component
public class CommonMappings {

	public static final String SALESFORCE = "SALESFORCE";
	public static final String IP = "0.0.0.0";
	public static final String TRANSACTION_ALIAS = "AXIS_Xfmr_CreateOrg";
	public static final String CLASE_PERSONA = "ECO";
	public static final String SEXO = "M";
	
	//sonarqube
	public static final String TIPO_PERFIL = "TIPO_PERFIL";
	public static final String CORRELATOR_ID = "correlatorId";
	public static final String MESSAGE_ID = "messageId";
	public static final String TARGET_SYSTEM = "targetSystem";
	public static final String ORIGIN_SYSTEM = "originSystem";
	public static final String COUNTRY = "country";
	public static final String IP_CLIENT = "ipClient";
	public static final String CHANNEL = "channel";
	public static final String APPLICATION_ID = "applicationId";
	public static final String SUB_CHANNEL = "subChannel";
	public static final String DEVICE = "device";
	public static final String USER_NAME = "userName";
	public static final String OPERATOR_ID = "operatorId";
	public static final String REQUEST_DATE = "requestDate";
	public static final String RETRIES = "retries";
	public static final String CREATE_ORGANIZATION_AXIS = "direct:createOrganizationAxis";
	public static final String CREATE_ORGANIZATION_AXIS_RETRIES = "direct:createOrganizationAxisRetries";
	

	public void saveHeaders(Exchange exchange) {
		// Headers
		if (exchange.getIn().getHeader(CORRELATOR_ID) != null)
			exchange.setProperty(CORRELATOR_ID, exchange.getIn().getHeader(CORRELATOR_ID));

		if (exchange.getIn().getHeader(MESSAGE_ID) != null)
			exchange.setProperty(MESSAGE_ID, exchange.getIn().getHeader(MESSAGE_ID));
		else
			exchange.setProperty(MESSAGE_ID, "");

		exchange.setProperty(REQUEST_DATE, OffsetDateTime.now());

		if (exchange.getIn().getHeader(TARGET_SYSTEM) != null)
			exchange.setProperty(TARGET_SYSTEM, exchange.getIn().getHeader(TARGET_SYSTEM));

		if (exchange.getIn().getHeader(ORIGIN_SYSTEM) != null)
			exchange.setProperty(ORIGIN_SYSTEM, exchange.getIn().getHeader(ORIGIN_SYSTEM));
		else
			exchange.setProperty(ORIGIN_SYSTEM, "");

		if (exchange.getIn().getHeader(COUNTRY) != null)
			exchange.setProperty(COUNTRY, exchange.getIn().getHeader(COUNTRY));

		if (exchange.getIn().getHeader(IP_CLIENT) != null)
			exchange.setProperty(IP_CLIENT, exchange.getIn().getHeader(IP_CLIENT));
		else
			exchange.setProperty(IP_CLIENT, "");

		if (exchange.getIn().getHeader(CHANNEL) != null)
			exchange.setProperty(CHANNEL, exchange.getIn().getHeader(CHANNEL));
		else
			exchange.setProperty(CHANNEL, "");

		if (exchange.getIn().getHeader(APPLICATION_ID) != null)
			exchange.setProperty(APPLICATION_ID, exchange.getIn().getHeader(APPLICATION_ID));
		else
			exchange.setProperty(APPLICATION_ID, "");

		if (exchange.getIn().getHeader(SUB_CHANNEL) != null)
			exchange.setProperty(SUB_CHANNEL, exchange.getIn().getHeader(SUB_CHANNEL));
		else
			exchange.setProperty(SUB_CHANNEL, "");

		if (exchange.getIn().getHeader(DEVICE) != null)
			exchange.setProperty(DEVICE, exchange.getIn().getHeader(DEVICE));
		else
			exchange.setProperty(DEVICE, "");

		if (exchange.getIn().getHeader(USER_NAME) != null)
			exchange.setProperty(USER_NAME, exchange.getIn().getHeader(USER_NAME));

		if (exchange.getIn().getHeader(OPERATOR_ID) != null)
			exchange.setProperty(OPERATOR_ID, exchange.getIn().getHeader(OPERATOR_ID));

	}

	public void headerResponse(Exchange exchange) {

		OffsetDateTime responseDate = OffsetDateTime.now();
		exchange.getOut().setHeader(CORRELATOR_ID, exchange.getIn().getHeader(CORRELATOR_ID));
		exchange.getOut().setHeader(COUNTRY, exchange.getIn().getHeader(COUNTRY));
		exchange.getOut().setHeader(MESSAGE_ID, exchange.getIn().getHeader(MESSAGE_ID));
		exchange.getOut().setHeader("responseDate", responseDate);
		exchange.getOut().setHeader(TARGET_SYSTEM, exchange.getIn().getHeader(TARGET_SYSTEM));
		exchange.getOut().setHeader("rabbitmq.CORRELATIONID", exchange.getProperty("rabbitMQCorrelatorId"));

	}
	
	public void setHeaderRequest(Exchange exchange) {
		
		exchange.getOut().setHeader(CORRELATOR_ID, exchange.getProperty(CORRELATOR_ID));
		exchange.getOut().setHeader(MESSAGE_ID, exchange.getProperty(MESSAGE_ID));
		exchange.getOut().setHeader(REQUEST_DATE, exchange.getProperty(REQUEST_DATE));
		exchange.getOut().setHeader(TARGET_SYSTEM, exchange.getProperty(TARGET_SYSTEM));
		exchange.getOut().setHeader(ORIGIN_SYSTEM, exchange.getProperty(ORIGIN_SYSTEM));
		exchange.getOut().setHeader(COUNTRY, exchange.getProperty(COUNTRY));
		exchange.getOut().setHeader(IP_CLIENT, exchange.getProperty(IP_CLIENT));
		exchange.getOut().setHeader(CHANNEL, exchange.getProperty(CHANNEL));
		exchange.getOut().setHeader(APPLICATION_ID, exchange.getProperty(APPLICATION_ID));
		exchange.getOut().setHeader(SUB_CHANNEL, exchange.getProperty(SUB_CHANNEL));
		exchange.getOut().setHeader(DEVICE, exchange.getProperty(DEVICE));
		exchange.getOut().setHeader(USER_NAME, exchange.getProperty(USER_NAME));
		exchange.getOut().setHeader(OPERATOR_ID, exchange.getProperty(OPERATOR_ID));

	}

	// este metodo parsea una cadena json a objetos java del modelo swagger
	public Organization parseRequestSuccess(List<HashMap<String, Object>> requestOrganization) throws IOException {

		String reqOrg = new ObjectMapper().writeValueAsString(requestOrganization.get(0));
		
		return parseRequestSuccess(reqOrg);

	}

	// este metodo parsea una cadena json a objetos java del modelo swagger
	public Organization parseRequestSuccess(String requestOrganization) throws IOException {

		// Parsear el Request del Microservicio
		ObjectMapper mapper = new ObjectMapper();
		// configuramos el mapper para deserealizar las fechas en un formato
		// ISO_OFFSET_DATE_TIME
		mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
		JavaTimeModule module = new JavaTimeModule();
		module.addDeserializer(OffsetDateTime.class, new JsonDeserializer<OffsetDateTime>() {
			@Override
			public OffsetDateTime deserialize(JsonParser parser, DeserializationContext context)
					throws IOException {
				return OffsetDateTime.parse(parser.getText(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
			}
		});
		// registramos el modulo creado en el mapper
		mapper.registerModule(module);
		// ejecutamos el readValue del mapper para trasformar al objeto
		
		
		return mapper.readValue(requestOrganization, Organization.class);
	}

	public CreateOrganizationRes parseResLegacy(String bodyResLegacy) throws IOException {
		// Parsear el Request del Legacy
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
		JavaTimeModule module = new JavaTimeModule();
		module.addDeserializer(OffsetDateTime.class, new JsonDeserializer<OffsetDateTime>() {
			@Override
			public OffsetDateTime deserialize(JsonParser parser, DeserializationContext context)
					throws IOException {
				return OffsetDateTime.parse(parser.getText(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
			}
		});
		mapper.registerModule(module);
		
		return mapper.readValue(bodyResLegacy, CreateOrganizationRes.class);
	}

	@SuppressWarnings("unchecked")
	public void saveProperty(Exchange exchange) {
		exchange.setProperty("messageFault", "Mandatory data missing in the structure of the request");
		exchange.setProperty("codeFault", "MSA-001");
		exchange.setProperty("codeHttp", "400");
		exchange.setProperty("actor", "API Management");
		// body
		ArrayList<HashMap<String, Object>> body = exchange.getIn().getBody(ArrayList.class);
		if (body.size() == 1) {
			exchange.setProperty("body", body);
			exchange.setProperty("isMQ", "false");
		} else {
			exchange.setProperty("body", exchange.getIn().getBody(String.class));
			exchange.setProperty("isMQ", "true");
		}

		exchange.setProperty("rabbitMQCorrelatorId", exchange.getIn().getHeader("rabbitmq.CORRELATIONID"));
		exchange.setProperty(RETRIES, exchange.getIn().getHeader(RETRIES));
		exchange.setProperty("codeFaultLegacy", "0");
	}

	public void saveProperty(Exchange exchange, String messageFault, String codeFault, String codeHttp, String actor, String codeFaultLegacy) {
		exchange.setProperty("messageFault", messageFault);
		exchange.setProperty("codeFault", codeFault);
		exchange.setProperty("codeHttp", codeHttp);
		exchange.setProperty("actor", actor);
		exchange.setProperty("codeFaultLegacy", codeFaultLegacy);
	}

}
