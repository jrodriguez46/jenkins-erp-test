package ec.com.claro.mscreateorganizationaxis.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

import io.swagger.model.Organization;



@Component
public class RestRouter extends RouteBuilder {
	
	@Override
    public void configure() throws Exception {
    	
		restConfiguration()
    		.contextPath("/PartyManagement").apiContextPath("/PartyManagement/api-camel")
    		.apiProperty("api.title", "Camel REST API")
    		.apiProperty("api.version", "1.0")
    		.apiProperty("cors", "true")
    		.apiContextRouteId("partyManagement")
    		.bindingMode(RestBindingMode.json)
    		.dataFormatProperty("json.in.disableFeatures", "FAIL_ON_UNKNOWN_PROPERTIES");

		rest("/Organization")
    		.produces("application/json")
    		.consumes("application/json")
    		.post("/").outType(Organization.class).route().routeId("createOrganizationAxis")//.outputType(Organization.class)
    		.doTry()
				.log("START >>>>> MSListAssociationDB")
    			.to("direct:createOrganizationAxis")
    			
    		.endRest();
		
    }
}

