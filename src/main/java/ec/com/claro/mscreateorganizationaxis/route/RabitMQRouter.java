package ec.com.claro.mscreateorganizationaxis.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ec.com.claro.mscreateorganizationaxis.process.OrganizationProcessorReq;
import ec.com.claro.mscreateorganizationaxis.util.CommonMappings;
import ec.com.claro.mscreateorganizationaxis.process.CreateOrganizationExceptionProcessor;
import ec.com.claro.mscreateorganizationaxis.process.CreateOrganizationExceptionProcessorRes;
import ec.com.claro.mscreateorganizationaxis.process.CreateOrganizationFaildProcessor;
import ec.com.claro.mscreateorganizationaxis.process.CreateOrganizationFaildResProcessor;
import ec.com.claro.mscreateorganizationaxis.process.CreateOrganizationResProcessor;

@Component
public class RabitMQRouter extends RouteBuilder {
	
	@Value("${endpoint.rest.createorganizationaxis.microsre}")
	private  String msCreateOrgAxisEndPoint;
	
	@Value("${endpoint.rabbitmq.createorganizationaxis}")
	private  String endpointRabbitMQ;
	
	@Value("${timeout.createorganizationaxis.microsre}")
	private  String timeoutMicroSRE;
	
	@Override
    public void configure() throws Exception {
    	
		
		
		from(endpointRabbitMQ + "/createOrgAxisExch?autoDelete=false&queue=createOrgAxisReq&skipQueueDeclare=false&declare=true&routingKey=createOrganizationAxisKey")
    	.log("START >>>>> MSCreateOrganizationAxis")
    	.to(CommonMappings.CREATE_ORGANIZATION_AXIS)
		.setHeader("operation", constant("createOrganizationAxis"))
		.log("salida: ${body}");
    	
    	from(endpointRabbitMQ + "/Error?autoDelete=false&queue=errorCreateOrganizationAxis&skipQueueDeclare=false&declare=true&routingKey=errorAxis2")
    	.log("RETRIES 1 >>>>> MSCreateOrganizationAxis")
    	.to(CommonMappings.CREATE_ORGANIZATION_AXIS_RETRIES);
    	
    	from(endpointRabbitMQ + "/Error?autoDelete=false&queue=errorCreateOrganizationAxis&skipQueueDeclare=false&declare=true&routingKey=errorAxis3")
    	.log("RETRIES 2 >>>>> MSCreateOrganizationAxis")
    	.to(CommonMappings.CREATE_ORGANIZATION_AXIS_RETRIES);
    	
    	from(endpointRabbitMQ + "/Error?autoDelete=false&queue=errorCreateOrganizationAxis&skipQueueDeclare=false&declare=true&routingKey=errorAxis4")
    	.log("RETRIES 3 >>>>> MSCreateOrganizationAxis")
    	.to(CommonMappings.CREATE_ORGANIZATION_AXIS_RETRIES);
		
    	from(CommonMappings.CREATE_ORGANIZATION_AXIS_RETRIES)
    	.delay(10000)
    	.log("180s")
    	.to(CommonMappings.CREATE_ORGANIZATION_AXIS);
    	
    	from(CommonMappings.CREATE_ORGANIZATION_AXIS)
		.doTry()
    		.process(new OrganizationProcessorReq())
    		.to(msCreateOrgAxisEndPoint + "/EAIServices/SRE/rest/microsre?throwExceptionOnFailure=false&connectionRequestTimeout=" + timeoutMicroSRE)
    		.process(new CreateOrganizationResProcessor())
			.log("END SUCCESS >>>>> MSCreateOrganizationAxis")
		.doCatch(Exception.class)
			.doTry()
				.log("ERROR >>>>> MSCreateOrganizationAxis")
				.process(new CreateOrganizationExceptionProcessor())
				.inOut(endpointRabbitMQ + "/Error?autoDelete=false&queue=errorCreateOrganizationAxis&skipQueueDeclare=false&declare=true&requestTimeout=12000000")
				.process(new CreateOrganizationExceptionProcessorRes())
			.doCatch(Exception.class)
				.process(new CreateOrganizationFaildProcessor())
				.inOnly(endpointRabbitMQ + "/Failed?autoDelete=false&queue=failed&skipQueueDeclare=false&declare=true")
				.log("fallo")
				.process(new CreateOrganizationFaildResProcessor())
    			.log("END FAULT >>>>> MSCreateOrganizationAxis")
			.endDoTry()
		.endDoTry().end();
		
    }
}

