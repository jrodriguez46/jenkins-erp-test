package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

import ec.com.claro.mscreateorganizationaxis.util.CustomerDeserializer;
import ec.com.claro.mscreateorganizationaxis.util.CustomerSerializer;

/**
 * Individual represents a single human being (a man, woman or child). The individual can be a customer, an employee or any other person that the organization needs to store information about.
 */
@ApiModel(description = "Individual represents a single human being (a man, woman or child). The individual can be a customer, an employee or any other person that the organization needs to store information about.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Individual   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("birthDate")
  private OffsetDateTime birthDate = null;

  @JsonProperty("familyName")
  private String familyName = null;

  @JsonProperty("gender")
  private String gender = null;

  @JsonProperty("givenName")
  private String givenName = null;

  @JsonProperty("maritalStatus")
  private String maritalStatus = null;

  @JsonProperty("secondLastName")
  private String secondLastName = null;

  @JsonProperty("nationality")
  private String nationality = null;

  @JsonProperty("title")
  private String title = null;

  @JsonProperty("fullName")
  private String fullName = null;

  @JsonProperty("age")
  private String age = null;

  @JsonProperty("contactMedium")
  @Valid
  private List<ContactMedium> contactMedium = null;

  @JsonProperty("externalReference")
  @Valid
  private List<ExternalReference> externalReference = null;

  @JsonProperty("individualIdentification")
  @Valid
  private List<IndividualIdentification> individualIdentification = null;

  @JsonProperty("languageAbility")
  @Valid
  private List<LanguageAbility> languageAbility = null;

  @JsonProperty("partyCharacteristic")
  @Valid
  private List<Characteristic> partyCharacteristic = null;

  @JsonProperty("relatedParty")
  @Valid
  private List<RelatedPartyRef> relatedParty = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("partyProfileType")
  @Valid
  private List<PartyProfileType> partyProfileType = null;

  @JsonProperty("owner")
  private String owner = null;

  @JsonProperty("createDateTime")
  @JsonSerialize(using = CustomerSerializer.class)
  @JsonDeserialize(using = CustomerDeserializer.class)
  private OffsetDateTime createDateTime = null;

  @JsonProperty("lastUpdateDateTime")
  @JsonSerialize(using = CustomerSerializer.class)
  @JsonDeserialize(using = CustomerDeserializer.class)
  private OffsetDateTime lastUpdateDateTime = null;

  @JsonProperty("createdBy")
  private String createdBy = null;

  @JsonProperty("lastUpdateBy")
  private String lastUpdateBy = null;

  public Individual id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Unique identifier of the individual
   * @return id
  **/
  @ApiModelProperty(value = "Unique identifier of the individual")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Individual href(String href) {
    this.href = href;
    return this;
  }

  /**
   * Hyperlink to access the individual
   * @return href
  **/
  @ApiModelProperty(value = "Hyperlink to access the individual")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public Individual birthDate(OffsetDateTime birthDate) {
    this.birthDate = birthDate;
    return this;
  }

  /**
   * Birth date
   * @return birthDate
  **/
  @ApiModelProperty(value = "Birth date")

  @Valid

  public OffsetDateTime getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(OffsetDateTime birthDate) {
    this.birthDate = birthDate;
  }

  public Individual familyName(String familyName) {
    this.familyName = familyName;
    return this;
  }

  /**
   * Contains the non-chosen or inherited name. Also known as last name in the Western context
   * @return familyName
  **/
  @ApiModelProperty(value = "Contains the non-chosen or inherited name. Also known as last name in the Western context")


  public String getFamilyName() {
    return familyName;
  }

  public void setFamilyName(String familyName) {
    this.familyName = familyName;
  }

  public Individual gender(String gender) {
    this.gender = gender;
    return this;
  }

  /**
   * Gender
   * @return gender
  **/
  @ApiModelProperty(value = "Gender")


  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public Individual givenName(String givenName) {
    this.givenName = givenName;
    return this;
  }

  /**
   * First name of the individual
   * @return givenName
  **/
  @ApiModelProperty(value = "First name of the individual")


  public String getGivenName() {
    return givenName;
  }

  public void setGivenName(String givenName) {
    this.givenName = givenName;
  }

  public Individual maritalStatus(String maritalStatus) {
    this.maritalStatus = maritalStatus;
    return this;
  }

  /**
   * Marital status (married, divorced, widow ...)
   * @return maritalStatus
  **/
  @ApiModelProperty(value = "Marital status (married, divorced, widow ...)")


  public String getMaritalStatus() {
    return maritalStatus;
  }

  public void setMaritalStatus(String maritalStatus) {
    this.maritalStatus = maritalStatus;
  }

  public Individual secondLastName(String secondLastName) {
    this.secondLastName = secondLastName;
    return this;
  }

  /**
   * Second Last Name
   * @return secondLastName
  **/
  @ApiModelProperty(value = "Second Last Name")


  public String getSecondLastName() {
    return secondLastName;
  }

  public void setSecondLastName(String secondLastName) {
    this.secondLastName = secondLastName;
  }

  public Individual nationality(String nationality) {
    this.nationality = nationality;
    return this;
  }

  /**
   * Nationality
   * @return nationality
  **/
  @ApiModelProperty(value = "Nationality")


  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public Individual title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Useful for titles (aristocratic, social,...) Pr, Dr, Sir, ...
   * @return title
  **/
  @ApiModelProperty(value = "Useful for titles (aristocratic, social,...) Pr, Dr, Sir, ...")


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Individual fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

  /**
   * Full name flatten (first, middle, and last names)
   * @return fullName
  **/
  @ApiModelProperty(value = "Full name flatten (first, middle, and last names)")


  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Individual age(String age) {
    this.age = age;
    return this;
  }

  /**
   * Age of the individual
   * @return age
  **/
  @ApiModelProperty(value = "Age of the individual")


  public String getAge() {
    return age;
  }

  public void setAge(String age) {
    this.age = age;
  }

  public Individual contactMedium(List<ContactMedium> contactMedium) {
    this.contactMedium = contactMedium;
    return this;
  }

  public Individual addContactMediumItem(ContactMedium contactMediumItem) {
    if (this.contactMedium == null) {
      this.contactMedium = new ArrayList<ContactMedium>();
    }
    this.contactMedium.add(contactMediumItem);
    return this;
  }

  /**
   * Get contactMedium
   * @return contactMedium
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ContactMedium> getContactMedium() {
    return contactMedium;
  }

  public void setContactMedium(List<ContactMedium> contactMedium) {
    this.contactMedium = contactMedium;
  }

  public Individual externalReference(List<ExternalReference> externalReference) {
    this.externalReference = externalReference;
    return this;
  }

  public Individual addExternalReferenceItem(ExternalReference externalReferenceItem) {
    if (this.externalReference == null) {
      this.externalReference = new ArrayList<ExternalReference>();
    }
    this.externalReference.add(externalReferenceItem);
    return this;
  }

  /**
   * Get externalReference
   * @return externalReference
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ExternalReference> getExternalReference() {
    return externalReference;
  }

  public void setExternalReference(List<ExternalReference> externalReference) {
    this.externalReference = externalReference;
  }

  public Individual individualIdentification(List<IndividualIdentification> individualIdentification) {
    this.individualIdentification = individualIdentification;
    return this;
  }

  public Individual addIndividualIdentificationItem(IndividualIdentification individualIdentificationItem) {
    if (this.individualIdentification == null) {
      this.individualIdentification = new ArrayList<IndividualIdentification>();
    }
    this.individualIdentification.add(individualIdentificationItem);
    return this;
  }

  /**
   * Get individualIdentification
   * @return individualIdentification
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<IndividualIdentification> getIndividualIdentification() {
    return individualIdentification;
  }

  public void setIndividualIdentification(List<IndividualIdentification> individualIdentification) {
    this.individualIdentification = individualIdentification;
  }

  public Individual languageAbility(List<LanguageAbility> languageAbility) {
    this.languageAbility = languageAbility;
    return this;
  }

  public Individual addLanguageAbilityItem(LanguageAbility languageAbilityItem) {
    if (this.languageAbility == null) {
      this.languageAbility = new ArrayList<LanguageAbility>();
    }
    this.languageAbility.add(languageAbilityItem);
    return this;
  }

  /**
   * Get languageAbility
   * @return languageAbility
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<LanguageAbility> getLanguageAbility() {
    return languageAbility;
  }

  public void setLanguageAbility(List<LanguageAbility> languageAbility) {
    this.languageAbility = languageAbility;
  }

  public Individual partyCharacteristic(List<Characteristic> partyCharacteristic) {
    this.partyCharacteristic = partyCharacteristic;
    return this;
  }

  public Individual addPartyCharacteristicItem(Characteristic partyCharacteristicItem) {
    if (this.partyCharacteristic == null) {
      this.partyCharacteristic = new ArrayList<Characteristic>();
    }
    this.partyCharacteristic.add(partyCharacteristicItem);
    return this;
  }

  /**
   * Get partyCharacteristic
   * @return partyCharacteristic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Characteristic> getPartyCharacteristic() {
    return partyCharacteristic;
  }

  public void setPartyCharacteristic(List<Characteristic> partyCharacteristic) {
    this.partyCharacteristic = partyCharacteristic;
  }

  public Individual relatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  public Individual addRelatedPartyItem(RelatedPartyRef relatedPartyItem) {
    if (this.relatedParty == null) {
      this.relatedParty = new ArrayList<RelatedPartyRef>();
    }
    this.relatedParty.add(relatedPartyItem);
    return this;
  }

  /**
   * Get relatedParty
   * @return relatedParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<RelatedPartyRef> getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
  }

  public Individual status(String status) {
    this.status = status;
    return this;
  }

  /**
   * Status of the individual
   * @return status
  **/
  @ApiModelProperty(value = "Status of the individual")


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Individual partyProfileType(List<PartyProfileType> partyProfileType) {
    this.partyProfileType = partyProfileType;
    return this;
  }

  public Individual addPartyProfileTypeItem(PartyProfileType partyProfileTypeItem) {
    if (this.partyProfileType == null) {
      this.partyProfileType = new ArrayList<PartyProfileType>();
    }
    this.partyProfileType.add(partyProfileTypeItem);
    return this;
  }

  /**
   * Get partyProfileType
   * @return partyProfileType
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PartyProfileType> getPartyProfileType() {
    return partyProfileType;
  }

  public void setPartyProfileType(List<PartyProfileType> partyProfileType) {
    this.partyProfileType = partyProfileType;
  }

  public Individual owner(String owner) {
    this.owner = owner;
    return this;
  }

  /**
   * Get owner
   * @return owner
  **/
  @ApiModelProperty(value = "")


  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public Individual createDateTime(OffsetDateTime createDateTime) {
    this.createDateTime = createDateTime;
    return this;
  }

  /**
   * Date when the resource was create
   * @return createDateTime
  **/
  @ApiModelProperty(value = "Date when the resource was create")

  @Valid

  public OffsetDateTime getCreateDateTime() {
    return createDateTime;
  }

  public void setCreateDateTime(OffsetDateTime createDateTime) {
    this.createDateTime = createDateTime;
  }

  public Individual lastUpdateDateTime(OffsetDateTime lastUpdateDateTime) {
    this.lastUpdateDateTime = lastUpdateDateTime;
    return this;
  }

  /**
   * Date when the resource was updated by last time
   * @return lastUpdateDateTime
  **/
  @ApiModelProperty(value = "Date when the resource was updated by last time")

  @Valid

  public OffsetDateTime getLastUpdateDateTime() {
    return lastUpdateDateTime;
  }

  public void setLastUpdateDateTime(OffsetDateTime lastUpdateDateTime) {
    this.lastUpdateDateTime = lastUpdateDateTime;
  }

  public Individual createdBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  /**
   * the user that creates the resource
   * @return createdBy
  **/
  @ApiModelProperty(value = "the user that creates the resource")


  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Individual lastUpdateBy(String lastUpdateBy) {
    this.lastUpdateBy = lastUpdateBy;
    return this;
  }

  /**
   * the user that updates the resource by last time
   * @return lastUpdateBy
  **/
  @ApiModelProperty(value = "the user that updates the resource by last time")


  public String getLastUpdateBy() {
    return lastUpdateBy;
  }

  public void setLastUpdateBy(String lastUpdateBy) {
    this.lastUpdateBy = lastUpdateBy;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Individual individual = (Individual) o;
    return Objects.equals(this.id, individual.id) &&
        Objects.equals(this.href, individual.href) &&
        Objects.equals(this.birthDate, individual.birthDate) &&
        Objects.equals(this.familyName, individual.familyName) &&
        Objects.equals(this.gender, individual.gender) &&
        Objects.equals(this.givenName, individual.givenName) &&
        Objects.equals(this.maritalStatus, individual.maritalStatus) &&
        Objects.equals(this.secondLastName, individual.secondLastName) &&
        Objects.equals(this.nationality, individual.nationality) &&
        Objects.equals(this.title, individual.title) &&
        Objects.equals(this.fullName, individual.fullName) &&
        Objects.equals(this.age, individual.age) &&
        Objects.equals(this.contactMedium, individual.contactMedium) &&
        Objects.equals(this.externalReference, individual.externalReference) &&
        Objects.equals(this.individualIdentification, individual.individualIdentification) &&
        Objects.equals(this.languageAbility, individual.languageAbility) &&
        Objects.equals(this.partyCharacteristic, individual.partyCharacteristic) &&
        Objects.equals(this.relatedParty, individual.relatedParty) &&
        Objects.equals(this.status, individual.status) &&
        Objects.equals(this.partyProfileType, individual.partyProfileType) &&
        Objects.equals(this.owner, individual.owner) &&
        Objects.equals(this.createDateTime, individual.createDateTime) &&
        Objects.equals(this.lastUpdateDateTime, individual.lastUpdateDateTime) &&
        Objects.equals(this.createdBy, individual.createdBy) &&
        Objects.equals(this.lastUpdateBy, individual.lastUpdateBy);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, href, birthDate, familyName, gender, givenName, maritalStatus, secondLastName, nationality, title, fullName, age, contactMedium, externalReference, individualIdentification, languageAbility, partyCharacteristic, relatedParty, status, partyProfileType, owner, createDateTime, lastUpdateDateTime, createdBy, lastUpdateBy);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Individual {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    birthDate: ").append(toIndentedString(birthDate)).append("\n");
    sb.append("    familyName: ").append(toIndentedString(familyName)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("    givenName: ").append(toIndentedString(givenName)).append("\n");
    sb.append("    maritalStatus: ").append(toIndentedString(maritalStatus)).append("\n");
    sb.append("    secondLastName: ").append(toIndentedString(secondLastName)).append("\n");
    sb.append("    nationality: ").append(toIndentedString(nationality)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
    sb.append("    age: ").append(toIndentedString(age)).append("\n");
    sb.append("    contactMedium: ").append(toIndentedString(contactMedium)).append("\n");
    sb.append("    externalReference: ").append(toIndentedString(externalReference)).append("\n");
    sb.append("    individualIdentification: ").append(toIndentedString(individualIdentification)).append("\n");
    sb.append("    languageAbility: ").append(toIndentedString(languageAbility)).append("\n");
    sb.append("    partyCharacteristic: ").append(toIndentedString(partyCharacteristic)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    partyProfileType: ").append(toIndentedString(partyProfileType)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    createDateTime: ").append(toIndentedString(createDateTime)).append("\n");
    sb.append("    lastUpdateDateTime: ").append(toIndentedString(lastUpdateDateTime)).append("\n");
    sb.append("    createdBy: ").append(toIndentedString(createdBy)).append("\n");
    sb.append("    lastUpdateBy: ").append(toIndentedString(lastUpdateBy)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

