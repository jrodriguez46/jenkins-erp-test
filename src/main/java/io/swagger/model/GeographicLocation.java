package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * A GeographicLocation allows describing through coordinate(s) a point, a line or a space
 */
@ApiModel(description = "A GeographicLocation allows describing through coordinate(s) a point, a line or a space")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeographicLocation   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("geometryType")
  private String geometryType = null;

  @JsonProperty("accuracy")
  private String accuracy = null;

  @JsonProperty("spatialRef")
  private String spatialRef = null;

  @JsonProperty("@type")
  private String type = null;

  @JsonProperty("@schemaLocation")
  private String schemaLocation = null;

  @JsonProperty("geometry")
  @Valid
  private List<GeographicPoint> geometry = null;

  public GeographicLocation id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Unique Identifier of a GeoLocation
   * @return id
  **/
  @ApiModelProperty(value = "Unique Identifier of a GeoLocation")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public GeographicLocation href(String href) {
    this.href = href;
    return this;
  }

  /**
   * href of the GeoLocation
   * @return href
  **/
  @ApiModelProperty(value = "href of the GeoLocation")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public GeographicLocation name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of a GeoLocation
   * @return name
  **/
  @ApiModelProperty(value = "Name of a GeoLocation")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public GeographicLocation geometryType(String geometryType) {
    this.geometryType = geometryType;
    return this;
  }

  /**
   * Type allows describing Geolocation form: it could be a point, a line, a polygon, a cylinder, etc....
   * @return geometryType
  **/
  @ApiModelProperty(value = "Type allows describing Geolocation form: it could be a point, a line, a polygon, a cylinder, etc....")


  public String getGeometryType() {
    return geometryType;
  }

  public void setGeometryType(String geometryType) {
    this.geometryType = geometryType;
  }

  public GeographicLocation accuracy(String accuracy) {
    this.accuracy = accuracy;
    return this;
  }

  /**
   * Get accuracy
   * @return accuracy
  **/
  @ApiModelProperty(value = "")


  public String getAccuracy() {
    return accuracy;
  }

  public void setAccuracy(String accuracy) {
    this.accuracy = accuracy;
  }

  public GeographicLocation spatialRef(String spatialRef) {
    this.spatialRef = spatialRef;
    return this;
  }

  /**
   * Get spatialRef
   * @return spatialRef
  **/
  @ApiModelProperty(value = "")


  public String getSpatialRef() {
    return spatialRef;
  }

  public void setSpatialRef(String spatialRef) {
    this.spatialRef = spatialRef;
  }

  public GeographicLocation type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Indicates the type of resource
   * @return type
  **/
  @ApiModelProperty(value = "Indicates the type of resource")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public GeographicLocation schemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
    return this;
  }

  /**
   * A link to the schema describing this REST Resource
   * @return schemaLocation
  **/
  @ApiModelProperty(value = "A link to the schema describing this REST Resource")


  public String getSchemaLocation() {
    return schemaLocation;
  }

  public void setSchemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
  }

  public GeographicLocation geometry(List<GeographicPoint> geometry) {
    this.geometry = geometry;
    return this;
  }

  public GeographicLocation addGeometryItem(GeographicPoint geometryItem) {
    if (this.geometry == null) {
      this.geometry = new ArrayList<GeographicPoint>();
    }
    this.geometry.add(geometryItem);
    return this;
  }

  /**
   * Get geometry
   * @return geometry
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<GeographicPoint> getGeometry() {
    return geometry;
  }

  public void setGeometry(List<GeographicPoint> geometry) {
    this.geometry = geometry;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GeographicLocation geographicLocation = (GeographicLocation) o;
    return Objects.equals(this.id, geographicLocation.id) &&
        Objects.equals(this.href, geographicLocation.href) &&
        Objects.equals(this.name, geographicLocation.name) &&
        Objects.equals(this.geometryType, geographicLocation.geometryType) &&
        Objects.equals(this.accuracy, geographicLocation.accuracy) &&
        Objects.equals(this.spatialRef, geographicLocation.spatialRef) &&
        Objects.equals(this.type, geographicLocation.type) &&
        Objects.equals(this.schemaLocation, geographicLocation.schemaLocation) &&
        Objects.equals(this.geometry, geographicLocation.geometry);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, href, name, geometryType, accuracy, spatialRef, type, schemaLocation, geometry);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GeographicLocation {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    geometryType: ").append(toIndentedString(geometryType)).append("\n");
    sb.append("    accuracy: ").append(toIndentedString(accuracy)).append("\n");
    sb.append("    spatialRef: ").append(toIndentedString(spatialRef)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    schemaLocation: ").append(toIndentedString(schemaLocation)).append("\n");
    sb.append("    geometry: ").append(toIndentedString(geometry)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

