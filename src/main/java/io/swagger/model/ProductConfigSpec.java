package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * The definition of how a Product operates or functions in terms of CharacteristicSpecification(s) and related ResourceSpec(s), ProductSpec(s), ServiceSpec(s).
 */
@ApiModel(description = "The definition of how a Product operates or functions in terms of CharacteristicSpecification(s) and related ResourceSpec(s), ProductSpec(s), ServiceSpec(s).")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductConfigSpec   {
  @JsonProperty("description")
  private String description = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("value")
  private String value = null;

  public ProductConfigSpec description(String description) {
    this.description = description;
    return this;
  }

  /**
   * A narrative that explains in detail what the ConfigurationSpecification is.
   * @return description
  **/
  @ApiModelProperty(value = "A narrative that explains in detail what the ConfigurationSpecification is.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ProductConfigSpec name(String name) {
    this.name = name;
    return this;
  }

  /**
   * A word, term, or phrase by which the ConfigurationSpecification is known and distinguished from other ConfigurationSpecifications.
   * @return name
  **/
  @ApiModelProperty(value = "A word, term, or phrase by which the ConfigurationSpecification is known and distinguished from other ConfigurationSpecifications.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ProductConfigSpec value(String value) {
    this.value = value;
    return this;
  }

  /**
   * Value of the ConfigurationSpecification
   * @return value
  **/
  @ApiModelProperty(value = "Value of the ConfigurationSpecification")


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProductConfigSpec productConfigSpec = (ProductConfigSpec) o;
    return Objects.equals(this.description, productConfigSpec.description) &&
        Objects.equals(this.name, productConfigSpec.name) &&
        Objects.equals(this.value, productConfigSpec.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(description, name, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProductConfigSpec {\n");
    
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

