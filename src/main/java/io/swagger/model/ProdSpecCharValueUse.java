package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * A use of the ProdSpecCharacteristicValue by a ProductOffering to which additional properties (attributes) apply or override the properties of similar properties contained in ProdSpecCharacteristicValue. It should be noted that characteristics which their value(s) addressed by this object must exist in corresponding product specification. The available characteristic values for a ProductSpecCharacteristic in a Product specification can be modified at the ProductOffering level. For example, a characteristic &#39;Color&#39; might have values White, Blue, Green, and Red. But, the list of values can be restricted to e.g. White and Blue in an associated product offering. It should be noted that the list of values in &#39;ProdSpecCharValueUse&#39; is a strict subset of the list of values as defined in the corresponding product specification characteristics.
 */
@ApiModel(description = "A use of the ProdSpecCharacteristicValue by a ProductOffering to which additional properties (attributes) apply or override the properties of similar properties contained in ProdSpecCharacteristicValue. It should be noted that characteristics which their value(s) addressed by this object must exist in corresponding product specification. The available characteristic values for a ProductSpecCharacteristic in a Product specification can be modified at the ProductOffering level. For example, a characteristic 'Color' might have values White, Blue, Green, and Red. But, the list of values can be restricted to e.g. White and Blue in an associated product offering. It should be noted that the list of values in 'ProdSpecCharValueUse' is a strict subset of the list of values as defined in the corresponding product specification characteristics.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProdSpecCharValueUse   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("valueType")
  private String valueType = null;

  @JsonProperty("value")
  private String value = null;

  public ProdSpecCharValueUse id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Identifier of the associated productSpecCharacteristic
   * @return id
  **/
  @ApiModelProperty(value = "Identifier of the associated productSpecCharacteristic")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ProdSpecCharValueUse name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the associated productSpecCharacteristic
   * @return name
  **/
  @ApiModelProperty(value = "Name of the associated productSpecCharacteristic")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ProdSpecCharValueUse description(String description) {
    this.description = description;
    return this;
  }

  /**
   * A narrative that explains in detail what the productSpecCharacteristic is
   * @return description
  **/
  @ApiModelProperty(value = "A narrative that explains in detail what the productSpecCharacteristic is")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ProdSpecCharValueUse valueType(String valueType) {
    this.valueType = valueType;
    return this;
  }

  /**
   * A kind of value that the characteristic can take on, such as numeric, text and so forth
   * @return valueType
  **/
  @ApiModelProperty(value = "A kind of value that the characteristic can take on, such as numeric, text and so forth")


  public String getValueType() {
    return valueType;
  }

  public void setValueType(String valueType) {
    this.valueType = valueType;
  }

  public ProdSpecCharValueUse value(String value) {
    this.value = value;
    return this;
  }

  /**
   * Value that the characteristi
   * @return value
  **/
  @ApiModelProperty(value = "Value that the characteristi")


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProdSpecCharValueUse prodSpecCharValueUse = (ProdSpecCharValueUse) o;
    return Objects.equals(this.id, prodSpecCharValueUse.id) &&
        Objects.equals(this.name, prodSpecCharValueUse.name) &&
        Objects.equals(this.description, prodSpecCharValueUse.description) &&
        Objects.equals(this.valueType, prodSpecCharValueUse.valueType) &&
        Objects.equals(this.value, prodSpecCharValueUse.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, description, valueType, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProdSpecCharValueUse {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    valueType: ").append(toIndentedString(valueType)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

