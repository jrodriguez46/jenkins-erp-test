package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Referes a party related with party information and not only the related Party Information. Defines party or party role linked to a specific entity.
 */
@ApiModel(description = "Referes a party related with party information and not only the related Party Information. Defines party or party role linked to a specific entity.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RelatedPartyRef   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("identifierType")
  private String identifierType = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("role")
  private String role = null;

  @JsonProperty("partyRank")
  private String partyRank = null;

  @JsonProperty("partyProfileType")
  @Valid
  private List<PartyProfileType> partyProfileType = null;

  @JsonProperty("externalReference")
  @Valid
  private List<ExternalReference> externalReference = null;

  @JsonProperty("partyIdentification")
  @Valid
  private List<PartyIdentification> partyIdentification = null;

  @JsonProperty("isLegalEntity")
  private Boolean isLegalEntity = null;

  @JsonProperty("individual")
  private Individual individual = null;

  @JsonProperty("organization")
  private Organization organization = null;

  @JsonProperty("partyCharacteristic")
  @Valid
  private List<Characteristic> partyCharacteristic = null;

  @JsonProperty("@baseType")
  private String baseType = null;

  @JsonProperty("@schemaLocation")
  private String schemaLocation = null;

  @JsonProperty("@type")
  private String type = null;

  @JsonProperty("@referredType")
  private String referredType = null;

  public RelatedPartyRef id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Unique identifier of a related entity.
   * @return id
  **/
  @ApiModelProperty(value = "Unique identifier of a related entity.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public RelatedPartyRef identifierType(String identifierType) {
    this.identifierType = identifierType;
    return this;
  }

  /**
   * Type of identifier used to the party
   * @return identifierType
  **/
  @ApiModelProperty(value = "Type of identifier used to the party")


  public String getIdentifierType() {
    return identifierType;
  }

  public void setIdentifierType(String identifierType) {
    this.identifierType = identifierType;
  }

  public RelatedPartyRef href(String href) {
    this.href = href;
    return this;
  }

  /**
   * Reference of the related entity.
   * @return href
  **/
  @ApiModelProperty(value = "Reference of the related entity.")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public RelatedPartyRef name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the related entity.
   * @return name
  **/
  @ApiModelProperty(value = "Name of the related entity.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public RelatedPartyRef role(String role) {
    this.role = role;
    return this;
  }

  /**
   * Role played by the related party
   * @return role
  **/
  @ApiModelProperty(value = "Role played by the related party")


  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public RelatedPartyRef partyRank(String partyRank) {
    this.partyRank = partyRank;
    return this;
  }

  /**
   * Degree of importance relative to other partys.
   * @return partyRank
  **/
  @ApiModelProperty(value = "Degree of importance relative to other partys.")


  public String getPartyRank() {
    return partyRank;
  }

  public void setPartyRank(String partyRank) {
    this.partyRank = partyRank;
  }

  public RelatedPartyRef partyProfileType(List<PartyProfileType> partyProfileType) {
    this.partyProfileType = partyProfileType;
    return this;
  }

  public RelatedPartyRef addPartyProfileTypeItem(PartyProfileType partyProfileTypeItem) {
    if (this.partyProfileType == null) {
      this.partyProfileType = new ArrayList<PartyProfileType>();
    }
    this.partyProfileType.add(partyProfileTypeItem);
    return this;
  }

  /**
   * Get partyProfileType
   * @return partyProfileType
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PartyProfileType> getPartyProfileType() {
    return partyProfileType;
  }

  public void setPartyProfileType(List<PartyProfileType> partyProfileType) {
    this.partyProfileType = partyProfileType;
  }

  public RelatedPartyRef externalReference(List<ExternalReference> externalReference) {
    this.externalReference = externalReference;
    return this;
  }

  public RelatedPartyRef addExternalReferenceItem(ExternalReference externalReferenceItem) {
    if (this.externalReference == null) {
      this.externalReference = new ArrayList<ExternalReference>();
    }
    this.externalReference.add(externalReferenceItem);
    return this;
  }

  /**
   * Get externalReference
   * @return externalReference
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ExternalReference> getExternalReference() {
    return externalReference;
  }

  public void setExternalReference(List<ExternalReference> externalReference) {
    this.externalReference = externalReference;
  }

  public RelatedPartyRef partyIdentification(List<PartyIdentification> partyIdentification) {
    this.partyIdentification = partyIdentification;
    return this;
  }

  public RelatedPartyRef addPartyIdentificationItem(PartyIdentification partyIdentificationItem) {
    if (this.partyIdentification == null) {
      this.partyIdentification = new ArrayList<PartyIdentification>();
    }
    this.partyIdentification.add(partyIdentificationItem);
    return this;
  }

  /**
   * Get partyIdentification
   * @return partyIdentification
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PartyIdentification> getPartyIdentification() {
    return partyIdentification;
  }

  public void setPartyIdentification(List<PartyIdentification> partyIdentification) {
    this.partyIdentification = partyIdentification;
  }

  public RelatedPartyRef isLegalEntity(Boolean isLegalEntity) {
    this.isLegalEntity = isLegalEntity;
    return this;
  }

  /**
   * If value is true, the party is a legal entity.
   * @return isLegalEntity
  **/
  @ApiModelProperty(value = "If value is true, the party is a legal entity.")


  public Boolean isIsLegalEntity() {
    return isLegalEntity;
  }

  public void setIsLegalEntity(Boolean isLegalEntity) {
    this.isLegalEntity = isLegalEntity;
  }

  public RelatedPartyRef individual(Individual individual) {
    this.individual = individual;
    return this;
  }

  /**
   * Get individual
   * @return individual
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Individual getIndividual() {
    return individual;
  }

  public void setIndividual(Individual individual) {
    this.individual = individual;
  }

  public RelatedPartyRef organization(Organization organization) {
    this.organization = organization;
    return this;
  }

  /**
   * Get organization
   * @return organization
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Organization getOrganization() {
    return organization;
  }

  public void setOrganization(Organization organization) {
    this.organization = organization;
  }

  public RelatedPartyRef partyCharacteristic(List<Characteristic> partyCharacteristic) {
    this.partyCharacteristic = partyCharacteristic;
    return this;
  }

  public RelatedPartyRef addPartyCharacteristicItem(Characteristic partyCharacteristicItem) {
    if (this.partyCharacteristic == null) {
      this.partyCharacteristic = new ArrayList<Characteristic>();
    }
    this.partyCharacteristic.add(partyCharacteristicItem);
    return this;
  }

  /**
   * Get partyCharacteristic
   * @return partyCharacteristic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Characteristic> getPartyCharacteristic() {
    return partyCharacteristic;
  }

  public void setPartyCharacteristic(List<Characteristic> partyCharacteristic) {
    this.partyCharacteristic = partyCharacteristic;
  }

  public RelatedPartyRef baseType(String baseType) {
    this.baseType = baseType;
    return this;
  }

  /**
   * When sub-classing, this defines the super-class
   * @return baseType
  **/
  @ApiModelProperty(value = "When sub-classing, this defines the super-class")


  public String getBaseType() {
    return baseType;
  }

  public void setBaseType(String baseType) {
    this.baseType = baseType;
  }

  public RelatedPartyRef schemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
    return this;
  }

  /**
   * A URI to a JSON-Schema file that defines additional attributes and relationships
   * @return schemaLocation
  **/
  @ApiModelProperty(value = "A URI to a JSON-Schema file that defines additional attributes and relationships")


  public String getSchemaLocation() {
    return schemaLocation;
  }

  public void setSchemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
  }

  public RelatedPartyRef type(String type) {
    this.type = type;
    return this;
  }

  /**
   * When sub-classing, this defines the sub-class entity name
   * @return type
  **/
  @ApiModelProperty(value = "When sub-classing, this defines the sub-class entity name")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public RelatedPartyRef referredType(String referredType) {
    this.referredType = referredType;
    return this;
  }

  /**
   * The actual type of the target instance when needed for disambiguation.
   * @return referredType
  **/
  @ApiModelProperty(value = "The actual type of the target instance when needed for disambiguation.")


  public String getReferredType() {
    return referredType;
  }

  public void setReferredType(String referredType) {
    this.referredType = referredType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RelatedPartyRef relatedPartyRef = (RelatedPartyRef) o;
    return Objects.equals(this.id, relatedPartyRef.id) &&
        Objects.equals(this.identifierType, relatedPartyRef.identifierType) &&
        Objects.equals(this.href, relatedPartyRef.href) &&
        Objects.equals(this.name, relatedPartyRef.name) &&
        Objects.equals(this.role, relatedPartyRef.role) &&
        Objects.equals(this.partyRank, relatedPartyRef.partyRank) &&
        Objects.equals(this.partyProfileType, relatedPartyRef.partyProfileType) &&
        Objects.equals(this.externalReference, relatedPartyRef.externalReference) &&
        Objects.equals(this.partyIdentification, relatedPartyRef.partyIdentification) &&
        Objects.equals(this.isLegalEntity, relatedPartyRef.isLegalEntity) &&
        Objects.equals(this.individual, relatedPartyRef.individual) &&
        Objects.equals(this.organization, relatedPartyRef.organization) &&
        Objects.equals(this.partyCharacteristic, relatedPartyRef.partyCharacteristic) &&
        Objects.equals(this.baseType, relatedPartyRef.baseType) &&
        Objects.equals(this.schemaLocation, relatedPartyRef.schemaLocation) &&
        Objects.equals(this.type, relatedPartyRef.type) &&
        Objects.equals(this.referredType, relatedPartyRef.referredType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, identifierType, href, name, role, partyRank, partyProfileType, externalReference, partyIdentification, isLegalEntity, individual, organization, partyCharacteristic, baseType, schemaLocation, type, referredType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RelatedPartyRef {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    identifierType: ").append(toIndentedString(identifierType)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("    partyRank: ").append(toIndentedString(partyRank)).append("\n");
    sb.append("    partyProfileType: ").append(toIndentedString(partyProfileType)).append("\n");
    sb.append("    externalReference: ").append(toIndentedString(externalReference)).append("\n");
    sb.append("    partyIdentification: ").append(toIndentedString(partyIdentification)).append("\n");
    sb.append("    isLegalEntity: ").append(toIndentedString(isLegalEntity)).append("\n");
    sb.append("    individual: ").append(toIndentedString(individual)).append("\n");
    sb.append("    organization: ").append(toIndentedString(organization)).append("\n");
    sb.append("    partyCharacteristic: ").append(toIndentedString(partyCharacteristic)).append("\n");
    sb.append("    baseType: ").append(toIndentedString(baseType)).append("\n");
    sb.append("    schemaLocation: ").append(toIndentedString(schemaLocation)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    referredType: ").append(toIndentedString(referredType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

