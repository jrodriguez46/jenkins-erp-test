package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * PartyCreditBalance
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartyCreditBalance   {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("creditBalanceAmount")
  private Money creditBalanceAmount = null;

  @JsonProperty("validFor")
  private TimePeriod validFor = null;

  public PartyCreditBalance name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the PartyCreditBalance
   * @return name
  **/
  @ApiModelProperty(value = "Name of the PartyCreditBalance")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PartyCreditBalance creditBalanceAmount(Money creditBalanceAmount) {
    this.creditBalanceAmount = creditBalanceAmount;
    return this;
  }

  /**
   * The amount of the credit and the currency in which the amount is expressed.
   * @return creditBalanceAmount
  **/
  @ApiModelProperty(value = "The amount of the credit and the currency in which the amount is expressed.")

  @Valid

  public Money getCreditBalanceAmount() {
    return creditBalanceAmount;
  }

  public void setCreditBalanceAmount(Money creditBalanceAmount) {
    this.creditBalanceAmount = creditBalanceAmount;
  }

  public PartyCreditBalance validFor(TimePeriod validFor) {
    this.validFor = validFor;
    return this;
  }

  /**
   * Get validFor
   * @return validFor
  **/
  @ApiModelProperty(value = "")

  @Valid

  public TimePeriod getValidFor() {
    return validFor;
  }

  public void setValidFor(TimePeriod validFor) {
    this.validFor = validFor;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PartyCreditBalance partyCreditBalance = (PartyCreditBalance) o;
    return Objects.equals(this.name, partyCreditBalance.name) &&
        Objects.equals(this.creditBalanceAmount, partyCreditBalance.creditBalanceAmount) &&
        Objects.equals(this.validFor, partyCreditBalance.validFor);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, creditBalanceAmount, validFor);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PartyCreditBalance {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    creditBalanceAmount: ").append(toIndentedString(creditBalanceAmount)).append("\n");
    sb.append("    validFor: ").append(toIndentedString(validFor)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

