package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ec.com.claro.mscreateorganizationaxis.util.CustomerDeserializer;
import ec.com.claro.mscreateorganizationaxis.util.CustomerSerializer;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Credit rating is to manage the estimation of credit score of the customer.
 */
@ApiModel(description = "Credit rating is to manage the estimation of credit score of the customer.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CheckPartyCreditRating   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("creditAgencyName")
  private String creditAgencyName = null;

  @JsonProperty("creditAgencyType")
  private String creditAgencyType = null;

  @JsonProperty("ratingRisk")
  private Integer ratingRisk = null;

  @JsonProperty("ratingScore")
  private Integer ratingScore = null;

  @JsonProperty("ratingDate")
  @JsonSerialize(using = CustomerSerializer.class)
  @JsonDeserialize(using = CustomerDeserializer.class)
  private OffsetDateTime ratingDate = null;

  @JsonProperty("creditLimit")
  private Money creditLimit = null;

  @JsonProperty("partyCreditBalance")
  @Valid
  private List<PartyCreditBalance> partyCreditBalance = null;

  @JsonProperty("validFor")
  private TimePeriod validFor = null;

  @JsonProperty("relatedParty")
  @Valid
  private List<RelatedPartyRef> relatedParty = null;

  @JsonProperty("relatedPayment")
  @Valid
  private List<PaymentRefType> relatedPayment = null;

  @JsonProperty("quote")
  private QuoteRef quote = null;

  @JsonProperty("policyRule")
  @Valid
  private List<PolicyRuleRef> policyRule = null;

  @JsonProperty("characteristic")
  @Valid
  private List<Characteristic> characteristic = null;

  @JsonProperty("createDateTime")
  @JsonSerialize(using = CustomerSerializer.class)
  @JsonDeserialize(using = CustomerDeserializer.class)
  private OffsetDateTime createDateTime = null;

  @JsonProperty("lastUpdateDateTime")
  @JsonSerialize(using = CustomerSerializer.class)
  @JsonDeserialize(using = CustomerDeserializer.class)
  private OffsetDateTime lastUpdateDateTime = null;

  @JsonProperty("createdBy")
  private String createdBy = null;

  @JsonProperty("lastUpdateBy")
  private String lastUpdateBy = null;

  @JsonProperty("externalReference")
  @Valid
  private List<ExternalReference> externalReference = null;

  public CheckPartyCreditRating id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Identifier of the CheckPartyCreditRating.
   * @return id
  **/
  @ApiModelProperty(value = "Identifier of the CheckPartyCreditRating.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public CheckPartyCreditRating type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Type of the CheckPartyCreditRating.
   * @return type
  **/
  @ApiModelProperty(value = "Type of the CheckPartyCreditRating.")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public CheckPartyCreditRating description(String description) {
    this.description = description;
    return this;
  }

  /**
   * A narrative that explains what the CheckPartyCreditRating is.
   * @return description
  **/
  @ApiModelProperty(value = "A narrative that explains what the CheckPartyCreditRating is.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public CheckPartyCreditRating status(String status) {
    this.status = status;
    return this;
  }

  /**
   * Status of the CheckPartyCreditRating  
   * @return status
  **/
  @ApiModelProperty(value = "Status of the CheckPartyCreditRating  ")


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public CheckPartyCreditRating creditAgencyName(String creditAgencyName) {
    this.creditAgencyName = creditAgencyName;
    return this;
  }

  /**
   * Name of the credit agency giving the score.
   * @return creditAgencyName
  **/
  @ApiModelProperty(value = "Name of the credit agency giving the score.")


  public String getCreditAgencyName() {
    return creditAgencyName;
  }

  public void setCreditAgencyName(String creditAgencyName) {
    this.creditAgencyName = creditAgencyName;
  }

  public CheckPartyCreditRating creditAgencyType(String creditAgencyType) {
    this.creditAgencyType = creditAgencyType;
    return this;
  }

  /**
   * Type of the credit agency giving the score
   * @return creditAgencyType
  **/
  @ApiModelProperty(value = "Type of the credit agency giving the score")


  public String getCreditAgencyType() {
    return creditAgencyType;
  }

  public void setCreditAgencyType(String creditAgencyType) {
    this.creditAgencyType = creditAgencyType;
  }

  public CheckPartyCreditRating ratingRisk(Integer ratingRisk) {
    this.ratingRisk = ratingRisk;
    return this;
  }

  /**
   * This is an integer whose value is used to rate the risk of this Party paying late or defaulting versus paying on time
   * @return ratingRisk
  **/
  @ApiModelProperty(value = "This is an integer whose value is used to rate the risk of this Party paying late or defaulting versus paying on time")


  public Integer getRatingRisk() {
    return ratingRisk;
  }

  public void setRatingRisk(Integer ratingRisk) {
    this.ratingRisk = ratingRisk;
  }

  public CheckPartyCreditRating ratingScore(Integer ratingScore) {
    this.ratingScore = ratingScore;
    return this;
  }

  /**
   * A measure of a party’s creditworthiness calculated on the basis of a combination of factors such as their income and credit history
   * @return ratingScore
  **/
  @ApiModelProperty(value = "A measure of a party’s creditworthiness calculated on the basis of a combination of factors such as their income and credit history")


  public Integer getRatingScore() {
    return ratingScore;
  }

  public void setRatingScore(Integer ratingScore) {
    this.ratingScore = ratingScore;
  }

  public CheckPartyCreditRating ratingDate(OffsetDateTime ratingDate) {
    this.ratingDate = ratingDate;
    return this;
  }

  /**
   * The date the rating was established.
   * @return ratingDate
  **/
  @ApiModelProperty(value = "The date the rating was established.")

  @Valid

  public OffsetDateTime getRatingDate() {
    return ratingDate;
  }

  public void setRatingDate(OffsetDateTime ratingDate) {
    this.ratingDate = ratingDate;
  }

  public CheckPartyCreditRating creditLimit(Money creditLimit) {
    this.creditLimit = creditLimit;
    return this;
  }

  /**
   * Get creditLimit
   * @return creditLimit
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Money getCreditLimit() {
    return creditLimit;
  }

  public void setCreditLimit(Money creditLimit) {
    this.creditLimit = creditLimit;
  }

  public CheckPartyCreditRating partyCreditBalance(List<PartyCreditBalance> partyCreditBalance) {
    this.partyCreditBalance = partyCreditBalance;
    return this;
  }

  public CheckPartyCreditRating addPartyCreditBalanceItem(PartyCreditBalance partyCreditBalanceItem) {
    if (this.partyCreditBalance == null) {
      this.partyCreditBalance = new ArrayList<PartyCreditBalance>();
    }
    this.partyCreditBalance.add(partyCreditBalanceItem);
    return this;
  }

  /**
   * A credit balance for a Party.
   * @return partyCreditBalance
  **/
  @ApiModelProperty(value = "A credit balance for a Party.")

  @Valid

  public List<PartyCreditBalance> getPartyCreditBalance() {
    return partyCreditBalance;
  }

  public void setPartyCreditBalance(List<PartyCreditBalance> partyCreditBalance) {
    this.partyCreditBalance = partyCreditBalance;
  }

  public CheckPartyCreditRating validFor(TimePeriod validFor) {
    this.validFor = validFor;
    return this;
  }

  /**
   * Get validFor
   * @return validFor
  **/
  @ApiModelProperty(value = "")

  @Valid

  public TimePeriod getValidFor() {
    return validFor;
  }

  public void setValidFor(TimePeriod validFor) {
    this.validFor = validFor;
  }

  public CheckPartyCreditRating relatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  public CheckPartyCreditRating addRelatedPartyItem(RelatedPartyRef relatedPartyItem) {
    if (this.relatedParty == null) {
      this.relatedParty = new ArrayList<RelatedPartyRef>();
    }
    this.relatedParty.add(relatedPartyItem);
    return this;
  }

  /**
   * Get relatedParty
   * @return relatedParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<RelatedPartyRef> getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
  }

  public CheckPartyCreditRating relatedPayment(List<PaymentRefType> relatedPayment) {
    this.relatedPayment = relatedPayment;
    return this;
  }

  public CheckPartyCreditRating addRelatedPaymentItem(PaymentRefType relatedPaymentItem) {
    if (this.relatedPayment == null) {
      this.relatedPayment = new ArrayList<PaymentRefType>();
    }
    this.relatedPayment.add(relatedPaymentItem);
    return this;
  }

  /**
   * Get relatedPayment
   * @return relatedPayment
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PaymentRefType> getRelatedPayment() {
    return relatedPayment;
  }

  public void setRelatedPayment(List<PaymentRefType> relatedPayment) {
    this.relatedPayment = relatedPayment;
  }

  public CheckPartyCreditRating quote(QuoteRef quote) {
    this.quote = quote;
    return this;
  }

  /**
   * Get quote
   * @return quote
  **/
  @ApiModelProperty(value = "")

  @Valid

  public QuoteRef getQuote() {
    return quote;
  }

  public void setQuote(QuoteRef quote) {
    this.quote = quote;
  }

  public CheckPartyCreditRating policyRule(List<PolicyRuleRef> policyRule) {
    this.policyRule = policyRule;
    return this;
  }

  public CheckPartyCreditRating addPolicyRuleItem(PolicyRuleRef policyRuleItem) {
    if (this.policyRule == null) {
      this.policyRule = new ArrayList<PolicyRuleRef>();
    }
    this.policyRule.add(policyRuleItem);
    return this;
  }

  /**
   * Get policyRule
   * @return policyRule
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PolicyRuleRef> getPolicyRule() {
    return policyRule;
  }

  public void setPolicyRule(List<PolicyRuleRef> policyRule) {
    this.policyRule = policyRule;
  }

  public CheckPartyCreditRating characteristic(List<Characteristic> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public CheckPartyCreditRating addCharacteristicItem(Characteristic characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<Characteristic>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

  /**
   * Get characteristic
   * @return characteristic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Characteristic> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<Characteristic> characteristic) {
    this.characteristic = characteristic;
  }

  public CheckPartyCreditRating createDateTime(OffsetDateTime createDateTime) {
    this.createDateTime = createDateTime;
    return this;
  }

  /**
   * Date when the resource was create
   * @return createDateTime
  **/
  @ApiModelProperty(value = "Date when the resource was create")

  @Valid

  public OffsetDateTime getCreateDateTime() {
    return createDateTime;
  }

  public void setCreateDateTime(OffsetDateTime createDateTime) {
    this.createDateTime = createDateTime;
  }

  public CheckPartyCreditRating lastUpdateDateTime(OffsetDateTime lastUpdateDateTime) {
    this.lastUpdateDateTime = lastUpdateDateTime;
    return this;
  }

  /**
   * Date when the resource was updated by last time
   * @return lastUpdateDateTime
  **/
  @ApiModelProperty(value = "Date when the resource was updated by last time")

  @Valid

  public OffsetDateTime getLastUpdateDateTime() {
    return lastUpdateDateTime;
  }

  public void setLastUpdateDateTime(OffsetDateTime lastUpdateDateTime) {
    this.lastUpdateDateTime = lastUpdateDateTime;
  }

  public CheckPartyCreditRating createdBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  /**
   * the user that creates the resource
   * @return createdBy
  **/
  @ApiModelProperty(value = "the user that creates the resource")


  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public CheckPartyCreditRating lastUpdateBy(String lastUpdateBy) {
    this.lastUpdateBy = lastUpdateBy;
    return this;
  }

  /**
   * the user that updates the resource by last time
   * @return lastUpdateBy
  **/
  @ApiModelProperty(value = "the user that updates the resource by last time")


  public String getLastUpdateBy() {
    return lastUpdateBy;
  }

  public void setLastUpdateBy(String lastUpdateBy) {
    this.lastUpdateBy = lastUpdateBy;
  }

  public CheckPartyCreditRating externalReference(List<ExternalReference> externalReference) {
    this.externalReference = externalReference;
    return this;
  }

  public CheckPartyCreditRating addExternalReferenceItem(ExternalReference externalReferenceItem) {
    if (this.externalReference == null) {
      this.externalReference = new ArrayList<ExternalReference>();
    }
    this.externalReference.add(externalReferenceItem);
    return this;
  }

  /**
   * Get externalReference
   * @return externalReference
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ExternalReference> getExternalReference() {
    return externalReference;
  }

  public void setExternalReference(List<ExternalReference> externalReference) {
    this.externalReference = externalReference;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CheckPartyCreditRating checkPartyCreditRating = (CheckPartyCreditRating) o;
    return Objects.equals(this.id, checkPartyCreditRating.id) &&
        Objects.equals(this.type, checkPartyCreditRating.type) &&
        Objects.equals(this.description, checkPartyCreditRating.description) &&
        Objects.equals(this.status, checkPartyCreditRating.status) &&
        Objects.equals(this.creditAgencyName, checkPartyCreditRating.creditAgencyName) &&
        Objects.equals(this.creditAgencyType, checkPartyCreditRating.creditAgencyType) &&
        Objects.equals(this.ratingRisk, checkPartyCreditRating.ratingRisk) &&
        Objects.equals(this.ratingScore, checkPartyCreditRating.ratingScore) &&
        Objects.equals(this.ratingDate, checkPartyCreditRating.ratingDate) &&
        Objects.equals(this.creditLimit, checkPartyCreditRating.creditLimit) &&
        Objects.equals(this.partyCreditBalance, checkPartyCreditRating.partyCreditBalance) &&
        Objects.equals(this.validFor, checkPartyCreditRating.validFor) &&
        Objects.equals(this.relatedParty, checkPartyCreditRating.relatedParty) &&
        Objects.equals(this.relatedPayment, checkPartyCreditRating.relatedPayment) &&
        Objects.equals(this.quote, checkPartyCreditRating.quote) &&
        Objects.equals(this.policyRule, checkPartyCreditRating.policyRule) &&
        Objects.equals(this.characteristic, checkPartyCreditRating.characteristic) &&
        Objects.equals(this.createDateTime, checkPartyCreditRating.createDateTime) &&
        Objects.equals(this.lastUpdateDateTime, checkPartyCreditRating.lastUpdateDateTime) &&
        Objects.equals(this.createdBy, checkPartyCreditRating.createdBy) &&
        Objects.equals(this.lastUpdateBy, checkPartyCreditRating.lastUpdateBy) &&
        Objects.equals(this.externalReference, checkPartyCreditRating.externalReference);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, type, description, status, creditAgencyName, creditAgencyType, ratingRisk, ratingScore, ratingDate, creditLimit, partyCreditBalance, validFor, relatedParty, relatedPayment, quote, policyRule, characteristic, createDateTime, lastUpdateDateTime, createdBy, lastUpdateBy, externalReference);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CheckPartyCreditRating {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    creditAgencyName: ").append(toIndentedString(creditAgencyName)).append("\n");
    sb.append("    creditAgencyType: ").append(toIndentedString(creditAgencyType)).append("\n");
    sb.append("    ratingRisk: ").append(toIndentedString(ratingRisk)).append("\n");
    sb.append("    ratingScore: ").append(toIndentedString(ratingScore)).append("\n");
    sb.append("    ratingDate: ").append(toIndentedString(ratingDate)).append("\n");
    sb.append("    creditLimit: ").append(toIndentedString(creditLimit)).append("\n");
    sb.append("    partyCreditBalance: ").append(toIndentedString(partyCreditBalance)).append("\n");
    sb.append("    validFor: ").append(toIndentedString(validFor)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    relatedPayment: ").append(toIndentedString(relatedPayment)).append("\n");
    sb.append("    quote: ").append(toIndentedString(quote)).append("\n");
    sb.append("    policyRule: ").append(toIndentedString(policyRule)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("    createDateTime: ").append(toIndentedString(createDateTime)).append("\n");
    sb.append("    lastUpdateDateTime: ").append(toIndentedString(lastUpdateDateTime)).append("\n");
    sb.append("    createdBy: ").append(toIndentedString(createdBy)).append("\n");
    sb.append("    lastUpdateBy: ").append(toIndentedString(lastUpdateBy)).append("\n");
    sb.append("    externalReference: ").append(toIndentedString(externalReference)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

