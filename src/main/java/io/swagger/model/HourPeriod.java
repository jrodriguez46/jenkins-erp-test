package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * HourPeriod
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-05-26T15:24:42.266Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class HourPeriod   {
  @JsonProperty("endHour")
  private String endHour = null;

  @JsonProperty("startHour")
  private String startHour = null;

  public HourPeriod endHour(String endHour) {
    this.endHour = endHour;
    return this;
  }

  /**
   * The time when the status ends applying
   * @return endHour
  **/
  @ApiModelProperty(value = "The time when the status ends applying")


  public String getEndHour() {
    return endHour;
  }

  public void setEndHour(String endHour) {
    this.endHour = endHour;
  }

  public HourPeriod startHour(String startHour) {
    this.startHour = startHour;
    return this;
  }

  /**
   * The time when the status starts applying
   * @return startHour
  **/
  @ApiModelProperty(value = "The time when the status starts applying")


  public String getStartHour() {
    return startHour;
  }

  public void setStartHour(String startHour) {
    this.startHour = startHour;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HourPeriod hourPeriod = (HourPeriod) o;
    return Objects.equals(this.endHour, hourPeriod.endHour) &&
        Objects.equals(this.startHour, hourPeriod.startHour);
  }

  @Override
  public int hashCode() {
    return Objects.hash(endHour, startHour);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HourPeriod {\n");
    
    sb.append("    endHour: ").append(toIndentedString(endHour)).append("\n");
    sb.append("    startHour: ").append(toIndentedString(startHour)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

